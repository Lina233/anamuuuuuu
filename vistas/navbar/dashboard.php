<main>
        <div class="content-all">
            <header>
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
                
            </header>
            <input type="checkbox" name="" id="check">
            <label for="check" class="icon-menu"><img width="30px" src="../../vistas/navbar/css/imagenes/boton.png"></label>
            <h2><img class="logo" width="350px" src="../../vistas/navbar/css/imagenes/2.png"></h2>
            <!-- <img width="1000" src="css/imagenes/2.png"> -->
            <nav class="menu">
                <ul>
                    
                    <li><i class="fas fa-user-friends"></i> <a href="../../Usuario/Vista/ListarUsuarios.php">Usuarios</a></li>
                    <li><i class="fab fa-product-hunt"></i> <a href="../../Producto/Vista/ListarProducto.php">Productos</a></li>
                    <li>
                    <i class="fas fa-boxes"></i> 
                      <a href="../../Categoria/Vista/ListarCategoria.php">Categorías</a>
                    </li>
                    <li><i class="fas fa-store-alt"></i> <a href="../../Servicio/Vista/ListarServicio.php">Servicios</a></li>

                    

                    <li ><i class="fas fa-seedling"></i> <a href="../../HojaTecnica/Vista/ListarHojaTecnica.php">Hojas Técnicas</a></li>   
                    
                    
                    <li><i class="fab fa-pagelines"></i> <a href="../../Tipo_Planta/Vista/ListarTipoPlanta.php">Tipo Planta</a></li>
                    <li><i class="fas fa-tasks"></i> <a href="../../Facturacion/Vista/ListarCotizacion.php">Cotización</a></li>
                    <li><i class="fas fa-file-invoice-dollar"></i> <a href="../../IVA/Vista/ListarIVA.php">IVA</a></li>
                    <li><i class="far fa-calendar-alt"></i>   <a href="../../Agenda/Vista/ListarAgenda.php"> Agenda</a></li>
                    <li><i class="fas fa-book"></i>   <a href="../../vistas/vistas/ManualUsuario.php"> Manual de Usuario</a></li>
                    <li><a href="../../CerrarSesion.php">Cerrar Sesión</a></li>
                    
                </ul>
            </nav>
        </div>
    </main>
    <!-- No borrar este div como el navbar esta en posicion absoluta queda
    un espacio con este div se llena ese espacio -->
    <div class="div-espacio"></div>