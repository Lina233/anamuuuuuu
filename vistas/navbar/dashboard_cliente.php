<main>
        <div class="content-all">
            <header>
                 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
            </header>
            <input type="checkbox" name="" id="check">
            <label for="check" class="icon-menu"><img width="30px" src="../../vistas/navbar/css/imagenes/boton.png"></label>
            <h2><img class="logo" width="350px" src="../../vistas/navbar/css/imagenes/2.png"></h2>
            <!-- <img width="1000" src="css/imagenes/2.png"> -->
            <nav class="menu">
                <ul>
                    <li><i class="far fa-calendar-alt"></i> <a href="../../Agenda/calendarCliente/calendario.php" >Agendar Servicios</li>
                    
                    <li><i class="fas fa-user"></i> <a href="../../vistas/vistas/EditarClientes.php">Mi Usuario</a></li>
                    <li ><i class="fas fa-seedling"></i> <a href="../../HojaTecnica/Vista/ListarHojaTecnicaCliente.php">Hojas Técnicas</a></li>   
                    <!-- <li><a href="../../Producto/Vista/ListarProducto.php">Productos</a></li> -->
                    <!-- <li><a href="../../Servicio/Vista/ListarServicio.php">Servicios</a></li> -->
                    
                    
                    <li><a href="../../CerrarSesion.php">Cerrar Sesión</a></li>
                </ul>
            </nav>
        </div>
    </main>
    <!-- No borrar este div como el navbar esta en posicion absoluta queda
    un espacio con este div se llena ese espacio -->
    <div class="div-espacio"></div>