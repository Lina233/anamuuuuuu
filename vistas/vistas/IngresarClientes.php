<?php



    
require_once('../../Conexion.php');


require_once('../../Usuario/Modelo/Usuario.php');
require_once('../../Usuario/Modelo/CrudUsuario.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-navbarcss.css">
     <link rel="stylesheet" type="text/css" href="estilos/estilos-ingresarClientes.css">
    
</head>
    
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/navbar.php'); 
    ?>
    
    <form action="../../Usuario/Controlador/ControladorUsuario.php" method="post">
        <h1 class="titulo1">Formulario de Registro</h1>
    <div class="diiv-2">
        <p>Segundo Nombre:</p>
        <input type="" name="Segundo_Nombre" id="Segundo_Nombre" pattern="[A-Za-z´-Ññ-]+"
        required >
        <br>
        <p>Segundo Apellido:</p>
        <input type="" name="Segundo_Apellido" id="Segundo_Apellido" pattern="[A-Za-z´-Ññ-]+" required>
        <br>
        <p>Documento de Identidad:</p>
        <input type="" name="Documento_Iden" id="Documento_Iden" pattern="[0-9]{6,12}"
        required >
        <br>
        <p>Edad:</p>
        <input type="" name="Edad" id="Edad" pattern="[0-9]+" required>
        <br>
        <p>Telefono:</p>
        <input type="" pattern="[0-9]+" name="Telefono" id="Telefono">
        
        <br>
        <br>

        <p>Contraseña:</p>
        <input type="" name="Contrasena" id="Contrasena"
        onkeyup="contraseñas_iguales()" 
        required type="password" title="Minimo 8 digitos" pattern="[A-Za-z0-9]{8,20}">
        <br>
        <p>Confirmar Contraseña:</p>
        <input type="" onkeyup="contraseñas_iguales()" name="ConfirmarContrasena" id="ConfirmarContrasena" 
        required type="password" title="Minimo 8 digitos" pattern="[A-Za-z0-9]{8,20}">
        <br>
        <label id="validacion_contraseñas" class="alerta">
        </label>
        <br>
        <br>

        
    </div>  
    <div class="diiv-3">
        <p>Primer Nombre:</p>
        <input type="" name="Primer_Nombre" id="Primer_Nombre" pattern="[A-Za-z´-Ññ-]+"
        required >
        <br>
        <p>Primer Apellido:</p>
        <input type="" name="Primer_Apellido" id="Primer_Apellido" pattern="[A-Za-z´-Ññ-]+"
        required >
        <br>
        <p>Género:</p>
                <select  name="Genero" id="Genero">
                    <option value="Masculino">Masculino
                    </option>
                    <option value="Femenino">Femenino</option>
                    <option value="Otro">Otro</option>
                </select>
                <br>

        
        <p>Dirección:</p>
        <input type="" name="Direccion" id="Direccion"
        required pattern="[A-aZ-z -ñÑ-09]">
        <br>
        <p>Correo Electronico:</p>
        <input type=""  name="Correo_Electronico" id="Correo_Electronico"
        onkeyup="validacion_correo_repetido();return validacion_correo_repetido();" 
        required type="email">
        <br>
        <label id="prob3" class="alerta"></label>
        <br>
        
        <p>Nombre Usuario:</p>
                <input  onkeyup="validacion_usuario_repetido();return validacion_usuario_repetido();" pattern="[A-Za-z0-9]{8,20}" required type="text" name="NombreUsuario" id="NombreUsuario" title="Minimo 8 digitos">
                                        <br>
                                        
        <label id="prob2" class="alerta"></label>
                                        <br>
                        <p>Acepto las politicas de uso de información:</p>
                        <select required  name="Politicas" id="Politicas">
                            <option value="">Seleccionar</option>
                            <option value="Si">Si</option>              
                        </select> 
                        <br>
                        <br>
                        <a class="politicas" href="../../vistas/vistas/Politicas.php" target="_blank">Ver Políticas de Privacidad</a>

    </div>  
    <div class="form-column-boton1">
        <button class="btn1" onclick="" type="submit" name="RegistarCliente" id="RegistarCliente">
            Registrar
        </button>
    </div>
        
    </form>

    
</body>
<script>
    
function contraseñas_iguales()
{
    var campo1 = $('#Contrasena').val();
    var campo2 = $('#ConfirmarContrasena').val();

    if ( campo1==campo2 )
    {
        // $(#Modificar).attr('disabled',false);
        document.getElementById("RegistarCliente").disabled = false;
        $("#validacion_contraseñas").text("Las Contraseñas si coinciden");
    }
    else
    {
         // $(#Modificar).attr('disabled',true);
         document.getElementById("RegistarCliente").disabled = true;
         $("#validacion_contraseñas").text("No coinciden las Contraseñas");
    }

}


function validacion_usuario_repetido()
{
    var nombre= document.getElementById("NombreUsuario").value;
    var dataen= 'nombre='+ nombre;
    
    $.ajax({
        type:'post',
        url:'../../Usuario/Controlador/ControladorUsuarioRepetido.php',

        data:dataen,
        success:function(r)
        {
            // alert(r);

            
            if(r == 0)
            {
                $('#prob2').html("Usuario disponible");
                document.getElementById("RegistarCliente").disabled = false;
            }else 
            {
                $('#prob2').html("Usuario no disponible");
                document.getElementById("RegistarCliente").disabled = true;
            }

            
        }

    });

    return false;
}

function validacion_correo_repetido()
{
    var correo= document.getElementById("Correo_Electronico").value;
    var dataen= 'correo='+ correo;
    
    $.ajax({
        type:'post',
        url:'../../Usuario/Controlador/ControladorCorreoRepetido.php',

        data:dataen,
        success:function(re)
        {
            // alert(r);

            
            if(re == 0)
            {
                $('#prob3').html("Correo disponible");
                document.getElementById("RegistarCliente").disabled = false;
            }else 
            {
                $('#prob3').html("Correo no disponible");
                document.getElementById("RegistarCliente").disabled = true;
            }

            
        }

        

    });

    return false;
}
</script>
</html>
