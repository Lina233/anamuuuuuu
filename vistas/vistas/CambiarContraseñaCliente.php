<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==1  ) 
{
    header("Location:../../Index.php");
}elseif ($_SESSION["IdRol"]==2) {
    header("Location:../../Index.php");
}
// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
    
require_once('../../Conexion.php');
//require_once('../../Tipo_Producto/Modelo/Tipo_Producto.php');

require_once('../../Usuario/Modelo/Usuario.php');
require_once('../../Usuario/Modelo/CrudUsuario.php');


$CrudUsuario = new CrudUsuario(); 

// $_SESSION["IdUsuario"] = $Usuario->getIdUsuario();
// IdUsuario=<?php echo $Usuarios->getIdUsuario()
// IdUsuario=$Usuarios->getIdUsuario()

$IDCliente=$_SESSION["IdUsuario"];
// echo $IDCliente;


// $UsuarioRepetido = $CrudUsuario::ObtenerUsuarioRepetido($_GET["NombreUsuario"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    </script>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
     <link rel="stylesheet" type="text/css" href="estilos/estilos-CambiarContraseñaClientes.css">
    
</head>
    
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard_cliente.php'); 
    ?> 
    
    <div class="diiv-1">

        <form name="form1" action="../../Usuario/Controlador/ControladorUsuario.php" method="post" >
        <h1 class="titulo1">Cambiar Contraseña</h1>
            
        <div class="diiv-2">
                <!-- input oculto del id -->
                <input pattern="[A-Za-z´-ñÑ-]+" type="hidden"  name="IdUsuario" id="IdUsuario"
                value="<?php echo $IDCliente;  ?>" >
         <br>       
         <p>Confirmar Contraseña:</p>
        <input type="" onkeyup="contraseñas_iguales()" name="ConfirmarContrasena" id="ConfirmarContrasena" 
        required type="password" title="Minimo 8 digitos" pattern="[A-Za-z0-9]{8,20}">
        <br>       
            
                                             
        </div>          
        <div class="diiv-3">
            <br> 
                <p>Contraseña:</p>
        <input type="" name="Contrasena" id="Contrasena"
        onkeyup="contraseñas_iguales()" 
        required type="password" title="Minimo 8 digitos" pattern="[A-Za-z0-9]{8,20}">
        <br>
        
        <label id="validacion_contraseñas" class="alerta">
        </label>


                

                

         </div>  
                
        
        <div class="form-column-boton1">
            
            <button   class="btn1" onclick="deshabilita()" type="submit" name="CambiarContraseña" id="Modificar" >Modificar</button>
        </div>
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="EditarClientes.php">Cancelar</a>
            </button>
        </div>
        

        <br>
        </div>
    </form>
    <script src="js/funciones.js"></script>
</body>
<script>
    

//si pongo este el boton no funciona
// document.getElementById("Modificar").disabled = true;

//si pongo este el boton si funciona
// document.getElementById("Modificar").disabled = false;





function contraseñas_iguales()
{
    var campo1 = $('#Contrasena').val();
    var campo2 = $('#ConfirmarContrasena').val();

    if ( campo1==campo2 )
    {
        // $(#Modificar).attr('disabled',false);
        document.getElementById("Modificar").disabled = false;
        $("#validacion_contraseñas").text("Las Contraseñas si coinciden");
    }
    else
    {
         // $(#Modificar).attr('disabled',true);
         document.getElementById("Modificar").disabled = true;
         $("#validacion_contraseñas").text("No coinciden las Contraseñas");
    }

}







</script>
</html>
