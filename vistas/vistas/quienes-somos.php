
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimun-scale=1">
    
    
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-navbarcss.css">
    <!-- <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-navbar1.css"> -->
    <link rel="stylesheet" type="text/css" href="estilos/quienes-somos.css">
    
</head>
<body>
    
   <?php 
    include('../../vistas/navbar/navbar.php'); 
    ?>


    <div class="diiv-1">
        <p class="titulo1">Quiénes Somos</p>
    </div>

    <div class="diiv-2">
        
        <h1 class="titulo_mision">Misión</h1>
        <p class="parrafo_mision">Realizar  labores de acondicionamiento, mantenimiento de jardines y zonas verdes cuidando cada planta a partir de su correcto diagnóstico, mantenimiento y protección, logrando que cada espacio pueda ser apreciado, disfrutado y preservado integralmente por todos los habitantes. Ponga en nuestras manos las zonas verdes y jardines de su propiedad y las vera crecer en abundancia.
    </p>
        <h1 class="titulo_objetivos">Visión</h1>
        <p class="parrafo_mision">Ser una empresa líder en soluciones de jardinería, compartiendo nuestros conocimientos para el beneficio de todos nuestros cliente ademas de  proveer un servicio profesional y unico.</p>
    </div>
    <div class="diiv-3">
        <h1 class="titulo_objetivos">Valores corporativos</h1>
        <p class="parrafo_mision">Entrega a tiempo.
        <br> 
        ▪ Alta calidad.
        
        <br> 
        ▪ Comunicación fluida, dinamica y permanente.
        
        <br> 
        ▪ Atención on-line.
        
        <br> 
        ▪ Información confiable y actualizada.
        <br> 
        ▪ Sugerencias.
        
        <br> 
        ▪ Crecimiento en conjunto: Su Negocio y el Nuestro. 
        <br> 
        ▪ Reposición inmediata.
        <br> 
        ▪ Atención personalizada.
        <br> 
        ▪ Marketing interactivo.
        <br> 
        ▪ Relación Costo/Beneficio.</p>
    </div>
    <div class="diiv-4">
        <h1 class="nuestraubicacion_titulo">Nuestra Ubicación</h1>
        <h1 class="nuestraubicacion_titulo2">Calle 83B # 23c-177</h1>
        <h1 class="nuestraubicacion_titulo3">Contáctanos</h1>
        <h1 class="nuestraubicacion_titulo4">3115113733</h1>
        <h1 class="nuestraubicacion_titulo5">mantenimientoestrada@gmail.com</h1>
    </div>
    
    <div class="diiv-5">
        <iframe  class="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9154127356433!2d-75.54135468676535!3d6.274852127748507!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4428bb35752a05%3A0xa4d6d792d1e5e8db!2sCl.%2083b%20%2323c-177%2C%20Medell%C3%ADn%2C%20Antioquia!5e0!3m2!1sen!2sco!4v1596152322938!5m2!1sen!2sco" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

</body>
</html>