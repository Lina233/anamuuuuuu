<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==1  ) 
{
    header("Location:../../Index.php");
}elseif ($_SESSION["IdRol"]==2) {
    header("Location:../../Index.php");
}
// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
    
require_once('../../Conexion.php');
//require_once('../../Tipo_Producto/Modelo/Tipo_Producto.php');

require_once('../../Usuario/Modelo/Usuario.php');
require_once('../../Usuario/Modelo/CrudUsuario.php');


$CrudUsuario = new CrudUsuario(); 

// $_SESSION["IdUsuario"] = $Usuario->getIdUsuario();
// IdUsuario=<?php echo $Usuarios->getIdUsuario()
// IdUsuario=$Usuarios->getIdUsuario()

$IDCliente=$_SESSION["IdUsuario"];
// echo $IDCliente;
$Usuario = $CrudUsuario::ObtenerCliente($IDCliente);
$NombreRol = $CrudUsuario::ObtenerNombreRolUsuario($IDCliente);

// $UsuarioRepetido = $CrudUsuario::ObtenerUsuarioRepetido($_GET["NombreUsuario"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    </script>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
     <link rel="stylesheet" type="text/css" href="estilos/estilos-editarClientes.css">
    
</head>
    
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard_cliente.php'); 
    ?> 
    
    <div class="diiv-1">

        <form name="form1" action="../../Usuario/Controlador/ControladorUsuario.php" method="post" >
        <h1 class="titulo1"  >Mi Usuario</h1>
            
        <div class="diiv-2">
                <!-- input oculto del id -->
                <input pattern="[A-Za-z´-ñÑ-]+" value="<?php echo $Usuario->getIdUsuario(); ?>" type="hidden"  name="IdUsuario" id="IdUsuario" >
                <p>Correo Electrónico:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" value="<?php echo $Usuario->getCorreo_Electronico(); ?>" type=""  name="Correo_Electronico" id="Correo_Electronico" disabled >
                <br>

                <p>Segundo Nombre:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" value="<?php echo $Usuario->getSegundo_Nombre(); ?>" type=""  name="Segundo_Nombre" id="Segundo_Nombre" >
                <br>
                <p>Segundo Apellido:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" type="" value="<?php echo $Usuario->getSegundo_Apellido(); ?>" required name="Segundo_Apellido" id="Segundo_Apellido">
                <br>
                <p>Género:</p>
                <select name="Genero" id="Genero" >
                    <option value="<?php echo $Usuario->getGenero(); ?>"><?php echo $Usuario->getGenero(); ?></option>
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                    <option value="Otro">Otro</option>
                </select>
                <br>
                <p>Edad:</p>
                <input pattern="[0-9]+" value="<?php echo $Usuario->getEdad(); ?>"  required type="text" name="Edad" id="Edad">
                
            
                                             
        </div>          
        <div class="diiv-3">
                <!-- <p>ID Hoja_Tecnica:</p> 
                <input pattern="[0-9]+" type="text" name="ID_Hoja_Tecnica" id="ID_Hoja_Tecnica">
                <br> -->
                <p>Nombre de Usuario:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" value="<?php echo $Usuario->getNombreUsuario(); ?>"  type=""  required  name="NombreUsuario" id="NombreUsuario" disabled >
                <br>
                <p>Primer Nombre:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" value="<?php echo $Usuario->getPrimer_Nombre(); ?>"  type=""  required  name="Primer_Nombre" id="Primer_Nombre">
                <br>
                <p>Primer Apellido:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" value="<?php echo $Usuario->getPrimer_Apellido(); ?>"  type=""  required name="Primer_Apellido" id="Primer_Apellido">
                <br>
                
                
                
                <p>Dirección:</p>
                <input pattern="[A-Za-z -ñÑ-09]+" value="<?php echo $Usuario->getDireccion(); ?>"  required type="text" name="Direccion" id="Direccion">         
                <br>
                <p>Teléfono:</p>
                <input onkeyup="activar_boton()" required pattern="[0-9]+" value="<?php echo $Usuario->getTelefono(); ?>"   type="text" name="Telefono"  id="Telefono">

                
                
                <br>
                <p>Documento de Identidad:</p>
                <input pattern="[0-9]{6,12}" value="<?php echo $Usuario->getDocumento_Iden(); ?>"  required type="text" name="Documento_Iden" id="Documento_Iden">



                

                

         </div>  
                
        
        <div class="form-column-boton1">
            
            <button   class="btn1" onclick="deshabilita()" type="submit" name="ModificarCliente" id="Modificar" >Modificar</button>
        </div>
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="EditarClientes.php">Cancelar</a>
            </button>
        </div>
        <div class="form-column-boton3">
            <button class="btn3" type="button">
                <a href="CambiarContraseñaCliente.php">Cambiar Contraseña</a>
            </button>
        </div>

        <br>
        </div>
    </form>
    <script src="js/funciones.js"></script>
</body>
<script>
    

//si pongo este el boton no funciona
// document.getElementById("Modificar").disabled = true;

//si pongo este el boton si funciona
// document.getElementById("Modificar").disabled = false;




function validacion_usuario_repetido()
{
    var nombre= document.getElementById("NombreUsuario").value;
    var dataen= 'nombre='+ nombre;
    
    $.ajax({
        type:'post',
        url:'../Modelo/CrudUsuariox.php',

        data:dataen,
        success:function(r)
        {
            // alert(r);

            
            if(r == 0)
            {
                $('#prob2').html("Usuario disponible");
                document.getElementById("Modificar").disabled = false;
            }else 
            {
                $('#prob2').html("Usuario no disponible");
                document.getElementById("Modificar").disabled = true;
            }

            
        }

        

    });

    return false;
}






  



function contraseñas_iguales()
{
    var campo1 = $('#Contrasena').val();
    var campo2 = $('#ConfirmarContrasena').val();

    if ( campo1==campo2 )
    {
        // $(#Modificar).attr('disabled',false);
        document.getElementById("Modificar").disabled = false;
        $("#validacion_contraseñas").text("Las Contraseñas si coinciden");
    }
    else
    {
         // $(#Modificar).attr('disabled',true);
         document.getElementById("Modificar").disabled = true;
         $("#validacion_contraseñas").text("No coinciden las Contraseñas");
    }

}







</script>
</html>
