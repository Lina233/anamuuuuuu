<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
    


// $CrudUsuario = new CrudUsuario(); //Crear de un objeto CrudCompetencia



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
     <link rel="stylesheet" type="text/css" href="CSS/estilos-ingresarUsuarios.css">
    
</head>
    
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <div class="diiv-1">

        <form action="../Controlador/ControladorUsuario.php" method="post">
        <h1 class="titulo1">Crear Usuario</h1>
            
        <div class="diiv-2">
                <p>Segundo Nombre:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" type=""  name="Segundo_Nombre" id="Segundo_Nombre">
                <br>
                <p>Segundo Apellido:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" type="" required name="Segundo_Apellido" id="Segundo_Apellido">
                <br>
                <p>Género:</p>
                <select name="Genero" id="Genero">
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                    <option value="Otro">Otro</option>
                </select>
                <br>
                <p>Edad:</p>
                <input pattern="[0-9]+" required type="text" name="Edad" id="Edad">
                <br>
                <p>Teléfono:</p>
                <input pattern="[0-9]+" required type="text" name="Telefono" id="Telefono">
                <br>
                
                <p>Correo Electronico:</p>
                <input onkeyup="validacion_correo_repetido();return validacion_correo_repetido();" required type="email" name="Correo_Electronico" id="Correo_Electronico">
                <br>    
                <label id="prob3" class="alerta"></label>            
                <br> 
                <br>
                <p>Contraseña:</p>
                <input onkeyup="contraseñas_iguales()" pattern="[A-Za-z0-9]{8,20}" required type="password" name="Contrasena" id="Contrasena" title="Minimo 8 digitos">
                <br>
                <p>Confirmar Contraseña:</p>
                <input onkeyup="contraseñas_iguales()" pattern="[A-Za-z0-9]{8,20}" required type="password" name="ConfirmarContrasena" id="ConfirmarContrasena" title="Minimo 8 digitos">
                <br>
                <label id="validacion_contraseñas" class="alerta"></label>
                <br>
                <br>
                
        </div>          
        <div class="diiv-3">
                <!-- <p>ID Hoja_Tecnica:</p> 
                <input pattern="[0-9]+" type="text" name="ID_Hoja_Tecnica" id="ID_Hoja_Tecnica">
                <br> -->
                <p>Primer Nombre:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" type=""  required  name="Primer_Nombre" id="Primer_Nombre">
                <br>
                <p>Primer Apellido:</p>
                <input pattern="[A-Za-z´-ñÑ-]+" type=""  required name="Primer_Apellido" id="Primer_Apellido">
                <br>
                <p>Documento de Identidad:</p>
                <input pattern="[0-9]{8,20}" type=""  required name="Documento_Iden" id="Documento_Iden">
                <br>
                <p>Dirección:</p>
                <input pattern="[A-Za-z -ñÑ-09]+" required type="text" name="Direccion" id="Direccion">
                <br>
                <p>Rol:</p>   
                <select name="IdRol" id="IdRol">
                    <option value="3">Cliente</option>
                    <?php
                        if ($_SESSION["IdRol"]==1) {
                            
                            ?>
                                <option value="2">Empleado</option>
                                <option value="1">Administrador</option>
                            <?php

                        }
                    ?>
                    
                </select>
                <br>
                <p>Estado:</p>
                <select name="IdEstado" id="IdEstado">
                    <option value="1">Activado</option>
                    <option value="2">Desactivado</option>              
                </select>
                <br>     
                <br>
                <br>
                
                <p>Nombre Usuario:</p>
                                <input  onkeyup="validacion_usuario_repetido();return validacion_usuario_repetido();" pattern="[A-Za-z0-9]{8,20}" required type="text" name="NombreUsuario" id="NombreUsuario" title="Minimo 8 digitos">
                                <br>
                                
                                <label id="prob2" class="alerta"></label>
                                <br>
                <p>Acepto las politicas de uso de información:</p>
                <select required  name="Politicas" id="Politicas">
                    <option value="">Seleccionar</option>
                    <option value="Si">Si</option>              
                </select> 
                <br>
                <br>
                <a class="politicas" href="../../vistas/vistas/Politicas.php" target="_blank">Ver Políticas de Privacidad</a>
                

         </div>  
                
        
        <div class="form-column-boton1">
            <button   class="btn1" onclick="deshabilita()" type="submit" name="Registrar" id="Registrar" >Registrar</button>

            
        </div> 
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a class="cancelar" href="ListarUsuarios.php">Cancelar</a>
            </button>
        </div>
        
        <br>
        </div>
    </form>
    
</body>
<script>
    

//si pongo este el boton no funciona
// document.getElementById("Modificar").disabled = true;

//si pongo este el boton si funciona
// document.getElementById("Modificar").disabled = false;




function validacion_usuario_repetido()
{
    var nombre= document.getElementById("NombreUsuario").value; //lleva el valor de la vble
    var dataen= 'nombre='+ nombre;//crea encabezado para la vble
    
    $.ajax({ //funcion ejecuta en tiempo real sin recargar pag
        type:'post',
        url:'../Controlador/ControladorUsuarioRepetido.php', //lo envia al controlador 

        data:dataen,//datos a enviaar al controdor
        success:function(r)//defino la r
        {
            // alert(r);

            
            if(r == 0)
            {
                $('#prob2').html("Usuario disponible");
                document.getElementById("Modificar").disabled = false;
            }else 
            {
                $('#prob2').html("Usuario no disponible");
                document.getElementById("Modificar").disabled = true;
            }

            
        }

        

    });

    return false;
}

function validacion_correo_repetido()
{
    var correo= document.getElementById("Correo_Electronico").value;
    var dataen= 'correo='+ correo;
    
    $.ajax({
        type:'post',
        url:'../Controlador/ControladorCorreoRepetido.php',

        data:dataen,
        success:function(re)
        {
            // alert(r);

            
            if(re == 0)
            {
                $('#prob3').html("Correo disponible");
                document.getElementById("Registrar").disabled = false;
            }else 
            {
                $('#prob3').html("Correo no disponible");
                document.getElementById("Registrar").disabled = true;
            }

            
        }

        

    });

    return false;
}






  



function contraseñas_iguales()
{
    var campo1 = $('#Contrasena').val();
    var campo2 = $('#ConfirmarContrasena').val();

    if ( campo1==campo2 )
    {
        // $(#Modificar).attr('disabled',false);
        document.getElementById("Registrar").disabled = false;
        $("#validacion_contraseñas").text("Las Contraseñas si coinciden");
    }
    else
    {
         // $(#Modificar).attr('disabled',true);
         document.getElementById("Registrar").disabled = true;
         $("#validacion_contraseñas").text("No coinciden las Contraseñas");
    }

}

function politicas()
{
    var campo1 = $('#Politicas').val();
    var campo2 = 'Si';

    if ( campo1 == campo2 )
    {
        // $(#Modificar).attr('disabled',false);
        document.getElementById("Registrar").disabled = false;
        $("#validacion_politicas").text("Las Contraseñas si coinciden");
    }
    else
    {
         // $(#Modificar).attr('disabled',true);
         document.getElementById("Registrar").disabled = true;
         $("#validacion_contraseñas").text("No coinciden las Contraseñas");
    }

}







</script>



</html>
