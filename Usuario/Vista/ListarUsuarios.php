﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}


?>
<?php
require_once('../../Conexion.php');
require_once('../../Usuario/Modelo/Usuario.php');
require_once('../../Usuario/Modelo/CrudUsuario.php');

$CrudUsuario = new CrudUsuario(); //Crear de un objeto CrudCompetencia
$ListaUsuario = $CrudUsuario->ListarUsuarios(); //Llamado al método 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-usuarios.css">
    
</head>
<body>
    
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Usuarios</h1>
    <div class="form-column-boton1">    
        <button class="btn1" type="button">
            <a href="IngresarUsuarios.php">Crear Usuario</a>
        </button>
    </div> 
    <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="../../TCPDF/examples/reporteRoles.php">Reporte PDF</a>
            </button>
        </div>
    
    <div id="main-container">
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Nombre Usuario</th>
                <th>Edad</th>
                <th>Género</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th>Correo Electrónico</th>
                <th>Rol</th>
                <th>Acciones</th>                   
            </tr>
            </thead>

            <tbody>
            <?php
                foreach($ListaUsuario as $Usuarios){

                
                    ?>
                    <tr>
                        <td hidden="" ><?php echo $Usuarios->getIdEstado(); ?></td>
                        <td  ><?php echo $Usuarios->getIdUsuario(); ?></td>
                        <td><?php echo $Usuarios->getNombreUsuario(); ?></td>
                        <td><?php echo $Usuarios->getEdad(); ?></td>
                        <td><?php echo $Usuarios->getGenero(); ?></td>
                        <td><?php echo $Usuarios->getTelefono(); ?></td>
                        <td><?php echo $Usuarios->getDireccion(); ?></td>
                        <td><?php echo $Usuarios->getCorreo_Electronico(); ?></td>
                        <td><?php echo $Usuarios->getIdRol(); ?></td>
                        <td>




                        <?php
                        if ($_SESSION["IdRol"]==1) {
                            
                            ?>
                                <a class="link2" href="EditarUsuarios.php?IdUsuario=<?php echo $Usuarios->getIdUsuario(); ?>"><!-- Editar --><img title="Editar" width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a>

                            <?php

                        }elseif ($_SESSION["IdRol"]==2) {
                            
                            if ($Usuarios->getIdRol()==3) {
                                
                                ?>
                                <a class="link2" href="EditarUsuarios.php?IdUsuario=<?php echo $Usuarios->getIdUsuario(); ?>"><!-- Editar --><img title="Editar" width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a>

                            <?php

                            }
                            

                        }
                        ?>

                        
                        
                        <?php
                            if ($Usuarios->getIdEstado()==1) {
                        ?>

                            <?php
                            if ($Usuarios->getIdRol()==1) {
                            
                            ?>  

                            <?php
                            }else{
                            ?>
                                <a  
                                    href="../Controlador/ControladorUsuario.php?IdUsuario=<?php echo $Usuarios->getIdUsuario();?>:<?php echo $Usuarios->getIdEstado();?>&Accion=EditarUsuario"
                                    ><img title="Usuario Activo" width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                                </a>
                            <?php
                            }
                            ?>


                                
                        
                            
                        <?php             
                            }
                            elseif ($Usuarios->getIdEstado()==2) {
                            ?>


                                <?php
                                    if ($Usuarios->getIdRol()==1) {
                                      

                                    }else{
                                    ?>

                                         <a  
                                            href="../Controlador/ControladorUsuario.php?IdUsuario=<?php echo $Usuarios->getIdUsuario();?>:<?php echo $Usuarios->getIdEstado();?>&Accion=EditarUsuario"
                                            ><img title="Usuario Inactivo" width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                                        </a>

                                    <?php
                                    }
                                ?>



                           
                            <?php  

                            }

                        ?>


                        </td>
                        
                    </tr>
                    <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</body>
</html>
                 