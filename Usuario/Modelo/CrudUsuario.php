<?php

require_once('../../Roles/Modelo/Roles.php');

// $nombre= $_POST['nombre'];
// echo "Hola $nombre";
// echo "Hola <b>".$_POST['nombre']."</b> ";

class CrudUsuario
{
	public function __construct(){} 

    



	public function ValidarAcceso($Usuario)
	{
		$Db= Db::Conectar();
		$Sql= $Db->prepare('SELECT * FROM usuarios WHERE NombreUsuario=:NombreUsuario AND IdEstado=1');

		$Sql->bindvalue('NombreUsuario',$Usuario->getNombreUsuario());


        $Sql->execute();//ejecutar la consulta
		$MiUsuario = new Usuario();
		
		if ($Sql->rowCount() > 0)//rowCont: determinar el umero de registros arrojados por la consulta 
		{
			$DatosUsuario= $Sql->fetch();//Almacenar los datos arrojados por la consulta este datos usuario es un array
			
            
            $MiUsuario->setIdUsuario($DatosUsuario['IdUsuario']);
			$MiUsuario->setNombreUsuario($DatosUsuario['NombreUsuario']);
			$MiUsuario->setIdRol($DatosUsuario['IdRol']);
            $MiUsuario->setPrimer_Nombre($DatosUsuario['Primer_Nombre']);
            $MiUsuario->setPrimer_Apellido($DatosUsuario['Primer_Apellido']);
            $MiUsuario->setSegundo_Apellido($DatosUsuario['Segundo_Apellido']);

            //el set significa que lo que estaba en ese campo se lo ponemos a mi usuarios
            //y abajo enhash mi usuario get significa que a hash le asignamos el valor que esta en mi usuario
            $MiUsuario->setContrasena($DatosUsuario['Contrasena']);
            $hash=$MiUsuario->getContrasena();

            $password=$Usuario->getContrasena();

            if (password_verify($password, $hash)) {
                
                $MiUsuario->setExiste(1);

            }else{

                $MiUsuario->setExiste(0);
            };

            
		}
		else
		{
			$MiUsuario->setExiste(0);
		}
		return $MiUsuario;
	}


	public function InsertarUsuario($Usuario){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO  usuarios(IdUsuario,NombreUsuario,Contrasena,IdEstado,IdRol,Correo_Electronico,Telefono,Direccion,Primer_Nombre,Segundo_Nombre,Primer_Apellido,Segundo_Apellido,Edad,Genero,Politicas,Documento_Iden)
            VALUES(NULL,:NombreUsuario,:Contrasena,:IdEstado,:IdRol,:Correo_Electronico,:Telefono,:Direccion,:Primer_Nombre,:Segundo_Nombre,:Primer_Apellido,:Segundo_Apellido,:Edad,:Genero,:Politicas,:Documento_Iden)');
 			

            // $Sql->execute();
            $Sql->bindValue('NombreUsuario',$Usuario->getNombreUsuario());
            $Sql->bindValue('IdEstado',$Usuario->getIdEstado());
            $Sql->bindValue('IdRol',$Usuario->getIdRol());
            $Sql->bindValue('Correo_Electronico',$Usuario->getCorreo_Electronico());
            $Sql->bindValue('Telefono',$Usuario->getTelefono());
            $Sql->bindValue('Direccion',$Usuario->getDireccion());
            $Sql->bindValue('Primer_Nombre',$Usuario->getPrimer_Nombre());
            $Sql->bindValue('Segundo_Nombre',$Usuario->getSegundo_Nombre());
            $Sql->bindValue('Primer_Apellido',$Usuario->getPrimer_Apellido());
            $Sql->bindValue('Segundo_Apellido',$Usuario->getSegundo_Apellido());
            $Sql->bindValue('Edad',$Usuario->getEdad());
            $Sql->bindValue('Genero',$Usuario->getGenero());
            $Sql->bindValue('Politicas',$Usuario->getPoliticas());
            $Sql->bindValue('Documento_Iden',$Usuario->getDocumento_Iden());

            


            //Descodificacion de la contraseña
            $password=$Usuario->getContrasena();
            $hash= password_hash($password, PASSWORD_DEFAULT, ['cost' => 10 ]); 
            $Sql->bindValue('Contrasena',$hash);

            


            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }


        public function InsertarCliente($Usuario){
            $Db= Db::Conectar();
            $Sql=$Db->prepare('INSERT INTO usuarios(IdUsuario,NombreUsuario,Contrasena,IdEstado,IdRol,Correo_Electronico,Telefono,Direccion,Primer_Nombre,Segundo_Nombre,Primer_Apellido,Segundo_Apellido,Edad,Genero,Politicas,Documento_Iden)
                VALUES(NULL,:NombreUsuario,:Contrasena,:IdEstado,:IdRol,:Correo_Electronico,:Telefono,:Direccion,:Primer_Nombre,:Segundo_Nombre,:Primer_Apellido,:Segundo_Apellido,:Edad,:Genero,:Politicas,:Documento_Iden)
                '); 

            $uno=1;
            $tres=3;
            $Sql->bindvalue('IdEstado',$uno);
            $Sql->bindvalue('IdRol',$tres);

            $Sql->bindvalue('NombreUsuario',$Usuario->getNombreUsuario());
            $Sql->bindvalue('Correo_Electronico',$Usuario->getCorreo_Electronico());
            $Sql->bindvalue('Telefono',$Usuario->getTelefono());
            $Sql->bindvalue('Direccion',$Usuario->getDireccion());
            $Sql->bindvalue('Primer_Nombre',$Usuario->getPrimer_Nombre());
            $Sql->bindvalue('Segundo_Nombre',$Usuario->getSegundo_Nombre());
            $Sql->bindvalue('Primer_Apellido',$Usuario->getPrimer_Apellido());
            $Sql->bindvalue('Segundo_Apellido',$Usuario->getSegundo_Apellido());
            $Sql->bindvalue('Edad',$Usuario->getEdad());
            $Sql->bindvalue('Genero',$Usuario->getGenero());
            $Sql->bindvalue('Politicas',$Usuario->getPoliticas());
            $Sql->bindvalue('Documento_Iden',$Usuario->getDocumento_Iden());

            $password=$Usuario->getContrasena();
            $hash= password_hash($password, PASSWORD_DEFAULT,['cost' => 10]);
            $Sql->bindvalue('Contrasena',$hash);


            try{
                $Sql->execute();

            }catch(Exception $e){
                echo $e->getMessage();
                die();
            }
        }

        public function ListarUsuarios(){
            $Db = Db::Conectar();
            $ListaUsuarios = [];
            $Sql = $Db->query('SELECT * FROM usuarios');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Usuario){
                $MyUsuario = new Usuario();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyUsuario->setIdUsuario($Usuario['IdUsuario']);

                $MyUsuario->setNombreUsuario($Usuario['NombreUsuario']);
                $MyUsuario->setEdad($Usuario['Edad']);
                $MyUsuario->setGenero($Usuario['Genero']);
                $MyUsuario->setTelefono($Usuario['Telefono']);
                $MyUsuario->setDireccion($Usuario['Direccion']);
                $MyUsuario->setCorreo_Electronico($Usuario['Correo_Electronico']);
                $MyUsuario->setIdRol($Usuario['IdRol']);
                $MyUsuario->setIdEstado($Usuario['IdEstado']);
                $ListaUsuarios[] = $MyUsuario;
            }
            return $ListaUsuarios;
        }

        public function ObtenerUsuario($IdUsuario)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM usuarios WHERE IdUsuario=:IdUsuario'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('IdUsuario',$IdUsuario);
            $MyUsuario = new Usuario();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Usuario = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyUsuario->setIdUsuario($Usuario['IdUsuario']);
                $MyUsuario->setNombreUsuario($Usuario['NombreUsuario']);
                $MyUsuario->setPrimer_Nombre($Usuario['Primer_Nombre']);
                $MyUsuario->setSegundo_Nombre($Usuario['Segundo_Nombre']);
                $MyUsuario->setPrimer_Apellido($Usuario['Primer_Apellido']);
                $MyUsuario->setContrasena($Usuario['Contrasena']);
                $MyUsuario->setSegundo_Apellido($Usuario['Segundo_Apellido']);
                $MyUsuario->setEdad($Usuario['Edad']);
                $MyUsuario->setGenero($Usuario['Genero']);
                $MyUsuario->setDireccion($Usuario['Direccion']);
                $MyUsuario->setTelefono($Usuario['Telefono']);
                $MyUsuario->setCorreo_Electronico($Usuario['Correo_Electronico']);
                $MyUsuario->setIdEstado($Usuario['IdEstado']);
                $MyUsuario->setIdRol($Usuario['IdRol']);
                $MyUsuario->setDocumento_Iden($Usuario['Documento_Iden']);

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyUsuario;
        }

        public function ObtenerCliente($IdUsuario)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM usuarios WHERE IdUsuario=:IdUsuario'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('IdUsuario',$IdUsuario);
            $MyUsuario = new Usuario();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Usuario = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                
                $MyUsuario->setIdUsuario($Usuario['IdUsuario']);
                $MyUsuario->setNombreUsuario($Usuario['NombreUsuario']);
                $MyUsuario->setPrimer_Nombre($Usuario['Primer_Nombre']);
                $MyUsuario->setSegundo_Nombre($Usuario['Segundo_Nombre']);
                $MyUsuario->setPrimer_Apellido($Usuario['Primer_Apellido']);
                
                $MyUsuario->setSegundo_Apellido($Usuario['Segundo_Apellido']);
                $MyUsuario->setEdad($Usuario['Edad']);
                $MyUsuario->setGenero($Usuario['Genero']);
                $MyUsuario->setDireccion($Usuario['Direccion']);
                $MyUsuario->setTelefono($Usuario['Telefono']);
                $MyUsuario->setCorreo_Electronico($Usuario['Correo_Electronico']);
                
                $MyUsuario->setDocumento_Iden($Usuario['Documento_Iden']);
                $MyUsuario->setNombreUsuario($Usuario['NombreUsuario']);
                $MyUsuario->setCorreo_Electronico($Usuario['Correo_Electronico']);


            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyUsuario;
        }

        public function ObtenerUsuarioRepetido($nombre)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            // $Sql = $Db->prepare('SELECT * FROM usuarios WHERE  NombreUsuario=:NombreUsuario'); 
            $Sql = $Db->prepare('SELECT COUNT(*) FROM usuarios WHERE  NombreUsuario=:NombreUsuario'); 
            // SELECT COUNT(*) recibe un # 
            $Sql->bindValue('NombreUsuario',$nombre);// paso 1 NombreUsuario=nombre  
            $MyUsuario = new Usuario();//Crear un objeto de tipo competencia



            try{
                $Sql->execute(); 
                $Usuario = $Sql->fetch(); 
                

            

                $coun =$Usuario[0];
                echo $coun; //se hace un eco por que espera la confirmacion o rta
                


            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyUsuario;// se va para el controlador 
        }

        public function ObtenerCorreoRepetido($correo)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT COUNT(*) FROM usuarios WHERE  Correo_Electronico=:Correo_Electronico'); 

            $Sql->bindValue('Correo_Electronico',$correo);
            $MyUsuario = new Usuario();//Crear un objeto de tipo competencia



            try{
                $Sql->execute(); 
                $Usuario = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                
                $councorreo =$Usuario[0];
                echo $councorreo;
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyUsuario;
        }


        
        public function ConfirmarCorreo($Email)
        {
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT COUNT(*) FROM usuarios WHERE  Correo_Electronico=:Correo_Electronico'); 

            $Sql->bindValue('Correo_Electronico',$Email);
            $MyUsuario = new Usuario();//Crear un objeto de tipo competencia

            try{
                $Sql->execute(); 
                $Usuario = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql

                $coun =$Usuario[0];
                echo $coun;   

                if ($coun > 0) {

                    $clave_nueva= rand(10000000,99999999);
                    
                    echo ";";
                    echo $clave_nueva;
                    echo ";";

                    $hash= password_hash($clave_nueva, PASSWORD_DEFAULT, ['cost' => 10 ]); 

                    $Sql = $Db->prepare('UPDATE usuarios SET Contrasena=:Contrasena
                    WHERE Correo_Electronico=:Correo_Electronico');

                    $Sql->bindValue('Correo_Electronico',$Email);
                    $Sql->bindValue('Contrasena',$hash);

                    try{
                        $Sql->execute(); //Ejecutar el Sql que un Update
                        // echo "Modificación Exitosa";
                    }
                    catch(Exception $e){ //Capturar Errores
                        echo $e->getMessage(); //Mostar errores en la modificación
                        die();
                    }

                    
                    $Sql = $Db->prepare('SELECT NombreUsuario FROM usuarios WHERE Correo_Electronico=:Correo_Electronico');
                    $Sql->bindValue('Correo_Electronico',$Email);

                    $Sql->execute();
                    foreach($Sql->fetchAll() as $Usuario){
                    
                    $Nombresito=$Usuario['NombreUsuario'];
                    echo $Nombresito;
                    
                     }



                }else{
                    //estos echos no borrarlos tienen su proposito
                    //el controlador esta esperando ciertos echos que estan arriba cuando se cumple pero como no se cumplio hay que mandarle algo para que no saque error en este caso le mandamos estos echos vacios
                    echo ";";
                    echo " ";
                    echo ";";
                    echo " ";
                }


            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyUsuario;
        }

        public function ModificarUsuario($Usuario){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE usuarios SET IdRol=:IdRol,Telefono=:Telefono,Direccion=:Direccion,Primer_Nombre=:Primer_Nombre,Segundo_Nombre=:Segundo_Nombre,Primer_Apellido=:Primer_Apellido,Segundo_Apellido=:Segundo_Apellido,Edad=:Edad,Genero=:Genero,Documento_Iden=:Documento_Iden
            WHERE IdUsuario=:IdUsuario'); 
            $Sql->bindValue('IdUsuario',$Usuario->getIdUsuario());
            
            // $Sql->bindValue('IdEstado',$Usuario->getIdEstado());
            $Sql->bindValue('IdRol',$Usuario->getIdRol());
            
            $Sql->bindValue('Telefono',$Usuario->getTelefono());
            $Sql->bindValue('Direccion',$Usuario->getDireccion());
            $Sql->bindValue('Primer_Nombre',$Usuario->getPrimer_Nombre());
            $Sql->bindValue('Segundo_Nombre',$Usuario->getSegundo_Nombre());
            $Sql->bindValue('Primer_Apellido',$Usuario->getPrimer_Apellido());
            $Sql->bindValue('Segundo_Apellido',$Usuario->getSegundo_Apellido());
            $Sql->bindValue('Edad',$Usuario->getEdad());
            $Sql->bindValue('Genero',$Usuario->getGenero());
            $Sql->bindValue('Documento_Iden',$Usuario->getDocumento_Iden());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarContraseña($Usuario){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE usuarios SET Contrasena=:Contrasena
            WHERE IdUsuario=:IdUsuario'); 
            $Sql->bindValue('IdUsuario',$Usuario->getIdUsuario());
            
            // echo $Usuario->getIdUsuario();
            $password=$Usuario->getContrasena();
            $hash= password_hash($password, PASSWORD_DEFAULT,['cost' => 10]);
            $Sql->bindvalue('Contrasena',$hash);
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarCliente($Usuario){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE usuarios SET Telefono=:Telefono,Direccion=:Direccion,Primer_Nombre=:Primer_Nombre,Segundo_Nombre=:Segundo_Nombre,Primer_Apellido=:Primer_Apellido,Segundo_Apellido=:Segundo_Apellido,Edad=:Edad,Genero=:Genero,Documento_Iden=:Documento_Iden 
            WHERE IdUsuario=:IdUsuario'); 
            $Sql->bindValue('IdUsuario',$Usuario->getIdUsuario());
            
            // $Sql->bindValue('IdEstado',$Usuario->getIdEstado());
            
            
            $Sql->bindValue('Telefono',$Usuario->getTelefono());
            $Sql->bindValue('Direccion',$Usuario->getDireccion());
            $Sql->bindValue('Primer_Nombre',$Usuario->getPrimer_Nombre());
            $Sql->bindValue('Segundo_Nombre',$Usuario->getSegundo_Nombre());
            $Sql->bindValue('Primer_Apellido',$Usuario->getPrimer_Apellido());
            $Sql->bindValue('Segundo_Apellido',$Usuario->getSegundo_Apellido());
            $Sql->bindValue('Edad',$Usuario->getEdad());
            $Sql->bindValue('Genero',$Usuario->getGenero());
            $Sql->bindValue('Documento_Iden',$Usuario->getDocumento_Iden());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ObtenerNombreRolUsuario($IdUsuario)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT roles.Nombre_Rol 
            FROM usuarios
            INNER JOIN  roles
            ON usuarios.IdRol = roles.IdRol
            WHERE IdUsuario=:IdUsuario'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('IdUsuario',$IdUsuario);
            $MyUsuario = new Usuario();
            $MyRol = new Roles();//Crear un objeto de tipo competencia
            
            try{
                $Sql->execute(); 
                $Nombre_Rol = $Sql->fetch(); 
                $MyRol->setNombre_Rol($Nombre_Rol['Nombre_Rol']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyRol;
        }

        public function ModificarUsuarioEstado($IdUsuario){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE usuarios SET IdEstado=:IdEstado
            WHERE IdUsuario=:IdUsuario'); 
            $Sql->bindValue('IdUsuario',$IdUsuario);
            
            // echo $IdUsuario;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $IdUsuario=explode(":",$IdUsuario);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$IdUsuario[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('IdEstado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ObtenerNombreUsuario($ID)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT usuarios.NombreUsuario 
            FROM facturas
            INNER JOIN  usuarios
            ON facturas.CodigoCliente = usuarios.IdUsuario
            WHERE CodigoCotizacion=:CodigoCotizacion'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('CodigoCotizacion',$ID);
            $MyUsuario = new Usuario();
            $MyFactura = new Factura();//Crear un objeto de tipo competencia
            
            try{
                $Sql->execute(); 
                $Nombre = $Sql->fetch(); 
                $MyUsuario->setNombreUsuario($Nombre['NombreUsuario']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyUsuario;
        }

        public function TraerUsuario(){
            $Db = Db::Conectar();
            $TraeUsuario = [];

    $Sql = $Db->query('SELECT IdUsuario,NombreUsuario,Primer_Nombre,Segundo_Nombre,Primer_Apellido,Segundo_Apellido,Documento_Iden,Direccion,Telefono FROM usuarios WHERE IdEstado=1');
    // ,Primer_Nombre,Primer_Apellido,Segundo_Apellido,Documento_Iden

            $Sql->execute();
            foreach($Sql->fetchAll() as $Usuario){
                $MyUsuario = new Usuario();
                
                $MyUsuario->setIdUsuario($Usuario['IdUsuario']);
                $MyUsuario->setNombreUsuario($Usuario['NombreUsuario']);
                $MyUsuario->setPrimer_Nombre($Usuario['Primer_Nombre']);
                $MyUsuario->setSegundo_Nombre($Usuario['Segundo_Nombre']);
                $MyUsuario->setPrimer_Apellido($Usuario['Primer_Apellido']);
                $MyUsuario->setSegundo_Apellido($Usuario['Segundo_Apellido']);
                $MyUsuario->setDocumento_Iden($Usuario['Documento_Iden']);
                $MyUsuario->setTelefono($Usuario['Telefono']);
                $MyUsuario->setDireccion($Usuario['Direccion']);

                $TraeUsuario[] = $MyUsuario;
            }
            return $TraeUsuario;
        }


        

        
}
?>