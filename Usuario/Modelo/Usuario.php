<?php
class Usuario
{
	private $IdUsuario;
	private $NombreUsuario;
	private $Contrasena;
	private $IdRol;
	private $IdEstado;
	private $Existe;//para determinar si el usuario existe o no existe en la tabla
    private $Repetido;
    private $Correo_Electronico;
    private $Telefono;
    private $Direccion;
    private $Primer_Nombre;
    private $Segundo_Nombre;
    Private $Primer_Apellido;
    private $Segundo_Apellido;
    Private $Edad;
    private $Genero;
    private $Politicas;
    private $Documento_Iden;
   

	//Definir el constructor
    public function __construct(){}

    public function setPoliticas($Politicas){
        $this->Politicas = $Politicas;
    }

    public function getPoliticas(){
        return $this->Politicas;
    }


    
    //
    public function setDocumento_Iden($Documento_Iden){
        $this->Documento_Iden = $Documento_Iden;
    }

    public function getDocumento_Iden(){
        return $this->Documento_Iden;
    }

    //
    public function setIdUsuario($IdUsuario){
    	$this->IdUsuario = $IdUsuario;
    }

    public function getIdUsuario(){
    	return $this->IdUsuario;
    }

    //
    public function setNombreUsuario($NombreUsuario){
    	$this->NombreUsuario = $NombreUsuario;
    }

    public function getNombreUsuario(){
    	return $this->NombreUsuario;
    }

    //
    public function setContrasena($Contrasena){
    	$this->Contrasena = $Contrasena;
    }

    public function getContrasena(){
    	return $this->Contrasena;
    }

    //
    public function setIdRol($IdRol){
    	$this->IdRol = $IdRol;
    }

    public function getIdRol(){
    	return $this->IdRol;
    }

    //
    public function setIdEstado($IdEstado){
    	$this->IdEstado = $IdEstado;
    }

    public function getIdEstado(){
    	return $this->IdEstado;
    }

    //
    public function setExiste($Existe){
    	$this->Existe = $Existe;
    }

    public function getExiste(){
    	return $this->Existe;
    }

    //
    public function setRepetido($Repetido){
        $this->Repetido = $Repetido;
    }

    public function getRepetido(){
        return $this->Repetido;
    }

    //
    public function setCorreo_Electronico($Correo_Electronico){
        $this->Correo_Electronico = $Correo_Electronico;
    }

    public function getCorreo_Electronico(){
        return $this->Correo_Electronico;
    }

    //
    public function setTelefono($Telefono){
        $this->Telefono = $Telefono;
    }

    public function getTelefono(){
        return $this->Telefono;
    }

    //
    public function setDireccion($Direccion){
        $this->Direccion = $Direccion;
    }

    public function getDireccion(){
        return $this->Direccion;
    }

    //
    public function setPrimer_Nombre($Primer_Nombre){
        $this->Primer_Nombre = $Primer_Nombre;
    }

    public function getPrimer_Nombre(){
        return $this->Primer_Nombre;
    }

    //
    public function setSegundo_Nombre($Segundo_Nombre){
        $this->Segundo_Nombre = $Segundo_Nombre;
    }

    public function getSegundo_Nombre(){
        return $this->Segundo_Nombre;
    }

    //
    public function setPrimer_Apellido($Primer_Apellido){
        $this->Primer_Apellido = $Primer_Apellido;
    }

    public function getPrimer_Apellido(){
        return $this->Primer_Apellido;
    }

    //
    public function setSegundo_Apellido($Segundo_Apellido){
        $this->Segundo_Apellido = $Segundo_Apellido;
    }

    public function getSegundo_Apellido(){
        return $this->Segundo_Apellido;
    }

    //
    public function setGenero($Genero){
        $this->Genero = $Genero;
    }

    public function getGenero(){
        return $this->Genero;
    }

    //
    public function setEdad($Edad){
        $this->Edad = $Edad;
    }

    public function getEdad(){
        return $this->Edad;
    }


}	

?>