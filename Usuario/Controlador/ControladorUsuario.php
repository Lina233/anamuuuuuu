
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>

<?php




// session_start();
// if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
//     header("Location:../../Index.php");
// }
require_once("../../Conexion.php");	
require_once("../Modelo/Usuario.php");
require_once("../Modelo/CrudUsuario.php");

$Usuario = new Usuario();
$CrudUsuario = new CrudUsuario();


if (isset($_POST["Acceder"])) 
{//validar que se realizo la 
	$Usuario->setNombreUsuario($_POST["NombreUsuario"]);//importante aqui estoy trabajando con los name no con los ids
	$Usuario->setContrasena($_POST["Contrasena"]);

	//var_dump($Usuario);
	$Usuario = $CrudUsuario->ValidarAcceso($Usuario);
	// var_dump($Usuario);
	//en crudusuario llamamos ala funcion validar acceso y lemetemos los datos array usuario

    if ($Usuario->getExiste() == 1) 
	{
		session_start();//inicialisar sesion
		//Definir las variables de sesion a emplear en el aplicativo
		$_SESSION["NombreUsuario"] = $Usuario->getNombreUsuario();
		$_SESSION["IdUsuario"] = $Usuario->getIdUsuario();
		$_SESSION["IdRol"] = $Usuario->getIdRol();
        $_SESSION["Primer_Nombre"] = $Usuario->getPrimer_Nombre();
        $_SESSION["Primer_Apellido"] = $Usuario->getPrimer_Apellido();
        $_SESSION["Segundo_Apellido"] = $Usuario->getSegundo_Apellido();
		header("Location:../../Producto/Vista/ListarProducto.php");

        if ($Usuario->getIdRol() == 1) 
        {
            header("Location:../../Usuario/Vista/ListarUsuarios.php");
        }
        elseif ($Usuario->getIdRol() == 2) 
        {
             header("Location:../../Usuario/Vista/ListarUsuarios.php");
        }
        elseif ($Usuario->getIdRol() == 3) 
        {
            header("Location:../../Agenda/calendarCliente/calendario.php");
        }
        
            
        

	}
	else
	{  
        echo "<script>                       
                swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Usuario o Contraseña incorrecta',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../../Index.php';
                        }
                            })</script>";
		
	}
	
}elseif(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Producto->setID_Producto($_POST["ID_Producto"]); //Instanciar el atributo
    
    $Usuario->setNombreUsuario($_POST["NombreUsuario"]); //Instanciar el atributo

    $Usuario->setContrasena($_POST["Contrasena"]); //Instanciar el atributo

    $Usuario->setIdEstado($_POST["IdEstado"]); //Instanciar el atributo

    $Usuario->setIdRol($_POST["IdRol"]); //Instanciar el atributo

     //Instanciar el atributo
    $Usuario->setCorreo_Electronico($_POST["Correo_Electronico"]);
    $Usuario->setTelefono($_POST["Telefono"]);
   	$Usuario->setDireccion($_POST["Direccion"]);
    $Usuario->setPrimer_Nombre($_POST["Primer_Nombre"]);
    $Usuario->setSegundo_Nombre($_POST["Segundo_Nombre"]);
    $Usuario->setPrimer_Apellido($_POST["Primer_Apellido"]);
    $Usuario->setEdad($_POST["Edad"]);
    $Usuario->setSegundo_Apellido($_POST["Segundo_Apellido"]);
    $Usuario->setGenero($_POST["Genero"]);
    $Usuario->setDocumento_Iden($_POST["Documento_Iden"]);
    $Usuario->setPoliticas($_POST["Politicas"]);
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudUsuario::InsertarUsuario($Usuario); // Llamar el método para Insertar


    

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Usuario registrado exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarUsuarios.php';
                  }
            })
                                  
     </script>";
        

}elseif(isset($_POST["CambiarContraseña"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Producto->setID_Producto($_POST["ID_Producto"]); //Instanciar el atributo
    
     //Instanciar el atributo

    $Usuario->setContrasena($_POST["Contrasena"]); //Instanciar el atributo

    $Usuario->setIdUsuario($_POST["IdUsuario"]); //Instanciar el atributo

    
    $CrudUsuario::ModificarContraseña($Usuario); // Llamar el método para Insertar


    

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Contraseña Modificada exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../../vistas/vistas/EditarClientes.php';
                  }
            })
                                  
     </script>";
        

}elseif(isset($_POST["RegistarCliente"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Producto->setID_Producto($_POST["ID_Producto"]); //Instanciar el atributo
    
    $Usuario->setNombreUsuario($_POST["NombreUsuario"]); //Instanciar el atributo

    $Usuario->setContrasena($_POST["Contrasena"]); //Instanciar el atributo

     //Instanciar el atributo

     //Instanciar el atributo

     //Instanciar el atributo
    $Usuario->setCorreo_Electronico($_POST["Correo_Electronico"]);
    $Usuario->setTelefono($_POST["Telefono"]);

    $Usuario->setDireccion($_POST["Direccion"]);

    $Usuario->setPrimer_Nombre($_POST["Primer_Nombre"]);
    $Usuario->setSegundo_Nombre($_POST["Segundo_Nombre"]);

    $Usuario->setPrimer_Apellido($_POST["Primer_Apellido"]);

    $Usuario->setEdad($_POST["Edad"]);
    $Usuario->setSegundo_Apellido($_POST["Segundo_Apellido"]);
    $Usuario->setGenero($_POST["Genero"]);
    $Usuario->setDocumento_Iden($_POST["Documento_Iden"]);
    $Usuario->setPoliticas($_POST["Politicas"]);
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudUsuario::InsertarCliente($Usuario); // Llamar el método para Insertar


    

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Usuario registrado exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../../Index.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    
    
    // $Usuario->setIdEstado($_POST["IdEstado"]);
    $Usuario->setIdUsuario($_POST["IdUsuario"]);
    $Usuario->setIdRol($_POST["IdRol"]);
    
    $Usuario->setTelefono($_POST["Telefono"]);
    $Usuario->setDireccion($_POST["Direccion"]);
    $Usuario->setPrimer_Nombre($_POST["Primer_Nombre"]);
    $Usuario->setSegundo_Nombre($_POST["Segundo_Nombre"]);
    $Usuario->setPrimer_Apellido($_POST["Primer_Apellido"]);
    $Usuario->setSegundo_Apellido($_POST["Segundo_Apellido"]);
    $Usuario->setEdad($_POST["Edad"]);
    $Usuario->setGenero($_POST["Genero"]);
    $Usuario->setDocumento_Iden($_POST["Documento_Iden"]);

    // echo $Producto->getNombre_Producto(); //Verificar instanciación
    $CrudUsuario::ModificarUsuario($Usuario); // Llamar el método para Modificar
    
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Usuario modificado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarUsuarios.php';
                        }
                            })</script>";


}
elseif(isset($_POST["ModificarCliente"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    
    
    // $Usuario->setIdEstado($_POST["IdEstado"]);
    $Usuario->setIdUsuario($_POST["IdUsuario"]);
    
    
    $Usuario->setTelefono($_POST["Telefono"]);
    $Usuario->setDireccion($_POST["Direccion"]);
    $Usuario->setPrimer_Nombre($_POST["Primer_Nombre"]);
    $Usuario->setSegundo_Nombre($_POST["Segundo_Nombre"]);
    $Usuario->setPrimer_Apellido($_POST["Primer_Apellido"]);
    $Usuario->setSegundo_Apellido($_POST["Segundo_Apellido"]);
    $Usuario->setEdad($_POST["Edad"]);
    $Usuario->setGenero($_POST["Genero"]);
    $Usuario->setDocumento_Iden($_POST["Documento_Iden"]);
    
    // echo $Producto->getNombre_Producto(); //Verificar instanciación
    $CrudUsuario::ModificarCliente($Usuario); // Llamar el método para Modificar
    
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Usuario modificado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                // window.location.href = '../../HojaTecnica/Vista/ListarHojaTecnicaCliente.php';
                                 window.location.href = '../../vistas/vistas/EditarClientes.php';
                        }
                            })</script>";


}
elseif(isset($_POST["Recuperar"])) //Si la petición es de Registrar
{
     //Instanciar el atributo
    $Email=($_POST["Correo_Electronico"]);
    

    //este obj start es para poder encapsular el echo que viene del crud
    ob_start();
    
    $CrudUsuario::ConfirmarCorreo($Email); 

    //aqui guardamos el eccho que viene del crud
    $salida1 = ob_get_contents();
    
    //aqui cerramos el obhect funcion que creamos para poder capturar el echo
    ob_end_clean();
    
        $salida2=$salida1;
        $salida2=explode(";",$salida2);
        //creo una variable donde guardo el digito cortado
        $DigitoNuevo1=$salida2[0];
        $DigitoNuevo2=$salida2[1];
        $DigitoNuevo3=$salida2[2];

        //aqui hago varios echos para el mensaje que voy a enviar en el email
        ob_start();
        
        echo $DigitoNuevo3;
        echo " ";
        echo "su nueva contraseña es: ";
        echo $DigitoNuevo2;

        $salidax1 = ob_get_contents();
        
        //aqui cerramos el obhect funcion que creamos para poder capturar el echo
        ob_end_clean();


    if ($DigitoNuevo1>0) {   
        
        include("../PHPMailer/src/PHPMailer.php");
        include("../PHPMailer/src/SMTP.php");
        include("../PHPMailer/src/Exception.php");    

        try
        {

            //en este enlace configuro mi correo para que permita que aplicaciones menos seguras accedan a ella
            // este enlace. https://myaccount.google.com/u/0/lesssecureapps?pli=1

            $fromemail="CorreosMantenimientoEstrada@gmail.com";
            $fromname="Mantenimiento Estrada";
            
            // $host = "smtp.live.com"; // hotmail
            // $host = "gmail.live.com"; // gmail
            $host= "smtp.gmail.com";
            $port="25";
            $SMTPAuth="login";
            $SMTPSecure="tls";
            $subject="Tu nuevo password";

            // Mensaje
            $bodyEmail=$salidax1;
            
            //contraseña del correo
            $password="stevenchif3@gmail.com";

            $mail= new PHPMailer\PHPMailer\PHPMailer();

            $mail->isSMTP();

            $mail->SMTPDebug=0;
            $mail->Host= $host;
            $mail->Port= $port;
            $mail->SMTPAuth= $SMTPAuth;
            $mail->SMTPSecure= $SMTPSecure;

            $mail->Username= $fromemail;
            $mail->Password= $password;
            $mail->setFrom($fromemail,$fromname);
            $mail->addAddress($Email);

            //asunto
            $mail->isHTML(true);
            $mail->Subject= $subject;
            $mail->Body= $bodyEmail;

            if ($mail->send()) {  
                echo "<script>
                    
                        
                    swal.fire({
                    icon: 'success',    
                    title: 'Exito revisa tu correo',
                    // text: 'Correo electronico no valido',
                    type: 'success',
                    confirmButtonText: 'Okey',
                              
                    }).then((result) => {
                      if (result.value) {
                        window.location.href = '../../Index.php';
                          }
                    })
                                                         
                </script>";


            }else{         
            }
            

        }catch(Exception $e)
        {
        }

    }else{
        
        //pagina con ejemplos de swet alert2
        // https://sweetalert2.github.io/
        echo "<script>
                    
                                
            swal.fire({
            icon: 'error',      
            title: 'Error',
            text: 'Correo electronico no valido',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../../Index.php';
                  }
            })
                                                 
        </script>";

    }



}
elseif($_GET["Accion"]=="EditarUsuario"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarUsuarios.php';
                        }
                            })</script>";




}
else //en caso contrario envio al login
{
	
		header("Location:../../Index.php");
}





?>


</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>


