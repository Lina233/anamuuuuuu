﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
// require_once('../bdd.php');
 require_once('../../Conexion.php');
require_once('../Modelo/CrudAgenda.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Agenda.php');

$CrudAgenda = new CrudAgenda(); //Crear de un objeto CrudCompetencia
$ListaAgenda = $CrudAgenda->ListarAgenda(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-Agenda.css">
    
</head>
<body>
    
    <?php 
    include('../../../vistas/navbar/dashboard.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Agenda</h1>
    <div class="form-column-boton1">    
        <button class="btn1" type="button">
            <a href="IngresarAgenda.php">Crear Agenda</a>
        </button>
    </div> 
    
    <div id="main-container">
        <table>
            <thead>
            <tr>
                <th class="th1">ID Agenda</th>
                <th class="th1">ID_Cliente</th>
                <th>Servicio</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Inicio de la Cita</th>
                <th>Acciones</th>                   
            </tr>
            </thead>

            <tbody>
            <?php
                foreach($ListaAgenda as $Agenda){
                    ?>
                    <tr>
                        <td><?php echo $Agenda->getid(); ?></td>
                        <td><?php echo $Agenda->gettitle(); ?></td>
                        <td><?php echo $Agenda->getPrimer_Nombre(); ?></td>
                        <td><?php echo $Agenda->getPrimer_Apellido(); ?></td>
                        <td><?php echo $Agenda->getdireccion(); ?></td>
                        <td><?php echo $Agenda->gettelefono(); ?></td>
                        <td><?php echo $Agenda->getstart(); ?></td>
                       
                        <!-- <td hidden=""><?php echo $Agenda->getEstado(); ?></td> -->
                        <td>
                        <a class="link2" href="EditarAgenda.php?id=<?php echo $Agenda->getid(); ?>"><img width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a>

                        <?php
                            if ($Agenda->getEstado()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorAgenda.php?id=<?php echo $Agenda->getid();?>:<?php echo $Agenda->getEstado();?>&Accion=EditarAgendaEstado"
                        ><img title="Agenda Activada" width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($Agenda->getEstado()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorAgenda.php?id=<?php echo $Agenda->getid();?>:<?php echo $Agenda->getEstado();?>&Accion=EditarAgendaEstado"
                        ><img title="Agenda Desactivada" width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                        </td>
                    </tr>
                    <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</body>
</html>