<?php
    class Agenda{
        //Parámentros de entrada
        private $IdUsuario;
        private $title;
        private $contacto;
        private $direccion;
        private $telefono;
        private $start;
        private $end;
        // private $Estado;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setid($id){
            $this->id = $id;
        }

        public function getid(){
            return $this->id;
        }

        //
        public function settitle($title){
            $this->title = $title;
        }

        public function gettitle(){
            return $this->title;
        }

        //
        public function setstart($start){
            $this->start = $start;
        }

        public function getstart(){
            return $this->start;
        }

         //
         public function setend($end){
            $this->end = $end;
        }

        public function getend(){
            return $this->end;
        }

        public function setcontacto($contacto){
            $this->contacto = $contacto;
        }

        public function getcontacto(){
            return $this->contacto;
        }

        public function setdireccion($direccion){
            $this->direccion = $direccion;
        }

        public function getdireccion(){
            return $this->direccion;
        }

        public function settelefono($telefono){
            $this->telefono = $telefono;
        }

        public function gettelefono(){
            return $this->telefono;
        }

        // public function setEstado($Estado){
        //     $this->Estado = $Estado;
        // }

        // public function getEstado(){
        //     return $this->Estado;
        // }


        
    
    }
?>