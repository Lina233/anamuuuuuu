﻿<?php
//require_once('../../Conexion.php');    
    class CrudAgenda{
    
        public function __construct(){
        }

        public function InsertarAgenda($Agenda){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO Agenda(id,title,start,end)
            VALUES(NULL,:title,:start,:end)');
 

            // $Sql->execute();
            $Sql->bindValue('title',$Agenda->gettitle());
            $Sql->bindValue('start',$Agenda->getstart());
            $Sql->bindValue('end',$Agenda->getend());
            // $Sql->bindValue('estado',$Agenda->getestado());
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }

        // public function TraerAgenda(){
        //     $Db = Db::Conectar();
        //     $TraeTipoAgenda = [];
        //     $Sql = $Db->query('SELECT id,title,start,end FROM Agenda WHERE Estado=1');
        //     $Sql->execute();
        //     foreach($Sql->fetchAll() as $Categoria){
        //         $MyCategoria = new Categoria();
        //         // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
        //         $MyCategoria->setID_Categoria($Categoria['ID_Categoria']);

        //         $MyCategoria->setNombre_Categoria($Categoria['Nombre_Categoria']);          
        //         $TraeCategoria[] = $MyCategoria;
        //     }
        //     return $TraeCategoria;
        // }

        public function ObtenerAgenda($id)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM events WHERE id=:id'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('id',$id);
            $MyAgenda = new Agenda();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Agenda = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyAgenda->setid($Agenda['id']);
                $MyAgenda->settitle($Agenda['title']);
                $MyAgenda->setstart($Agenda['start']);
                $MyAgenda->setend($Agenda['end']);
                

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyAgenda;
        }

        public function ObtenerNombreCategoriaProducto($ID_Producto)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT Categoria.Nombre_Categoria 
            FROM Producto
            INNER JOIN  Categoria
            ON Producto.ID_Categoria = Categoria.ID_Categoria
            WHERE ID_Producto=:ID_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Producto',$ID_Producto);
            $MyProducto = new Producto();
            $MyCategoria = new Categoria();//Crear un objeto de tipo competencia
            
            try{
                $Sql->execute(); 
                $Nombre_Categoria = $Sql->fetch(); 
                $MyCategoria->setNombre_Categoria($Nombre_Categoria['Nombre_Categoria']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyCategoria;
        }

        //Listar todos los registros de la tabla
        public function ListarAgenda(){
            $Db = Db::Conectar();
            $ListaAgenda = [];
            $Sql = $Db->query('SELECT * FROM events');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Agenda){
                $MyAgenda = new Agenda();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyAgenda->setIdUsuario($Agenda['IdUsuario']);
                $MyAgenda->settitle($Agenda['title']);
                $MyAgenda->setcontacto($Agenda['contacto']);
                $MyAgenda->setdireccion($Agenda['direccion']);
                $MyAgenda->settelefono($Agenda['telefono']);
                $MyAgenda->setstart($Agenda['start']);
                $MyAgenda->setend($Agenda['end']);
                // $MyAgenda->setEstado($Agenda['Agenda']);
                $ListaAgenda[] = $MyAgenda;
            }
            return $ListaAgenda;
        }

         

        public function ModificarCategoria($Categoria){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE Categoria SET Nombre_Categoria=:Nombre_Categoria
            WHERE ID_Categoria=:ID_Categoria'); 
            $Sql->bindValue('ID_Categoria',$Categoria->getID_Categoria());
            $Sql->bindValue('Nombre_Categoria',$Categoria->getNombre_Categoria());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarCategoriaEstado($ID_Categoria){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE Categoria SET Estado=:Estado
            WHERE ID_Categoria=:ID_Categoria'); 
            $Sql->bindValue('ID_Categoria',$ID_Categoria);
            
            // echo $ID_Producto;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_Categoria=explode(":",$ID_Categoria);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_Categoria[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('Estado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }    

        public function EliminarTipoProducto($ID_Tipo_Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM tipo_producto WHERE ID_Tipo_Producto=:ID_Tipo_Producto'); 
            $Sql->bindValue('ID_Tipo_Producto',$ID_Tipo_Producto);
            // $ID_Producto = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>