﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>



<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}


require_once('../../Conexion.php');
require_once('../Modelo/Agenda.php'); //Vincular la Clase Competencia
require_once('../Modelo/CrudAgenda.php'); //Vincular la Clase Crud

$Agenda = new Agenda(); //Crear el objeto Competencia
$CrudAgenda = new CrudAgenda();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Tipo_Producto->setID_Tipo_Producto($_POST["ID_Tipo_Producto"]); //Instanciar el atributo
    
    $Agenda->setid($_POST["id"]); //Instanciar el atributo
    $Agenda->settitle($_POST["title"]); //Instanciar el atributo
    $Agenda->setstart($_POST["start"]); //Instanciar el atributo
    $Agenda->setend($_POST["end"]); //Instanciar el atributo

    

   
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudAgenda::InsertarAgenda($Agenda); // Llamar el método para Insertar

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Agenda registrada exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarAgenda.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    $Agenda->setid($_POST["id"]); //Instanciar el atributo
    $Agenda->setstart($_POST["start"]); //Instatnciar el atributo
    $Agenda->setend($_POST["end"]); //Instatnciar el atributo
    $Agenda->settitle($_POST["title"]); //Instatnciar el atributo
    $CrudAgenda::Modificar($Agenda); // Llamar el método para Modificar

    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'title modificada exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarAgenda.php';
                        }
                            })</script>";


}elseif($_GET["Accion"]=="EditarAgendaEstado"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudAgenda::ModificarAgendaEstado($_GET["id"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarAgenda.php';
                        }
                            })</script>";
}
elseif($_GET["Accion"]=="EliminarProducto"){
    $CrudTipoProducto::EliminarTipoProducto($_GET["ID_Tipo_Producto"]); // Llamar el método para Modificar
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Producto Eliminado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarAgenda.php';
                        }
                            })</script>";
}

?>

</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>