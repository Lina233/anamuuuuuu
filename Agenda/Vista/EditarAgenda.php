<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}
require_once('../../Conexion.php');
require_once('../Modelo/CrudAgenda.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Agenda.php');

$CrudAgenda = new CrudAgenda(); //Crear un Objeto CrudCompetencia
$Agenda = $CrudAgenda::ObtenerAgenda($_GET["id"]);

// $CrudCategoria = new CrudCategoria(); //Crear de un objeto CrudCompetencia
 $TraerAgenda = $CrudAgenda->TraerAgenda();



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Modificar</title>
    
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="CSS/estilos-editar-Agenda.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <!-- para poner fotos en los formukarios --> 
    <!-- enctype="multipart/form-data" -->

    <div class="diiv-1">
        <form action="../Controlador/ControladorAgenda.php" method="post">
            <h1 class="titulo1">Editar Agenda</h1> 
            <div class="diiv-2">
                    <!-- id oculto -->   
                    <input  hidden="" readonly="disabled" type="text" name="id" id="id" value="<?php echo $Agenda->getid(); ?>">
                    
                    <p>Nombre</p>
                    <input  pattern="[A-Za-z -ñÑ-09]+"  readonly="disabled" type=""  name="Primer_Nombre"class="form-control" id="Primer_Nombre"value="<?php echo $Agenda->getPrimer_Nombre(); ?>">
                    <br>
                    
                    <p>Segundo Apellido</p>
                    <input  pattern="[A-Za-z -ñÑ-09]+"  readonly="disabled" type=""  name="Segundo_Apellido"class="form-control" id="Segundo_Apellido"value="<?php echo $Agenda->getSegundo_Apellido(); ?>">
                    <br>
                
                    <p>Servicio</p>
                    <select name="title" id="title" value="<?php echo $Agenda->gettitle(); ?>" >
                    <option  value="<?php echo $Agenda->gettitle(); ?>"><?php echo $Agenda->gettitle(); ?></option>
                            <option value="Instalación">Instalación</option>
                            <option value="Mantenimiento">Mantenimiento</option>
                            <option  value="Cotización">Cotización</option>						  
                    </select>	
                    <br>
                    <p>Dirección</p>
                    <input pattern="[A-Za-z -ñÑ-09]+" required type="text" name="direccion" id="direccion"class="form-control" id="direccion"value="<?php echo $Agenda->getdireccion(); ?>">
                    <br>
                    <p>Teléfono</p>
                    <input onkeyup="activar_boton()" required pattern="[0-9]+" value="<?php echo $Agenda->gettelefono(); ?>"type="text" name="telefono"  id="telefono">
                    <br>

            </div> 
            <div class="diiv-3">
            <p>Primer Apellido</p>
                    <input  pattern="[A-Za-z -ñÑ-09]+"  readonly="disabled" type=""  name="Primer_Apellido"class="form-control" id="Primer_Apellido"value="<?php echo $Agenda->getPrimer_Apellido(); ?>">
            <br>
                <!--funcion para concatenar apellidos
                <?php 
                // $str = <<<EOD
               
                // EOD;
            //    echo $_SESSION['Primer_Nombre']."\n". $_SESSION['Primer_Apellido'];// concatenar el nombre y apellido del mismo vector sesion
            //    $a= $_SESSION['Primer_Apellido']."\n". $_SESSION['Segundo_Apellido'];
            //    var_dump($a);
                //  echo $a;

                //  "$a"
               

                // ?> -->
                    <!-- <input  pattern="[A-Za-z -ñÑ-09]+" type=""  name="NombreUsuario"class="form-control" id="NombreUsuario"value="<?php echo $a; ?>"> -->
                    
                <p>Color</p>
                <select name="color" class="form-control" id="color"value="<?php echo $Agenda->getcolor(); ?>">
                        <!-- <option value="">Seleccionar</option> -->
                        <option style="color:#0071c5;" value="Azul oscuro">&#9724; Azul oscuro</option>
                        <option style="color:#40E0D0;" value="#Turquesa">&#9724; Turquesa</option>
                        <option style="color:#008000;" value="#Verde">&#9724; Verde</option>						  
                        <option style="color:#FFD700;" value="#Amarillo">&#9724; Amarillo</option>
                        <option style="color:#FF8C00;" value="#Naranja">&#9724; Naranja</option>
                        <option style="color:#FF0000;" value="#Rojo">&#9724; Rojo</option>
                        <option style="color:#000;" value="#Negro">&#9724; Negro</option>
                </select>
                <br>
                <p>Hora inicial</p>
                <input type="text" name="start" id="start"value="<?php echo $Agenda->getstart(); ?>">
                <br>
                <p>Hora Final</p>
                <input type=""  name="end"class="form-control" id="end"value="<?php echo $Agenda->getend(); ?>">
                <br>
                
                <p>Estado</p>   
                        <!-- <input  readonly="disabled" type="text" name="estado" class="form-control" id="estado" value="Abierto"> -->
                        <select name="estado" type="text" class="form-control" id="estado"value="<?php echo $Agenda->getestado(); ?>">
                            <!-- <option value="">Seleccionar</option> -->
                            <option  value="<?php echo $Agenda->getestado(); ?>"><?php echo $Agenda->getestado(); ?></option>
                            <option style="color:#0071c5;" value="Abierto">&#9724;Abierto</option>
                            <option style="color:#008000;" value="En Proceso">&#9724;En Proceso</option>
                            <option style="color:#40E0D0;" value="Cerrado">&#9724;Cerrado</option>
                        </select>
                                  <!-- <input  readonly="disabled" type="text" name="estado" class="form-control" id="estado" value="Abierto"> -->
                <!-- <select name="estado" type="text" class="form-control" id="estado"value="<?php echo $Agenda->getestado(); ?>">
                                    < <option value="">Seleccionar</option> -->
                                    <!-- <option style="color:#0071c5;" value="Abierto">&#9724;Abierto</option>
                                    <option style="color:#008000;" value="En Proceso">&#9724;En Proceso</option>
                                    <option style="color:#40E0D0;" value="Cerrado">&#9724;Cerrado</option> -->
                <!-- </select> -->
                <br>
            </div>  
                <!-- <button  type="button" name="Agreg" id="Agreg" >Agregar</button> -->
                <!-- <input type="hidden" name="Agreg" id="Agreg">
                <button type="button">Modificar</button> -->
                <br>
                <div class="form-column-boton1">
                    <input type="hidden" name="Modificar" id="Modificar">
                    <button class="btn1" type="submit">Modificar</button>
                </div>
                <div class="form-column-boton2">
                    <button class="btn2" type="button">
                        <a href="ListarAgenda.php">Cancelar</a>
                    </button>
                </div>
                <br>
        
        </form>
    </div>
</body>

<script>
// $(document).ready(function(){


//     $("#Agreg").click(function(){
//         let validado=0;
//         //Validación del tipo documento
        
        


        

//         //Validación del PrecioProducto
//         if($("#ValorUnitario").val().length == 0 ){
//          $("#validacion_ValorUnitario").text("Precio obligatorio");
//         }
//         else
//         {
//          $("#validacion_ValorUnitario").text("");
//          validado++;
//         }

//         //Validación del CantidadProducto
//         if($("#ProductoCantidad").val().length == 0 ){
//          $("#validacion_ProductoCantidad").text("Cantidad Obligatoria");
//         }
//         else
//         {
//          $("#validacion_ProductoCantidad").text("");
//          validado++;
//         }
        

        
//         // validacion todos los campos
//         if(validado==2) //Si validado == al número de campos que requieren validación entocnces se registra
//          {

//             // AgregarDetalle();
              
           
//             alert("Registro exitosddddo");
//             //Limpiar cajas de texto, select, textarea, checkbox, radiobutton
            

           
            

            
//          }
//     });
// })

</script>
</html>