<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}
require_once('../../Conexion.php');
require_once('../../Usuario/Modelo/Usuario.php');
require_once('../../Usuario/Modelo/CrudUsuario.php');
$CrudUsuario = new CrudUsuario(); 
$TraerUsuario = $CrudUsuario->TraerUsuario();

require_once('../../Servicio/Modelo/Servicio.php');
require_once('../../Servicio/Modelo/CrudServicio.php');
$CrudServicio = new CrudServicio(); 
$TraerServicio = $CrudServicio->TraerServicios();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="CSS/estilos-ingresar-agenda.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <div class="diiv-1">
    <!-- formulario -->
      <form class="form-1" action="../Controlador/Controladoragenda.php" method="post">
            <h1 class="titulo1" >Separar Cita</h1>
            
            <div class="diiv-3">
                        <!-- ID Tipo Producto: <input required pattern="[0-9]+" type="text" name="ID_Tipo_Producto" id="ID_Tipo_Producto"> -->
                            <p>ID Usuario</p>
                            <select name="IdUsuario" id="IdUsuario">       
                                    <option></option>       
                                
                                <?php
                            foreach($TraerUsuario as $Usuario1){
                                ?>            
                                <option 

                                value="<?php
                                echo $Usuario1->getIdUsuario();
                                echo ";";
                                echo $Usuario1->getPrimer_Nombre();
                                echo ";"; 
                                echo $Usuario1->getPrimer_Apellido();
                                echo ";";
                                echo $Usuario1->getSegundo_Apellido();
                                echo ";";
                                echo $Usuario1->getDireccion();
                                echo ";";
                                echo $Usuario1->getTelefono();
                                
                                ?>">
                                    <?php

                                    echo $Usuario1->getNombreUsuario();
                                    

                                    ?>
                                    
                                </option>             
                                <?php
                                
                                
                        }
                        ?>
                        
                        </select>
                        <label id="validacion_CodigoCliente" class="alerta"></label>


                        
                        <p>Primer Nombre</p>
                        <input pattern="[A-Za-z´-ñÑ-]+" type=""  required name="Primer_Nombre" id="Primer_Nombre">
                                                                        
                        
                        <label id="validacion_CodigoCliente" class="alerta"></label>

                        <br>

                        
                           <!-- pone un espacio option   -->
                              <!-- <option></option>        -->
                            
                        
                        <label id="validacion_CodigoCliente" class="alerta"></label>
								
                                              

                        <p>Segundo Apellido</p>
                        <input pattern="[A-Za-z´-ñÑ-]+" type=""  required name="Segundo_Apellido" id="Segundo_Apellido">
                        <label id="validacion_CodigoCliente" class="alerta"></label>
        
                        <br>
                                                                 
                        <p>Teléfono</p>
                        <input pattern="[0-9]{8,20}" type=""  required name="telefono" id="telefono">
                        <br>
                        <p>Hora inicial</p>
                        <input type="text" name="start" id="start">
                        <br>
                        <p>Estado</p>   
                        <!-- <input  readonly="disabled" type="text" name="estado" class="form-control" id="estado" value="Abierto"> -->
                        <select name="estado" type="text" class="form-control" id="estado">
                            <!-- <option value="">Seleccionar</option> -->
                            <option style="color:#0071c5;" value="Abierto">&#9724;Abierto</option>
                            <option style="color:#008000;" value="EnProceso">&#9724;En Proceso</option>
                            <option style="color:#40E0D0;" value="Cerrado">&#9724;Cerrado</option>
                        </select>
                            <br>
         </div>
          
         <div class="diiv-2">
                        
                        <p>Servicio</p>
                        <select name="title" id="title" require>
                                <!-- <option value="0">Seleccionar</option> -->
                                <option value="Instalación">Instalación</option>
                                <option value="Mantenimiento">Mantenimiento</option>
                                <option  value="Cotización">Cotización</option>						  
                        </select>	
                       
                        <p>Primer Apellido</p>
                        <input pattern="[A-Za-z´-ñÑ-]+" type=""  required name="Primer_Apellido" id="Primer_Apellido">
                        <br>
                        <p>dirección</p>
                        <input pattern="[A-Za-z -ñÑ-09]+" required type="text" name="direccion" id="direccion">
                        
                        <p>Color</p>
                        <select name="color" class="form-control" id="color">
                                <!-- <option value="">Seleccionar</option> -->
                                <option style="color:#0071c5;" value="Azul oscuro">&#9724; Azul oscuro</option>
                                <option style="color:#40E0D0;" value="#Turquesa">&#9724; Turquesa</option>
                                <option style="color:#008000;" value="#Verde">&#9724; Verde</option>						  
                                <option style="color:#FFD700;" value="#Amarillo">&#9724; Amarillo</option>
                                <option style="color:#FF8C00;" value="#Naranja">&#9724; Naranja</option>
                                <option style="color:#FF0000;" value="#Rojo">&#9724; Rojo</option>
                                <option style="color:#000;" value="#Negro">&#9724; Negro</option>
                        </select>
                        
                        
                        
                        <br>
                        
                        <p>Hora Final</p>
                        <input type=""  name="end"class="form-control" id="end">
                        <br>
                        
                        
                        <br>
            </div>
           
                <div class="form-column-boton1">
                    <input type="hidden" name="Registrar" id="Registrar">
                    <button class="btn1" type="submit">Registrar</button>
                </div> 
                <div class="form-column-boton2">
                    <button class="btn2" type="button">
                        <a href="ListarAgenda.php">Cancelar</a>
                    </button>
                </div>
        </form>
    </div>
    <script src="js/funciones.js"></script>
</body>
<script >
    

   $(document).ready(function(){

        

            

        $('#IdUsuario').on('change',function(){
                                
                                //coger el value del option seleccionado
                                var CodigoCliente=$('#IdUsuario').val(); 

                                var arr =CodigoCliente.split(";");
                                    $("#IdUsuario1").val(arr[0]);
                                    $("#Primer_Nombre").val(arr[1]);
                                    $("#Primer_Apellido").val(arr[2]);
                                    $("#Segundo_Apellido").val(arr[3]);
                                    $("#direccion").val(arr[4]);
                                    $("#telefono").val(arr[5]);
                                    
                                    
                                    

                    });
        
        
    
})
</script>



</html>
