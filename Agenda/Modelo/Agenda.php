<?php
    class Agenda{
        //Parámentros de entrada
        private $id; //agenda
        private $IdUsuario;//cliente
        private $title;
        // private $a;
        private $color;
        private $direccion;
        private $telefono;
        private $start;
        private $end;
        private $estado;
        private $Primer_Nombre;
        private $Primer_Apellido;
        private $Segundo_Apellido;
    
        
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        
        public function setid($id){
            $this->id = $id;
        }

        public function getid(){
            return $this->id;
        }

        public function setIdUsuario($IdUsuario){
            $this->IdUsuario = $IdUsuario;
        }

        public function getIdUsuario(){
            return $this->IdUsuario;
        }

        public function setPrimer_Nombre($Primer_Nombre){
            $this->Primer_Nombre = $Primer_Nombre;
        }

        public function getPrimer_Nombre(){
            return $this->Primer_Nombre;
        }


         public function setPrimer_Apellido($Primer_Apellido){
            $this->Primer_Apellido = $Primer_Apellido;
        }

        public function getPrimer_Apellido(){
            return $this->Primer_Apellido;
        }
        
        public function setSegundo_Apellido($Segundo_Apellido){
            $this->Segundo_Apellido = $Segundo_Apellido;
        }

        public function getSegundo_Apellido(){
            return $this->Segundo_Apellido;
        }
        

        public function settitle($title){
            $this->title = $title;
        }

        public function gettitle(){
            return $this->title;
        }

        //
        public function setstart($start){
            $this->start = $start;
        }

        public function getstart(){
            return $this->start;
        }

         //
         public function setend($end){
            $this->end = $end;
        }

        public function getend(){
            return $this->end;
        }

        // public function seta($a){
        //     $this->a = $a;
        // }

        // public function geta(){
        //     return $this->a;
        // }

        public function setdireccion($direccion){
            $this->direccion = $direccion;
        }

        public function getdireccion(){
            return $this->direccion;
        }

        public function settelefono($telefono){
            $this->telefono = $telefono;
        }

        public function gettelefono(){
            return $this->telefono;
        }

        public function setestado($estado){
            $this->estado = $estado;
        }

        public function getestado(){
            return $this->estado;
        }

        public function setcolor($color){
            $this->color = $color;
        }

        public function getcolor(){
            return $this->color;
        }


        
    
    }
?>