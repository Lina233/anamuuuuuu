﻿<?php
//require_once('../../Conexion.php');    
    class CrudAgenda{
    
        public function __construct(){
        }

        public function InsertarAgenda($Agenda){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO events(id,IdUsuario,Primer_Nombre,Primer_Apellido,Segundo_Apellido,title,color,direccion,telefono,estado,start,end)
            VALUES(NULL,:IdUsuario,:Primer_Nombre,:Primer_Apellido,:Segundo_Apellido,:title,:color,:direccion,:telefono,:estado,:start,:end)');
 

            // $Sql->execute();
            $Sql->bindValue('IdUsuario',$Agenda->getIdUsuario());
            $Sql->bindValue('Primer_Nombre',$Agenda->getPrimer_Nombre());
            $Sql->bindValue('Primer_Apellido',$Agenda->getPrimer_Apellido());
            $Sql->bindValue('Segundo_Apellido',$Agenda->getSegundo_Apellido());
            $Sql->bindValue('title',$Agenda->gettitle());
            $Sql->bindValue('color',$Agenda->getcolor());
            $Sql->bindValue('direccion',$Agenda->getdireccion());
            $Sql->bindValue('telefono',$Agenda->gettelefono());
            $Sql->bindValue('start',$Agenda->getstart());
            $Sql->bindValue('end',$Agenda->getend());
            $Sql->bindValue('estado',$Agenda->getestado());
            // $MyAgenda->seta($Agenda['a']);
            
            
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }
        
        //funcion para traer estado en el editar 

        public function TraerAgenda(){
            $Db = Db::Conectar();
            $TraeTipoProductos = [];
            $Sql = $Db->query('SELECT id,estado FROM events WHERE id=id');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Agenda){
                $MyAgenda = new Agenda();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyAgenda->setid($Agenda['id']);

                $MyAgenda->setestado($Agenda['estado']);          
                $TraeAgenda[] = $MyAgenda;
            }
            return $TraeAgenda;
        }



        public function ObtenerAgenda($id)
        { //Código para obtener agenda y mostrarla en listar 
            $Db = Db::Conectar();
           
            $Sql = $Db->prepare('SELECT * FROM events WHERE id=:id'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('id',$id);
           
            $MyAgenda = new Agenda();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Agenda = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyAgenda->setid($Agenda['id']);
                $MyAgenda->settitle($Agenda['title']);
                $MyAgenda->setPrimer_Nombre($Agenda['Primer_Nombre']);
                $MyAgenda->setPrimer_Apellido($Agenda['Primer_Apellido']);
                $MyAgenda->setSegundo_Apellido($Agenda['Segundo_Apellido']);
                $MyAgenda->setdireccion($Agenda['direccion']);
                $MyAgenda->settelefono($Agenda['telefono']);
                $MyAgenda->setcolor($Agenda['color']);
                $MyAgenda->setstart($Agenda['start']);
                $MyAgenda->setend($Agenda['end']);
                $MyAgenda->setestado($Agenda['estado']);
  
                // $MyAgenda->seta($Agenda['a']);
                          

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyAgenda;
        }

        // public function ObtenerNombreCategoriaProducto($ID_Producto)
        // { //Código para obtener una Competecia
        //     $Db = Db::Conectar();
        //     $Sql = $Db->prepare('SELECT Categoria.Nombre_Categoria 
        //     FROM Producto
        //     INNER JOIN  Categoria
        //     ON Producto.ID_Categoria = Categoria.ID_Categoria
        //     WHERE ID_Producto=:ID_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
        //     $Sql->bindValue('ID_Producto',$ID_Producto);
        //     $MyProducto = new Producto();
        //     $MyCategoria = new Categoria();//Crear un objeto de tipo competencia
            
        //     try{
        //         $Sql->execute(); 
        //         $Nombre_Categoria = $Sql->fetch(); 
        //         $MyCategoria->setNombre_Categoria($Nombre_Categoria['Nombre_Categoria']);
                
        //     }
        //     catch(Exception $e){ //Capturar Errores
        //         echo $e->getMessage(); //Mostar errores en la modificiación
        //         die();
        //     }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

        //     return $MyCategoria;
        // }

        //Listar todos los registros de la tabla
        public function ListarAgenda(){
            $Db = Db::Conectar();
            $ListaAgenda = [];
            $Sql = $Db->query('SELECT * FROM events');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Agenda){
                $MyAgenda = new Agenda();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyAgenda->setid($Agenda['id']);
                $MyAgenda->setIdUsuario($Agenda['IdUsuario']);
                $MyAgenda->setPrimer_Nombre($Agenda['Primer_Nombre']);
                $MyAgenda->setPrimer_Apellido($Agenda['Primer_Apellido']);
                $MyAgenda->settitle($Agenda['title']);                             
                $MyAgenda->setdireccion($Agenda['direccion']);
                $MyAgenda->settelefono($Agenda['telefono']);
                $MyAgenda->setstart($Agenda['start']);
                $MyAgenda->setestado($Agenda['estado']);
                
                $ListaAgenda[] = $MyAgenda;
            }
            return $ListaAgenda;
        }

      //editar agenda    
            public function ModificarAgenda($Agenda){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE events SET id=:id,Primer_Nombre=:Primer_Nombre,Primer_Apellido=:Primer_Apellido,Segundo_Apellido=:Segundo_Apellido, title=:title,direccion=:direccion,telefono=:telefono,estado=:estado,color=:color,start=:start,end=:end
            WHERE id=:id');
            $Sql->bindValue('id',$Agenda->getid()); 
            $Sql->bindValue('title',$Agenda->gettitle());
            $Sql->bindValue('Primer_Nombre',$Agenda->getPrimer_Nombre());
            $Sql->bindValue('Primer_Apellido',$Agenda->getPrimer_Apellido());
            $Sql->bindValue('Segundo_Apellido',$Agenda->getSegundo_Apellido());
            $Sql->bindValue('direccion',$Agenda->getdireccion());
            $Sql->bindValue('telefono',$Agenda->gettelefono());
            $Sql->bindValue('estado',$Agenda->getestado());
            $Sql->bindValue('color',$Agenda->getcolor());
            $Sql->bindValue('start',$Agenda->getstart());
            $Sql->bindValue('end',$Agenda->getend());
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }
        
        //funcion reportes.
        public function ListarAgendapdf(){
            $Db = Db::Conectar();
            $ListaAgenda = []; // define un vector 
            $Sql = $Db->query('SELECT * FROM events');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Agenda){
                
                $MyAgenda = new Agenda();
                // echo $Factura['ID_Producto']."----".$Factura['Nombre_Producto'];
                $MyAgenda->setid($Agenda['id']);
                $MyAgenda->setIdUsuario($Agenda['IdUsuario']);
                $MyAgenda->setPrimer_Nombre($Agenda['Primer_Nombre']);
                $MyAgenda->setPrimer_Apellido($Agenda['Primer_Apellido']);
                $MyAgenda->settitle($Agenda['title']);                             
                $MyAgenda->setdireccion($Agenda['direccion']);
                $MyAgenda->settelefono($Agenda['telefono']);
                $MyAgenda->setstart($Agenda['start']);
                $MyAgenda->setestado($Agenda['estado']);
                       
 
                

                $ListaAgenda[] = $MyAgenda;
            }
            return $ListaAgenda;
        }

        // public function ModificarCategoriaEstado($ID_Categoria){
        //     $Db = Db::Conectar(); 
        //     $Sql = $Db->prepare('UPDATE Categoria SET Estado=:Estado
        //     WHERE ID_Categoria=:ID_Categoria'); 
        //     $Sql->bindValue('ID_Categoria',$ID_Categoria);
            
        //     // echo $ID_Producto;
            
            
        //     //utilizo explode corto el string donde estan los :
        //     // es el cortador
        //     $ID_Categoria=explode(":",$ID_Categoria);
        //     //creo una variable donde guardo el digito cortado
        //     $DigitoNuevo2=$ID_Categoria[1];

        //     //condicional que lee si es uno o dos y les da un nuevo valor
        //     if ($DigitoNuevo2==1) {
        //         $NuevoEstado=2;
        //     }elseif ($DigitoNuevo2==2) {
        //         $NuevoEstado=1;
        //     }

        //     //asigno nuevoestado al campo idestado de la sentencia sql
        //     $Sql->bindValue('Estado',$NuevoEstado);

        //     try{
        //         $Sql->execute(); //Ejecutar el Sql que un Update
        //         // echo "Modificación Exitosa";
        //     }
        //     catch(Exception $e){ //Capturar Errores
        //         echo $e->getMessage(); //Mostar errores en la modificación
        //         die();
        //     }
        // }    

        public function EliminarAgenda($id){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM events WHERE id=:id'); 
            $Sql->bindValue('id',$id);
            // $ID_Producto = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                // echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>