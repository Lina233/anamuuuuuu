﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>



<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}


require_once('../../Conexion.php');
require_once('../Modelo/Tipo_Planta.php'); //Vincular la Clase Competencia
require_once('../Modelo/Crud_Tipo_Planta.php'); //Vincular la Clase Crud

$Tipo_Planta = new Tipo_Planta(); //Crear el objeto Competencia
$CrudTipo_Planta = new CrudTipo_Planta();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Tipo_Planta->setID_Tipo_Planta($_POST["ID_Tipo_Planta"]); //Instanciar el atributo
    
    $Tipo_Planta->setNombre_Tipo_Planta($_POST["Nombre_Tipo_Planta"]); //Instanciar el atributo

    

   
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudTipo_Planta::InsertarTipoPlanta($Tipo_Planta); // Llamar el método para Insertar

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Planta registrado exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarTipoPlanta.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    $Tipo_Planta->setID_Tipo_Planta($_POST["ID_Tipo_Planta"]); //Instanciar el atributo
    $Tipo_Planta->setNombre_Tipo_Planta($_POST["Nombre_Tipo_Planta"]); //Instanciar el atributo
    
    $CrudTipo_Planta::ModificarTipoPlanta($Tipo_Planta); // Llamar el método para Modificar

    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Tipo Planta modificado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarTipoPlanta.php';
                        }
                            })</script>";


}elseif($_GET["Accion"]=="EditarTipoPlantaEstado"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudTipo_Planta::ModificarTipoPlantaEstado($_GET["ID_Tipo_Planta"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarTipoPlanta.php';
                        }
                            })</script>";
}
// elseif($_GET["Accion"]=="EliminarPlanta"){
//     $CrudTipoPlanta::EliminarTipoPlanta($_GET["ID_Tipo_Planta"]); // Llamar el método para Modificar
//     echo "<script>                       
//                 swal.fire({
//                     title: 'Éxito',
//                     text: 'Planta Eliminado exitosamente',
//                     type: 'success',
//                     confirmButtonText: 'Okey',
                        
//                     }).then((result) => {
//                         if (result.value) {
//                                 window.location.href = '../Vista/ListarTipoPlanta.php';
//                         }
//                             })</script>";
// }

?>

</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>