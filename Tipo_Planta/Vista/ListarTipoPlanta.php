﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/Crud_Tipo_Planta.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Tipo_Planta.php');

$CrudTipo_Planta = new CrudTipo_Planta(); //Crear de un objeto CrudCompetencia
$ListaTipo_Planta = $CrudTipo_Planta->ListarTipo_Planta(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-tipo-planta.css">
    
</head>
<body>
    
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Tipo Planta</h1>
    <div class="form-column-boton1">    
        <button class="btn1" type="button">
            <a href="IngresarTipoPlanta.php">Crear Tipo Planta</a>
        </button>
    </div> 
    
    <div id="main-container">
        <table>
            <thead>
            <tr>
                <th class="th1">ID Tipo Planta</th>
                <th>Nombre Tipo Planta</th>
                <th>Acciones</th>  
            </tr>
            </thead>

            <tbody>
            <?php
                 foreach($ListaTipo_Planta as $Tipo_Planta){
                    ?>
                    <tr>
                    <td><?php echo $Tipo_Planta->getID_Tipo_Planta(); ?></td>
                    <td><?php echo $Tipo_Planta->getNombre_Tipo_Planta(); ?></td>
                    <td hidden=""><?php echo $Tipo_Planta->getEstado(); ?></td>                           
                    <td>
                                
                        <a class="link2" href="EditarTipoPlanta.php?ID_Tipo_Planta=<?php echo $Tipo_Planta->getID_Tipo_Planta(); ?>"><img title="Editar" width="40px" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a>

                        <?php
                            if ($Tipo_Planta->getEstado()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorTipoPlanta.php?ID_Tipo_Planta=<?php echo $Tipo_Planta->getID_Tipo_Planta();?>:<?php echo $Tipo_Planta->getEstado();?>&Accion=EditarTipoPlantaEstado"
                        ><img title="Tipo Planta Activado" width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($Tipo_Planta->getEstado()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorTipoPlanta.php?ID_Tipo_Planta=<?php echo $Tipo_Planta->getID_Tipo_Planta();?>:<?php echo $Tipo_Planta->getEstado();?>&Accion=EditarTipoPlantaEstado"
                        ><img title="Tipo Planta Desactivado" width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                    </td>
                    </tr>
                    <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</body>
</html>