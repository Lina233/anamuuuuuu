<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

require_once('../../Conexion.php');
require_once('../Modelo/Tipo_Planta.php');
require_once('../Modelo/Crud_Tipo_Planta.php');

$CrudTipo_Planta = new CrudTipo_Planta(); //Crear un Objeto CrudCompetencia
$Tipo_Planta = $CrudTipo_Planta::ObtenerTipo_Planta($_GET["ID_Tipo_Planta"]);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Modificar</title>
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-editar-tipo-planta.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <!-- para poner fotos en los formukarios --> 
    <!-- enctype="multipart/form-data" -->

    
    <form action="../Controlador/ControladorTipoPlanta.php" method="post">
        <h1 class="titulo1">Editar Tipo Planta</h1> 
        <br>
        <!-- <p>ID Tipo Planta</p>  -->  
        <input hidden="" readonly="disabled" type="text" name="ID_Tipo_Planta" id="ID_Tipo_Planta" value="<?php echo $Tipo_Planta->getID_Tipo_Planta(); ?>">
        
        <p>Nombre</p>
        <input  pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Nombre_Tipo_Planta" id="Nombre_Tipo_Planta" value="<?php echo $Tipo_Planta->getNombre_Tipo_Planta(); ?>">
        <br>
        
        <!-- <button  type="button" name="Agreg" id="Agreg" >Agregar</button> -->
        <!-- <input type="hidden" name="Agreg" id="Agreg">
        <button type="button">Modificar</button> -->
        <br>
        <div class="form-column-boton1">
            <input type="hidden" name="Modificar" id="Modificar">
            <button class="btn1" type="submit">Modificar</button>
        </div>
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarTipoPlanta.php">Cancelar</a>
            </button>
        </div>
    </form>
</body>

<script>
// $(document).ready(function(){


//     $("#Agreg").click(function(){
//         let validado=0;
//         //Validación del tipo documento
        
        


        

//         //Validación del PrecioProducto
//         if($("#ValorUnitario").val().length == 0 ){
//          $("#validacion_ValorUnitario").text("Precio obligatorio");
//         }
//         else
//         {
//          $("#validacion_ValorUnitario").text("");
//          validado++;
//         }

//         //Validación del CantidadProducto
//         if($("#ProductoCantidad").val().length == 0 ){
//          $("#validacion_ProductoCantidad").text("Cantidad Obligatoria");
//         }
//         else
//         {
//          $("#validacion_ProductoCantidad").text("");
//          validado++;
//         }
        

        
//         // validacion todos los campos
//         if(validado==2) //Si validado == al número de campos que requieren validación entocnces se registra
//          {

//             // AgregarDetalle();
              
           
//             alert("Registro exitosddddo");
//             //Limpiar cajas de texto, select, textarea, checkbox, radiobutton
            

           
            

            
//          }
//     });
// })

</script>
</html>