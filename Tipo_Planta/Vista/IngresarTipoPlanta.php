<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
// $CrudProducto = new CrudProducto(); //Crear de un objeto CrudCompetencia
// $TraerProductos = $CrudProducto->TraerProductos();



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-ingresar-tipo-planta.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <!-- formulario -->
    <form action="../Controlador/ControladorTipoPlanta.php" method="post">
        <h1 class="titulo1" >Crear Tipo Planta</h1>

        <!-- ID Tipo Producto: <input required pattern="[0-9]+" type="text" name="ID_Tipo_Planta" id="ID_Tipo_Planta"> -->
        <br>
        <p>Tipo de Planta</p>
        <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Nombre_Tipo_Planta" id="Nombre_Tipo_Planta">
        <br>   
        <div class="form-column-boton1">
            <input type="hidden" name="Registrar" id="Registrar">
            <button class="btn1" type="submit">Registrar</button>
        </div> 
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarTipoPlanta.php">Cancelar</a>
            </button>
        </div>
    </form>
</body>
</html>