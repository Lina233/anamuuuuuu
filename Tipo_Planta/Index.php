<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("../../Location:Index.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1 align="center">Administrar Tipo Productos</h1>
    <table align="center">
        <thead>
            <tr>
                <td><a href="Vista/IngresarTipoProducto.php">Ingresar</a></td>
                <td><a href="Vista/ListarTipoProducto.php">Listar</a></td>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</body>
</html>