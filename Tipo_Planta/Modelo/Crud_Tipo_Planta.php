﻿<?php
//require_once('../../Conexion.php');    
    class CrudTipo_Planta{
    
        public function __construct(){
        }

        public function InsertarTipoPlanta($Tipo_Planta){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO  Tipo_Planta(ID_Tipo_Planta,Nombre_Tipo_Planta,Estado)
        VALUES(NULL,:Nombre_Tipo_Planta,1)');
 

            // $Sql->execute();
            $Sql->bindValue('Nombre_Tipo_Planta',$Tipo_Planta->getNombre_Tipo_Planta());

            
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }

        public function TraerTipoPlanta(){
            $Db = Db::Conectar();
            $TraeTipoPlanta = [];
            $Sql = $Db->query('SELECT ID_Tipo_Planta,Nombre_Tipo_Planta FROM Tipo_Planta WHERE Estado=1');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Tipo_Planta){
                $MyTipo_Planta = new Tipo_Planta();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyTipo_Planta->setID_Tipo_Planta($Tipo_Planta['ID_Tipo_Planta']);

                $MyTipo_Planta->setNombre_Tipo_Planta($Tipo_Planta['Nombre_Tipo_Planta']);          
                $TraeTipoPlanta[] = $MyTipo_Planta;
            }
            return $TraeTipoPlanta;
        }


        public function ObtenerTipo_Planta($ID_Tipo_Planta)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM tipo_planta WHERE ID_Tipo_Planta=:ID_Tipo_Planta'); //¿Porque en el WHERE solo accede al dato ID_Planta
            $Sql->bindValue('ID_Tipo_Planta',$ID_Tipo_Planta);
            $MyTipo_Planta = new Tipo_Planta();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Tipo_Planta = $Sql->fetch(); //Se almacena en la variable $Planta los datos de la variable $Sql
                $MyTipo_Planta->setID_Tipo_Planta($Tipo_Planta['ID_Tipo_Planta']);
                $MyTipo_Planta->setNombre_Tipo_Planta($Tipo_Planta['Nombre_Tipo_Planta']);
                

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyTipo_Planta;
        }

        public function ObtenerHojaTecnicaNombreTipoPlanta($ID_Hoja_Tecnica)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT tipo_planta.Nombre_Tipo_Planta,hojas_tecnicas.Tamano 
            FROM hojas_tecnicas
            INNER JOIN  tipo_planta
            ON hojas_tecnicas.ID_Tipo_Planta = tipo_planta.ID_Tipo_Planta
            WHERE ID_Hojas_Tecnicas=:ID_Hoja_Tecnica'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Hoja_Tecnica',$ID_Hoja_Tecnica);
            $MyTipoPlanta = new Tipo_Planta();
            $MyHojaTecnica = new HojaTecnica();//Crear un objeto de tipo competencia
            
            try{
                

                $Sql->execute(); 
                $Tipo_Planta = $Sql->fetch(); 
                $MyTipoPlanta->setNombre_Tipo_Planta($Tipo_Planta['Nombre_Tipo_Planta']);
                $MyHojaTecnica->setTamano($Tipo_Planta['Tamano']);
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyTipoPlanta;
        }

        //Listar todos los registros de la tabla
        public function ListarTipo_Planta(){
            $Db = Db::Conectar();
            $ListaTipo_Planta = [];
            $Sql = $Db->query('SELECT * FROM tipo_planta');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Tipo_Planta){
                $MyTipo_Planta = new Tipo_Planta();
                
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyTipo_Planta->setID_Tipo_Planta($Tipo_Planta['ID_Tipo_Planta']);

                $MyTipo_Planta->setNombre_Tipo_Planta($Tipo_Planta['Nombre_Tipo_Planta']);

                $MyTipo_Planta->setEstado($Tipo_Planta['Estado']);

                $ListaTipo_Planta[] = $MyTipo_Planta;
            }
            return $ListaTipo_Planta;
        }


       
        public function ModificarTipoPlanta($Tipo_Planta){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE tipo_planta SET Nombre_Tipo_Planta=:Nombre_Tipo_Planta
            WHERE ID_Tipo_Planta=:ID_Tipo_Planta'); 
            $Sql->bindValue('ID_Tipo_Planta',$Tipo_Planta->getID_Tipo_Planta());
            $Sql->bindValue('Nombre_Tipo_Planta',$Tipo_Planta->getNombre_Tipo_Planta());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarTipoPlantaEstado($ID_Tipo_Planta){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE tipo_planta SET Estado=:Estado
            WHERE ID_Tipo_Planta=:ID_Tipo_Planta'); 
            $Sql->bindValue('ID_Tipo_Planta',$ID_Tipo_Planta);
            
            // echo $ID_Producto;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_Tipo_Planta=explode(":",$ID_Tipo_Planta);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_Tipo_Planta[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('Estado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }    

    //     public function EliminarTipoPlanta($ID_Tipo_Planta){
    //         $Db = Db::Conectar(); //Conectar a la base de datos
    //         //Definir la modificación a realizar.
    //         $Sql = $Db->prepare('DELETE FROM tipo_planta WHERE ID_Tipo_Planta=:ID_Tipo_Planta'); 
    //         $Sql->bindValue('ID_Tipo_Planta',$ID_Tipo_Planta);
    //         // $ID_Planta = 23;
    //         try{
    //             $Sql->execute(); //Ejecutar el Sql que contiene el Delete
    //             echo "Eliminación Exitosa";
    //         }
    //         catch(Exception $e){ //Capturar Errores
    //             echo $e->getMessage(); //Mostar errores en la modificación
    //             die();
    //         }
    //     }

     }


// $Crud = new CrudPlanta();
// $Crud->ListarPlantas();


?>

