<?php
    class Tipo_Planta{
        //Parámentros de entrada
        private $ID_Tipo_Planta;
        private $Nombre_Tipo_Planta;
        private $Estado;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setID_Tipo_Planta($ID_Tipo_Planta){
            $this->ID_Tipo_Planta = $ID_Tipo_Planta;
        }

        public function getID_Tipo_Planta(){
            return $this->ID_Tipo_Planta;
        }

        //
        public function setNombre_Tipo_Planta($Nombre_Tipo_Planta){
            $this->Nombre_Tipo_Planta = $Nombre_Tipo_Planta;
        }

        public function getNombre_Tipo_Planta(){
            return $this->Nombre_Tipo_Planta;
        }

        //
        public function setEstado($Estado){
            $this->Estado = $Estado;
        }

        public function getEstado(){
            return $this->Estado;
        }


        
    
    }
?>