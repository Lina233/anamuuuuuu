<?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once('../../Conexion.php');
require_once('../../Facturacion/Modelo/CrudFactura.php'); 
require_once('../../Facturacion/Modelo/Factura.php');
require_once('../../Facturacion/Modelo/CrudDetalleFactura.php'); 
require_once('../../Facturacion/Modelo/DetalleFactura.php');

$CrudFactura = new CrudFactura(); //Crear de un objeto CrudCompetencia
$Cotizacion = $CrudFactura::ObtenerCotizacion($_GET["CodigoCotizacion"]);

$CrudDetalleCotizacion = new CrudDetalleFactura(); //Crear un Objeto CrudCompetencia
$DetalleCotizacion = $CrudDetalleCotizacion::ListarFacturaspdf($_GET["CodigoCotizacion"]);

// extend TCPF with custom functions
class MYPDF extends TCPDF {

	// Load table data from file
	public function LoadData($file) {
		// Read file lines
		$lines = file($file);
		$data = array();
		foreach($lines as $line) {
			$data[] = explode(';', chop($line));
		}
		return $data;
	}

	// Colored table
	public function ColoredTable($header,$data) {
		// Colors, line width and bold font
		$this->SetFillColor(0, 160, 0);
		$this->SetTextColor(50);
		
		//color de las lineas de la tabla y el ancho
		$this->SetDrawColor(0, 100, 0);
		$this->SetLineWidth(0.1);
		$this->SetFont('', 'C');
		// Header
		$w = array(50, 50, 40, 40); //Ancho de las columnas 
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
		}
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(200, 220, 200);
		$this->SetTextColor(100);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) { //Lista los datos
			$this->Cell($w[0], 4, $row->getNombre_Producto(), 'LR', 0, 'L', $fill);
			$this->Cell($w[1], 4, $row->getValorUnitario(), 'LR', 0, 'R', $fill);
			$this->Cell($w[2], 4, $row->getProductoCantidad(), 'LR', 0, 'R', $fill);
			$this->Cell($w[3], 4, $row->getValorTotal(), 'LR', 0, 'R', $fill);
			
			
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 011');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 12);

// add a page
$pdf->AddPage();

$html = '<h1> </h1>';
$pdf->writeHTML($html, true, false, true, false, '');

$html2= "Medellin,";
$pdf->writeHTML($html2, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html149=$Cotizacion->getFechaFactura();
$pdf->writeHTML($html149, true, false, true, false, '');

$html6='<br>';
$pdf->writeHTML($html6, true, false, true, false, '');
$html7='<br>';
$pdf->writeHTML($html7, true, false, true, false, '');

$html8= "Señor(a):";
$pdf->writeHTML($html8, true, false, true, false, '');


$html3=$Cotizacion->getPrimer_Nombre();
$pdf->writeHTML($html3, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html4= $Cotizacion->getSegundo_Nombre();
$pdf->writeHTML($html4, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html41= $Cotizacion->getPrimer_Apellido();
$pdf->writeHTML($html41, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html42= $Cotizacion->getSegundo_Apellido();
$pdf->writeHTML($html42, true, false, true, false, '');

//Documento de identidad
// $html23= "Documento de Identidad:";
// $pdf->writeHTML($html23, false, false, true, false, '');
// $html19= "&nbsp;";
// $pdf->writeHTML($html19, false, false, true, false, '');
// $html150= $Cotizacion->getDocumento_Iden();
// $pdf->writeHTML($html150, true, false, true, false, '');

$html22='<br><br>';
$pdf->writeHTML($html22, true, false, true, false, '');

$html24= "Asunto: Cotización";
$pdf->writeHTML($html24, true, false, true, false, '');

$html22='<br>';
$pdf->writeHTML($html22, true, false, true, false, '');

$html24= "Cordial saludo señor(a) ";
$pdf->writeHTML($html24, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html41= $Cotizacion->getPrimer_Apellido();
$pdf->writeHTML($html41, true, false, true, false, '');

$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');

$html25= "De acuerdo a su solicitud,queremos compartir con usted la
cotizacion de los siguientes items.";
$pdf->writeHTML($html25, true, false, true, false, '');
$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');

// column titles
$header = array('Nombre Producto', 'Valor Unitario', 'Cantidad Producto', 'Valor Total');

$pdf->ColoredTable($header, $DetalleCotizacion);//Envío objetos que contienes los datos de las competencias.
$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');

$pdf->setRTL(true);






$html161= $Cotizacion->getCosto_Total_Produc();
$pdf->writeHTML($html161, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Valor total productos:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html163= $Cotizacion->getCosto_Transporte();
$pdf->writeHTML($html163, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Valor Costo Transporte:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html162= $Cotizacion->getCosto_Mano_Obra();
$pdf->writeHTML($html162, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Valor mano de obra:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');



$html169= $Cotizacion->getIVA();
$pdf->writeHTML($html169, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "IVA:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html169= $Cotizacion->getSubtotal();
$pdf->writeHTML($html169, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Subtotal:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html170= $Cotizacion->getTotal();
$pdf->writeHTML($html170, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Total con IVA:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');

$html170= $Cotizacion->getEstadoCoti();
$pdf->writeHTML($html170, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Estado:";
$pdf->writeHTML($html131, true, false, true, false, '');



require_once('../../Servicio/Modelo/Servicio.php');
require_once('../../Servicio/Modelo/CrudServicio.php');
$CrudServicio= new CrudServicio();

$ServicioNombreTraer=$Cotizacion->getID_Servicio();
                             
$ServicioNombre = $CrudServicio::ServicioNombreTraer($ServicioNombreTraer);
              
$html170= $ServicioNombre->getNombre_Servicio();
$pdf->writeHTML($html170, false, false, true, false, '');
$html19= "&nbsp;";
$pdf->writeHTML($html19, false, false, true, false, '');
$html131= "Servicio:";
$pdf->writeHTML($html131, true, false, true, false, '');

$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');
$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');
$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');
$html5='<br>';
$pdf->writeHTML($html5, true, false, true, false, '');


$html131= "Firma:_____________________________";
$pdf->writeHTML($html131, true, false, true, false, '');
$pdf->setRTL(false);




// $pdf->setRTL(false);

// ---------------------------------------------------------
ob_end_clean(); //Limpiar errores antes de la impresión
// close and output PDF document
$pdf->Output('$ListaCotizacion.pdf', 'I');

//============================================================+
// END OF FILE