<?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once('../../Conexion.php');
require_once('../../Usuario/Modelo/CrudUsuario.php'); 
require_once('../../Usuario/Modelo/Usuario.php');

$CrudUsuario = new CrudUsuario(); //Crear de un objeto CrudCompetencia
$ListaUsuario = $CrudUsuario->ListarUsuarios(); //Llamado al método ListarCompetencia

// extend TCPF with custom functions
class MYPDF extends TCPDF {

	// Load table data from file
	public function LoadData($file) {
		// Read file lines
		$lines = file($file);
		$data = array();
		foreach($lines as $line) {
			$data[] = explode(';', chop($line));
		}
		return $data;
	}

	// Colored table
	public function ColoredTable($header,$data) {
		// Colors, line width and bold font
		$this->SetFillColor(0, 160, 0);
		$this->SetTextColor(50);
		
		//color de las lineas de la tabla y el ancho
		$this->SetDrawColor(0, 100, 0);
		$this->SetLineWidth(0.1);
		$this->SetFont('', 'C');
		// Header
		$w = array(10, 35,  30, 48, 60); //Ancho de las columnas 
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
		}
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(200, 220, 200);
		$this->SetTextColor(100);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) { //Lista los datos
			$this->Cell($w[0], 7, $row->getIdUsuario(), 'LR', 0, 'L', $fill);
			$this->Cell($w[1], 7, $row->getNombreUsuario(), 'LR', 0, 'R', $fill);
			
			
			$this->Cell($w[2], 7, $row->getTelefono(), 'LR', 0, 'R', $fill);
			$this->Cell($w[3], 7, $row->getDireccion(), 'LR', 0, 'R', $fill);
			$this->Cell($w[4], 7, $row->getCorreo_Electronico(), 'LR', 0, 'R', $fill);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 011');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 12);

// add a page
$pdf->AddPage();

// column titles
$header = array('ID', 'Nombre Usuario', 'Telefono', 'Dirección','Correo Electrónico');

// data loading
//$data = $pdf->LoadData('data/table_data_demo.txt');

// print colored table
$pdf->ColoredTable($header, $ListaUsuario);//Envío objetos que contienes los datos de las competencias.

// ---------------------------------------------------------
ob_end_clean(); //Limpiar errores antes de la impresión
// close and output PDF document
$pdf->Output('ListaProductos.pdf', 'I');

//============================================================+
// END OF FILE