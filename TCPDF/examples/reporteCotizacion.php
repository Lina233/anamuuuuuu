<?php
//============================================================+
// File name   : example_011.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 011 for TCPDF class
//               Colored Table (very simple table)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Colored Table
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once('../../Conexion.php');
require_once('../../Facturacion/Modelo/CrudFactura.php'); 
require_once('../../Facturacion/Modelo/Factura.php');

$CrudFactura = new CrudFactura(); //Crear de un objeto CrudCompetencia
$ListaCotizacion = $CrudFactura->ListarFactura(); //Llamado al método ListarCompetencia

// extend TCPF with custom functions
class MYPDF extends TCPDF {

	// Load table data from file
	public function LoadData($file) {
		// Read file lines
		$lines = file($file);
		$data = array();
		foreach($lines as $line) {
			$data[] = explode(';', chop($line));
		}
		return $data;
	}

	// Colored table
	public function ColoredTable($header,$data) {
		// Colors, line width and bold font
		$this->SetFillColor(0, 160, 0);
		$this->SetTextColor(50);
		
		//color de las lineas de la tabla y el ancho
		$this->SetDrawColor(0, 100, 0);
		$this->SetLineWidth(0.1);
		$this->SetFont('', 'C');
		// Header
		$w = array(10, 50, 25, 26, 35, 35); //Ancho de las columnas 
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
		}
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(200, 220, 200);
		$this->SetTextColor(100);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) { //Lista los datos
			$this->Cell($w[0], 6, $row->getCodigoCotizacion(), 'LR', 0, 'L', $fill);
			$this->Cell($w[1], 6, $row->getFechaFactura(), 'LR', 0, 'R', $fill);
			$this->Cell($w[2], 6, $row->getCodigoCliente(), 'LR', 0, 'R', $fill);
			$this->Cell($w[3], 6, $row->getID_Servicio(), 'LR', 0, 'R', $fill);
			$this->Cell($w[4], 6, $row->getSubtotal(), 'LR', 0, 'R', $fill);
			$this->Cell($w[5], 6, $row->getTotal(), 'LR', 0, 'R', $fill);
			
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 011');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 12);

// add a page
$pdf->AddPage();

// column titles
$header = array('ID', 'Fecha', 'ID Cliente', 'ID Servicio', 'Subtotal', 'Total');

// data loading
//$data = $pdf->LoadData('data/table_data_demo.txt');

// print colored table
$pdf->ColoredTable($header, $ListaCotizacion);//Envío objetos que contienes los datos de las competencias.

// ---------------------------------------------------------
ob_end_clean(); //Limpiar errores antes de la impresión
// close and output PDF document
$pdf->Output('$ListaCotizacion.pdf', 'I');

//============================================================+
// END OF FILE