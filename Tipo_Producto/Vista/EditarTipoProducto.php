<?php

require_once('../../Conexion.php');
require_once('../Modelo/Tipo_Producto.php');
require_once('../Modelo/Crud_Tipo_Producto.php');

$CrudTipoProducto = new CrudTipoProducto(); //Crear un Objeto CrudCompetencia
$TipoProducto = $CrudTipoProducto::ObtenerTipo_Producto($_GET["ID_Tipo_Producto"]);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Modificar</title>
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-editar-tipo-producto.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <!-- para poner fotos en los formukarios --> 
    <!-- enctype="multipart/form-data" -->

    
    <form action="../Controlador/ControladorTipoProducto.php" method="post">
        <h1 class="titulo1">Editar Tipo Producto</h1> 
        <br>
        <p>ID Tipo Producto</p>   
        <input  readonly="disabled" type="text" name="ID_Tipo_Producto" id="ID_Tipo_Producto" value="<?php echo $TipoProducto->getID_Tipo_Producto(); ?>">
        <br>
        <br>
        <p>Nombre Tipo Producto</p>
        <input  pattern="[A-Za-z]+" required type="text" name="Nombre_Tipo_Producto" id="Nombre_Tipo_Producto" value="<?php echo $TipoProducto->getNombre_Tipo_Producto(); ?>">
        <br>
        
        <!-- <button  type="button" name="Agreg" id="Agreg" >Agregar</button> -->
        <!-- <input type="hidden" name="Agreg" id="Agreg">
        <button type="button">Modificar</button> -->
        <br>
        <div class="form-column-boton1">
            <input type="hidden" name="Modificar" id="Modificar">
            <button class="btn1" type="submit">Modificar</button>
        </div>
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarTipoProducto.php">Cancelar</a>
            </button>
        </div>
    </form>
</body>

<script>
// $(document).ready(function(){


//     $("#Agreg").click(function(){
//         let validado=0;
//         //Validación del tipo documento
        
        


        

//         //Validación del PrecioProducto
//         if($("#ValorUnitario").val().length == 0 ){
//          $("#validacion_ValorUnitario").text("Precio obligatorio");
//         }
//         else
//         {
//          $("#validacion_ValorUnitario").text("");
//          validado++;
//         }

//         //Validación del CantidadProducto
//         if($("#ProductoCantidad").val().length == 0 ){
//          $("#validacion_ProductoCantidad").text("Cantidad Obligatoria");
//         }
//         else
//         {
//          $("#validacion_ProductoCantidad").text("");
//          validado++;
//         }
        

        
//         // validacion todos los campos
//         if(validado==2) //Si validado == al número de campos que requieren validación entocnces se registra
//          {

//             // AgregarDetalle();
              
           
//             alert("Registro exitosddddo");
//             //Limpiar cajas de texto, select, textarea, checkbox, radiobutton
            

           
            

            
//          }
//     });
// })

</script>
</html>