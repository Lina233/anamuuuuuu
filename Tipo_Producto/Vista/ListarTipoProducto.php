﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/Crud_Tipo_Producto.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Tipo_Producto.php');

$CrudTipoProducto = new CrudTipoProducto(); //Crear de un objeto CrudCompetencia
$ListaTipoProduct = $CrudTipoProducto->ListarTipoProducto(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-tipo-producto.css">
    
</head>
<body>
    
    <?php 
    include('../../vistas/navbar/dashboard-tablas.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Tipo Productos</h1>
    <div class="form-column-boton1">    
        <button class="btn1" type="button">
            <a href="IngresarTipoProducto.php">Crear Tipo Producto</a>
        </button>
    </div> 
    
    <div id="main-container">
        <table>
            <thead>
            <tr>
                <th class="th1">ID Tipo Producto</th>
                <th>Nombre Tipo Producto</th>
                <th>Acciones</th>                   
            </tr>
            </thead>

            <tbody>
            <?php
                foreach($ListaTipoProduct as $Producto){
                    ?>
                    <tr>
                        <td><?php echo $Producto->getID_Tipo_Producto(); ?></td>
                        <td><?php echo $Producto->getNombre_Tipo_Producto(); ?></td>
                        
                        <td>
                        <a class="link2" href="EditarTipoProducto.php?ID_Tipo_Producto=<?php echo $Producto->getID_Tipo_Producto(); ?>">Editar<img src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a> 
                        <!-- <a  href="../Controlador/ControladorTipoProducto.php?ID_Tipo_Producto=<?php echo $Producto->getID_Tipo_Producto(); ?>&Accion=EliminarProducto" >Eliminar
                        </a> --></td>
                    </tr>
                    <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</body>
</html>