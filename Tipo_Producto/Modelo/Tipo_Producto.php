<?php
    class Tipo_Producto{
        //Parámentros de entrada
        private $ID_Tipo_Producto;
        private $Nombre_Tipo_Producto;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setID_Tipo_Producto($ID_Tipo_Producto){
            $this->ID_Tipo_Producto = $ID_Tipo_Producto;
        }

        public function getID_Tipo_Producto(){
            return $this->ID_Tipo_Producto;
        }

        //
        public function setNombre_Tipo_Producto($Nombre_Tipo_Producto){
            $this->Nombre_Tipo_Producto = $Nombre_Tipo_Producto;
        }

        public function getNombre_Tipo_Producto(){
            return $this->Nombre_Tipo_Producto;
        }


        
    
    }
?>