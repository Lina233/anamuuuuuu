﻿<?php
//require_once('../../Conexion.php');    
    class CrudTipoProducto{
    
        public function __construct(){
        }

        public function InsertarTipoProducto($Tipo_Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO  Tipo_Producto(ID_Tipo_Producto,Nombre_Tipo_Producto)
        VALUES(NULL,:Nombre_Tipo_Producto)');
 

            // $Sql->execute();
            $Sql->bindValue('Nombre_Tipo_Producto',$Tipo_Producto->getNombre_Tipo_Producto());

            
            try{
                $Sql->execute(); //Ejecutar el Insert
                echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }

        public function TraerTipoProducto(){
            $Db = Db::Conectar();
            $TraeTipoProductos = [];
            $Sql = $Db->query('SELECT Id_Tipo_Producto,Nombre_Tipo_Producto FROM Tipo_Producto');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Tipo_Producto){
                $MyProducto = new Tipo_Producto();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyProducto->setID_Tipo_Producto($Tipo_Producto['Id_Tipo_Producto']);

                $MyProducto->setNombre_Tipo_Producto($Tipo_Producto['Nombre_Tipo_Producto']);          
                $TraeTipoProductos[] = $MyProducto;
            }
            return $TraeTipoProductos;
        }

        public function ObtenerTipo_Producto($ID_Tipo_Producto)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM tipo_producto WHERE ID_Tipo_Producto=:ID_Tipo_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Tipo_Producto',$ID_Tipo_Producto);
            $MyProducto = new Tipo_Producto();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Tipo_Producto = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyProducto->setID_Tipo_Producto($Tipo_Producto['ID_Tipo_Producto']);
                $MyProducto->setNombre_Tipo_Producto($Tipo_Producto['Nombre_Tipo_Producto']);
                

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyProducto;
        }

        //Listar todos los registros de la tabla
        public function ListarTipoProducto(){
            $Db = Db::Conectar();
            $ListaTipoProductos = [];
            $Sql = $Db->query('SELECT * FROM tipo_producto');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Tipo_Producto){
                $MyProducto = new Tipo_Producto();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyProducto->setID_Tipo_Producto($Tipo_Producto['ID_Tipo_Producto']);

                $MyProducto->setNombre_Tipo_Producto($Tipo_Producto['Nombre_Tipo_Producto']);

                $ListaTipoProductos[] = $MyProducto;
            }
            return $ListaTipoProductos;
        }

        

        public function ModificarTipoProducto($Tipo_Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE tipo_producto SET Nombre_Tipo_Producto=:Nombre_Tipo_Producto
            WHERE ID_Tipo_Producto=:ID_Tipo_Producto'); 
            $Sql->bindValue('ID_Tipo_Producto',$Tipo_Producto->getID_Tipo_Producto());
            $Sql->bindValue('Nombre_Tipo_Producto',$Tipo_Producto->getNombre_Tipo_Producto());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function EliminarTipoProducto($ID_Tipo_Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM tipo_producto WHERE ID_Tipo_Producto=:ID_Tipo_Producto'); 
            $Sql->bindValue('ID_Tipo_Producto',$ID_Tipo_Producto);
            // $ID_Producto = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>