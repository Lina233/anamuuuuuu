<?php
class DetalleFactura{
    private $CodigoDetalleCotizacion;
    private $CodigoCotizacion;
    private $CodigoProducto;
    private $ProductoCantidad;
    private $ValorUnitario;
    private $ValorTotal;
    private $Nombre_Producto;
    
    


    function __construct(){}

    public function setCodigoDetalleCotizacion($CodigoDetalleCotizacion)
    {
        $this->CodigoDetalleCotizacion = $CodigoDetalleCotizacion;
    }

    public function getCodigoDetalleCotizacion(){
        return $this->CodigoDetalleCotizacion;
    }


    public function setCodigoCotizacion($CodigoCotizacion)
    {
        $this->CodigoCotizacion = $CodigoCotizacion;
    }

    public function getCodigoCotizacion(){
        return $this->CodigoCotizacion;
    }


    public function setCodigoProducto($CodigoProducto)
    {
        $this->CodigoProducto = $CodigoProducto;
    }

    public function getCodigoProducto(){
        return $this->CodigoProducto;
    }
    //

    public function setProductoCantidad($ProductoCantidad)
    {
        $this->ProductoCantidad = $ProductoCantidad;
    }

    public function getProductoCantidad(){
        return $this->ProductoCantidad;
    }
    //

    public function setValorUnitario($ValorUnitario)
    {
        $this->ValorUnitario = $ValorUnitario;
    }

    public function getValorUnitario(){
        return $this->ValorUnitario;
    }


    public function setValorTotal($ValorTotal)
    {
        $this->ValorTotal = $ValorTotal;
    }

    public function getValorTotal(){
        return $this->ValorTotal;
    }

    

    public function setNombre_Producto($Nombre_Producto)
    {
        $this->Nombre_Producto = $Nombre_Producto;
    }

    public function getNombre_Producto(){
        return $this->Nombre_Producto;
    }

    

}




?>