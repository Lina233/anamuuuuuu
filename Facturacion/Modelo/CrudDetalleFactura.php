<?php
class CrudDetalleFactura
{
    public function __construct(){}

    public function InsertarDetalleFactura($DetalleFactura){
        $detalleinsertado = 0;
        $Db = Db::Conectar();
        $Sql =$Db->prepare('INSERT INTO  detallefacturas(CodigoDetalleCotizacion, CodigoCotizacion,CodigoProducto,ProductoCantidad,ValorUnitario,ValorTotal,Nombre_Producto)
        VALUES(NULL, :CodigoCotizacion,:CodigoProducto,:ProductoCantidad,:ValorUnitario,:ValorTotal,:Nombre_Producto)');

        $Sql->bindValue('CodigoCotizacion',$DetalleFactura->getCodigoCotizacion());
        $Sql->bindValue('Nombre_Producto',$DetalleFactura->getNombre_Producto());
        $Sql->bindValue('CodigoProducto',$DetalleFactura->getCodigoProducto());
        $Sql->bindValue('ProductoCantidad',$DetalleFactura->getProductoCantidad());
        $Sql->bindValue('ValorUnitario',$DetalleFactura->getValorUnitario());
        
        
        
        $Sql->bindValue('ValorTotal',$DetalleFactura->getValorTotal());

        try{
            $Sql->execute();
            $detalleinsertado = 1;
        }
        catch(Exception $e){
            echo $e->getMessage();
            //die();
        }
        return $detalleinsertado;
    }

    public function ListarFacturas()
    {
        $Db = Db::Conectar();
        $ListarFacturas = [];
        $Sql = $Db->query('SELECT facturas.CodigoCliente,facturas.FechaFactura,detallefacturas.* FROM detallefacturas 
                           INNER JOIN facturas 
                           ON detallefacturas.CodigoCotizacion = facturas.CodigoCotizacion');
        $Sql->execute();
        foreach($Sql->fetchAll() as $Factura){
            $MiFactura = new Factura();
            $MiDetalleFactura = new DetalleFactura();
            $MiFactura->setCodigoCliente($Factura['CodigoCliente']);
            $MiFactura->setFechaFactura($Factura['FechaFactura']);
            $MiDetalleFactura->setCodigoCotizacion($Factura['CodigoCotizacion']);
            $MiDetalleFactura->setCodigoDetalleCotizacion($Factura['CodigoDetalleCotizacion']);
            $MiDetalleFactura->setCodigoProducto($Factura['CodigoProducto']);
            $MiDetalleFactura->setProductoCantidad($Factura['ProductoCantidad']);
            
            $MiDetalleFactura->setValorUnitario($Factura['ValorUnitario']);
            $MiDetalleFactura->setValorTotal($Factura['ValorTotal']);
            $ListaFacturas[] = $MiDetalleFactura;
        }
        return $ListaFacturas;
    }

    public function ListarFactura($ID)
    {
        $Db = Db::Conectar();
            $ListaFactura = [];

            $Sql = $Db->prepare('SELECT * FROM detallefacturas WHERE
                CodigoCotizacion=:CodigoCotizacion ');

            $Sql->bindValue('CodigoCotizacion',$ID);

            // echo "string $ID";

            $Sql->execute();
            foreach($Sql->fetchAll() as $Factura){

                $ListaFactura[] = $Factura;
            }
            // echo $ListaFactura;

            // return $ListaFactura;
            echo json_encode($ListaFactura);        
            
    }

    public function ListarFacturaEditar($CodigoCotizacion)
    {
        $Db = Db::Conectar();
            $ListaDetalleFactura = [];

            $Sql = $Db->prepare('SELECT * FROM detallefacturas WHERE
                CodigoCotizacion=:CodigoCotizacion ');

            $Sql->bindValue('CodigoCotizacion',$CodigoCotizacion);

            // echo "string $ID";

            $Sql->execute();
            foreach($Sql->fetchAll() as $Factura){

                $ListaDetalleFactura[] = $Factura;
            }
            
            return json_encode($ListaDetalleFactura);       
            
    }

    public function ListarFacturaspdf($CodigoCotizacion){
            $Db = Db::Conectar();
            $ListaFacturaspdf = [];
            
            $Sql = $Db->prepare('SELECT * FROM detallefacturas WHERE
                CodigoCotizacion=:CodigoCotizacion ');

            $Sql->bindValue('CodigoCotizacion',$CodigoCotizacion);

            $Sql->execute();
            foreach($Sql->fetchAll() as $DetalleFactura){
                
                $MiDetalleFactura = new DetalleFactura();
                // echo $Factura['ID_Producto']."----".$Factura['Nombre_Producto'];
                
                $MiDetalleFactura->setNombre_Producto($DetalleFactura['Nombre_Producto']);
                $MiDetalleFactura->setValorUnitario($DetalleFactura['ValorUnitario']);
                $MiDetalleFactura->setProductoCantidad($DetalleFactura['ProductoCantidad']);
                $MiDetalleFactura->setValorTotal($DetalleFactura['ValorTotal']);
                
                
                $MiDetalleFactura->setProductoCantidad($DetalleFactura['ProductoCantidad']);
                $MiDetalleFactura->setValorUnitario($DetalleFactura['ValorUnitario']);
                $MiDetalleFactura->setValorTotal($DetalleFactura['ValorTotal']);
                
                

                $ListaFacturaspdf[] = $MiDetalleFactura;
            }
            return $ListaFacturaspdf;
        }

        public function ObtenerDetalle($CodigoDetalleCotizacion)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM detallefacturas WHERE CodigoDetalleCotizacion=:CodigoDetalleCotizacion');
            $Sql->bindValue('CodigoDetalleCotizacion',$CodigoDetalleCotizacion);
            $MyDetalleFactura = new DetalleFactura();//Crear un objeto de tipo competencia
            try
            {
                $Sql->execute(); //Ejecutar el Update
                $DetalleFactura = $Sql->fetch(); //Se almacena en la variable $Competencia los datos de la variable $Sql
                $MyDetalleFactura->setCodigoDetalleCotizacion($DetalleFactura['CodigoDetalleCotizacion']);
                $MyDetalleFactura->setProductoCantidad($DetalleFactura['ProductoCantidad']);
                $MyDetalleFactura->setValorUnitario($DetalleFactura['ValorUnitario']);
                $MyDetalleFactura->setCodigoProducto($DetalleFactura['CodigoProducto']);
                $MyDetalleFactura->setCodigoEstado($DetalleFactura['CodigoEstado']);

            }
            catch(Exception $e)
            { //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyDetalleFactura;
        }
    
        public function ModificarDetalle($DetalleFactura){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE detallefacturas SET ValorUnitario=:ValorUnitario,ProductoCantidad=:ProductoCantidad,ValorTotal=:ValorTotal,CodigoEstado=:CodigoEstado,CodigoProducto=:CodigoProducto
            WHERE CodigoDetalleCotizacion=:CodigoDetalleCotizacion'); 
            
            $Sql->bindValue('CodigoDetalleCotizacion',$DetalleFactura->getCodigoDetalleCotizacion());
            $Sql->bindValue('ValorTotal',$DetalleFactura->getValorTotal());
            $Sql->bindValue('ValorUnitario',$DetalleFactura->getValorUnitario());
            $Sql->bindValue('ProductoCantidad',$DetalleFactura->getProductoCantidad());
            $Sql->bindValue('CodigoEstado',$DetalleFactura->getCodigoEstado());
            $Sql->bindValue('CodigoProducto',$DetalleFactura->getCodigoProducto());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function EliminarCotizacionDetalle($DetalleFactura)
        {
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM detallefacturas WHERE CodigoCotizacion=:CodigoCotizacion'); 
            
            $Sql->bindValue('CodigoCotizacion',$DetalleFactura->getCodigoCotizacion());

            try
            {
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                // echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }
    





}




?>