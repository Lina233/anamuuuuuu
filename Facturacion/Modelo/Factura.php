<?php
class Factura{
    private $CodigoCotizacion;
    private $CodigoCliente;
    private $FechaFactura;
    private $ID_Servicio;
    private $Subtotal;
    private $Total;
    private $Costo_Transporte;
    private $Costo_Mano_Obra;
    private $Costo_Total_Produc;
    private $Primer_Nombre;
    private $Segundo_Nombre;
    private $Primer_Apellido;
    private $Segundo_Apellido;
    private $Documento_Iden;
    private $IVA;
    private $EstadoCoti;

    function __construct(){}

    public function setCodigoCotizacion($CodigoCotizacion)
    {
        $this->CodigoCotizacion = $CodigoCotizacion;
    }

    public function getCodigoCotizacion(){
        return $this->CodigoCotizacion;
    }

    

    public function setCodigoCliente($CodigoCliente)
    {
        $this->CodigoCliente = $CodigoCliente;
    }

    public function getCodigoCliente(){
        return $this->CodigoCliente;
    }

    //
    public function setFechaFactura($FechaFactura)
    {
        $this->FechaFactura = $FechaFactura;
    }

    public function getFechaFactura(){
        return $this->FechaFactura;
    }

    //
    public function setID_Servicio($ID_Servicio)
    {
        $this->ID_Servicio = $ID_Servicio;
    }

    public function getID_Servicio(){
        return $this->ID_Servicio;
    }

    public function setSubtotal($Subtotal)
    {
        $this->Subtotal = $Subtotal;
    }

    public function getSubtotal(){
        return $this->Subtotal;
    }

    //
    public function setTotal($Total)
    {
        $this->Total = $Total;
    }

    public function getTotal(){
        return $this->Total;
    }

    //
    public function setCosto_Transporte($Costo_Transporte)
    {
        $this->Costo_Transporte = $Costo_Transporte;
    }

    public function getCosto_Transporte(){
        return $this->Costo_Transporte;
    }

    //
    public function setCosto_Mano_Obra($Costo_Mano_Obra)
    {
        $this->Costo_Mano_Obra = $Costo_Mano_Obra;
    }

    public function getCosto_Mano_Obra(){
        return $this->Costo_Mano_Obra;
    }

    //
    public function setCosto_Total_Produc($Costo_Total_Produc)
    {
        $this->Costo_Total_Produc = $Costo_Total_Produc;
    }

    public function getCosto_Total_Produc(){
        return $this->Costo_Total_Produc;
    }

    //
    public function setIVA($IVA)
    {
        $this->IVA = $IVA;
    }

    public function getIVA(){
        return $this->IVA;
    }

    //
    public function setPrimer_Nombre($Primer_Nombre)
    {
        $this->Primer_Nombre = $Primer_Nombre;
    }

    public function getPrimer_Nombre(){
        return $this->Primer_Nombre;
    }

    //
    public function setSegundo_Nombre($Segundo_Nombre)
    {
        $this->Segundo_Nombre = $Segundo_Nombre;
    }

    public function getSegundo_Nombre(){
        return $this->Segundo_Nombre;
    }

    //
    public function setPrimer_Apellido($Primer_Apellido)
    {
        $this->Primer_Apellido = $Primer_Apellido;
    }

    public function getPrimer_Apellido(){
        return $this->Primer_Apellido;
    }

    //
    public function setSegundo_Apellido($Segundo_Apellido)
    {
        $this->Segundo_Apellido = $Segundo_Apellido;
    }

    public function getSegundo_Apellido(){
        return $this->Segundo_Apellido;
    }

    //
    public function setDocumento_Iden($Documento_Iden)
    {
        $this->Documento_Iden = $Documento_Iden;
    }

    public function getDocumento_Iden(){
        return $this->Documento_Iden;
    }

    //
    public function setEstadoCoti($EstadoCoti)
    {
        $this->EstadoCoti = $EstadoCoti;
    }

    public function getEstadoCoti(){
        return $this->EstadoCoti;
    }
}
?>