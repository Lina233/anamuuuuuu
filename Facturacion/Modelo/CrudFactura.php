<?php
class CrudFactura{
    public function __construct(){
    
    }

    public function InsertarFactura($Factura){
        $CodigoCotizacionGenerado = -1;
        $Db = Db::Conectar();
        $Sql =$Db->prepare('INSERT INTO  facturas(CodigoCotizacion,CodigoCliente,FechaFactura,ID_Servicio,Subtotal,Total,Costo_Transporte,Costo_Mano_Obra,Costo_Total_Produc,IVA,Primer_Nombre,Segundo_Nombre,Primer_Apellido,Segundo_Apellido,Documento_Iden,EstadoCoti)
        VALUES(NULL,:CodigoCliente,NOW(),:ID_Servicio,:Subtotal,:Total,:Costo_Transporte,:Costo_Mano_Obra,:Costo_Total_Produc,:IVA,:Primer_Nombre,:Segundo_Nombre,:Primer_Apellido,:Segundo_Apellido,:Documento_Iden,:EstadoCoti)');

        $Sql->bindValue('CodigoCliente',$Factura->getCodigoCliente());
        $Sql->bindValue('ID_Servicio',$Factura->getID_Servicio());
        $Sql->bindValue('Subtotal',$Factura->getSubtotal());
        $Sql->bindValue('Costo_Transporte',$Factura->getCosto_Transporte());
        $Sql->bindValue('Total',$Factura->getTotal());
        $Sql->bindValue('Costo_Mano_Obra',$Factura->getCosto_Mano_Obra());
        $Sql->bindValue('Costo_Total_Produc',$Factura->getCosto_Total_Produc());
        $Sql->bindValue('IVA',$Factura->getIVA());
        $Sql->bindValue('Primer_Nombre',$Factura->getPrimer_Nombre());
        $Sql->bindValue('Segundo_Nombre',$Factura->getSegundo_Nombre());
        $Sql->bindValue('Primer_Apellido',$Factura->getPrimer_Apellido());
        $Sql->bindValue('Segundo_Apellido',$Factura->getSegundo_Apellido());
        $Sql->bindValue('Documento_Iden',$Factura->getDocumento_Iden());
        $Sql->bindValue('EstadoCoti',$Factura->getEstadoCoti());

        try{
            $Sql->execute();
            $CodigoCotizacionGenerado = $Db->lastInsertId();//Consultar el último Id insertado para el usurio que está conectado a la DB
        }
        catch(Exception $e){
            echo $e->getMessage();
            //die();
        }
        return $CodigoCotizacionGenerado;
    }

    public function ModificarFactura($Factura){
        $CodigoCotizacionGenerado = -1;
        $Db = Db::Conectar();
        $Sql =$Db->prepare('UPDATE facturas SET CodigoCliente=:CodigoCliente, ID_Servicio=:ID_Servicio,Subtotal=:Subtotal,Total=:Total,Costo_Transporte=:Costo_Transporte,Costo_Mano_Obra=:Costo_Mano_Obra,Costo_Total_Produc=:Costo_Total_Produc,IVA=:IVA,Primer_Nombre=:Primer_Nombre,Segundo_Nombre=:Segundo_Nombre,Primer_Apellido=:Primer_Apellido,Segundo_Apellido=:Segundo_Apellido,Documento_Iden=:Documento_Iden,EstadoCoti=:EstadoCoti WHERE CodigoCotizacion=:CodigoCotizacion');


        $Sql->bindValue('CodigoCotizacion',$Factura->getCodigoCotizacion());
        $Sql->bindValue('CodigoCliente',$Factura->getCodigoCliente());
        $Sql->bindValue('ID_Servicio',$Factura->getID_Servicio());
        $Sql->bindValue('Subtotal',$Factura->getSubtotal());
        $Sql->bindValue('Costo_Transporte',$Factura->getCosto_Transporte());
        $Sql->bindValue('Total',$Factura->getTotal());
        $Sql->bindValue('Costo_Mano_Obra',$Factura->getCosto_Mano_Obra());
        $Sql->bindValue('Costo_Total_Produc',$Factura->getCosto_Total_Produc());
        $Sql->bindValue('IVA',$Factura->getIVA());
        $Sql->bindValue('Primer_Nombre',$Factura->getPrimer_Nombre());
        $Sql->bindValue('Segundo_Nombre',$Factura->getSegundo_Nombre());
        $Sql->bindValue('Primer_Apellido',$Factura->getPrimer_Apellido());
        $Sql->bindValue('Segundo_Apellido',$Factura->getSegundo_Apellido());
        $Sql->bindValue('Documento_Iden',$Factura->getDocumento_Iden());
        $Sql->bindValue('EstadoCoti',$Factura->getEstadoCoti());

        try{
            $Sql->execute();
            // $CodigoCotizacionGenerado = $Db->lastInsertId();//Consultar el último Id insertado para el usurio que está conectado a la DB
            $CodigoCotizacionGenerado = 1;
        }
        catch(Exception $e){
            echo $e->getMessage();
            //die();
        }
        return $CodigoCotizacionGenerado;
    }

    public function ModificarSubtotalIngresar($Factura){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE facturas SET Costo_Total_Produc=:Costo_Total_Produc,Subtotal=:Subtotal,Total=:Total
            WHERE CodigoCotizacion=:CodigoCotizacion'); 


            
            $Sql->bindValue('CodigoCotizacion',$Factura->getCodigoCotizacion());
            
            $Sql->bindValue('Costo_Total_Produc',$Factura->getCosto_Total_Produc());
            $Sql->bindValue('Subtotal',$Factura->getSubtotal());
            $Sql->bindValue('Total',$Factura->getTotal());
            
            

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    public function ListarFactura(){
        $Db = Db::Conectar();
        $ListarFacturas = [];
        $Sql = $Db->query('SELECT * FROM facturas ');
        $Sql->execute();
        foreach($Sql->fetchAll() as $Factura){
            $MyFactura = new Factura();
            
            $MyFactura->setCodigoCotizacion($Factura['CodigoCotizacion']);
            $MyFactura->setCodigoCliente($Factura['CodigoCliente']);
            $MyFactura->setFechaFactura($Factura['FechaFactura']);
            $MyFactura->setID_Servicio($Factura['ID_Servicio']);
            $MyFactura->setSubtotal($Factura['Subtotal']);
            $MyFactura->setTotal($Factura['Total']);
                
            $ListarFacturas[] = $MyFactura;
        }
        return $ListarFacturas;
    }

    

    
    public function ListarCotizacion($ID)
    {
        $Db = Db::Conectar();
            $ListarCotizacion = [];

            $Sql = $Db->prepare('SELECT * FROM facturas WHERE
                CodigoCotizacion=:CodigoCotizacion ');

            $Sql->bindValue('CodigoCotizacion',$ID);

            // echo "string $ID";

            $Sql->execute();
            foreach($Sql->fetchAll() as $Cotizacion){

                $ListarCotizacion[] = $Cotizacion;
            }
            // echo $ListaFactura;

            // return $ListaFactura;
            echo json_encode($ListarCotizacion);        
            
    }

    public function ObtenerCotizacion($CodigoCotizacion)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM facturas WHERE CodigoCotizacion=:CodigoCotizacion'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('CodigoCotizacion',$CodigoCotizacion);
            $MyCotizacion = new Factura();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Cotizacion = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyCotizacion->setCodigoCotizacion($Cotizacion['CodigoCotizacion']);
                $MyCotizacion->setCodigoCliente($Cotizacion['CodigoCliente']); 
                $MyCotizacion->setID_Servicio($Cotizacion['ID_Servicio']); 
                $MyCotizacion->setCosto_Transporte($Cotizacion['Costo_Transporte']);
                $MyCotizacion->setCosto_Mano_Obra($Cotizacion['Costo_Mano_Obra']);
                $MyCotizacion->setPrimer_Nombre($Cotizacion['Primer_Nombre']);
                $MyCotizacion->setSegundo_Nombre($Cotizacion['Segundo_Nombre']);
                $MyCotizacion->setPrimer_Apellido($Cotizacion['Primer_Apellido']);
                $MyCotizacion->setSegundo_Apellido($Cotizacion['Segundo_Apellido']);
                $MyCotizacion->setDocumento_Iden($Cotizacion['Documento_Iden']);
                $MyCotizacion->setEstadoCoti($Cotizacion['EstadoCoti']);
                $MyCotizacion->setFechaFactura($Cotizacion['FechaFactura']);
                $MyCotizacion->setCosto_Total_Produc($Cotizacion['Costo_Total_Produc']);
                $MyCotizacion->setTotal($Cotizacion['Total']);
                $MyCotizacion->setSubtotal($Cotizacion['Subtotal']);
                $MyCotizacion->setIVA($Cotizacion['IVA']);
                

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyCotizacion;
        }
}

?>