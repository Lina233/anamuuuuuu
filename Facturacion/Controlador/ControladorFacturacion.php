<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>


<?php
session_start();
if(!(isset($_SESSION["NombreUsuario"]))){ //Si la sesión no existe redireccionar al login
    header("Location:../../Index.php");
}

require_once("../../Conexion.php");
require_once('../Modelo/Factura.php'); //Vincular la Clase Factura
require_once('../Modelo/CrudFactura.php'); //Vincular la Clase CrudFactura
require_once('../Modelo/DetalleFactura.php');//Vincular la clase DetalleFactura
require_once('../Modelo/CrudDetalleFactura.php');//Vincular la clase CrudDetalleFactura


$Factura = new Factura(); //Crear el objeto Factura
$CrudFactura = new CrudFactura();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    $Factura->setCodigoCliente($_POST["CodigoCliente1"]); //Instanciar el atributo
    $Factura->setID_Servicio($_POST["ID_Servicio"]);
    $Factura->setPrimer_Nombre($_POST["Primer_Nombre"]);
    $Factura->setSegundo_Nombre($_POST["Segundo_Nombre"]);
    $Factura->setPrimer_Apellido($_POST["Primer_Apellido"]);
    $Factura->setSegundo_Apellido($_POST["Segundo_Apellido"]);
    $Factura->setDocumento_Iden($_POST["Documento_Iden"]);
    $Factura->setEstadoCoti($_POST["EstadoCoti"]);

    
    //estos no se an calculado los pongo en 0
    $TotalProductos=0;
    $Subtotal= 0;
    $Total=0;
    $Factura->setSubtotal($Subtotal);
    $Factura->setTotal($Total);
    $Factura->setCosto_Total_Produc($TotalProductos);

    $Factura->setCosto_Transporte($_POST["Costo_Transporte"]);
    $Transporte=$Factura->getCosto_Transporte();
    // echo "string $Transporte";

    $Factura->setCosto_Mano_Obra($_POST["Costo_Mano_Obra"]);
    $ManoObra=$Factura->getCosto_Mano_Obra();
    // echo "string $ManoObra";

    $IVA=($_POST["IVA"]);
    // echo "IVA $IVA";
    $IVAReal= $IVA/100;
    // echo "IVA $IVAReal";
    $Factura->setIVA($IVAReal);


    $CodigoCotizacionGenerado=$CrudFactura::InsertarFactura($Factura); // Llamar el método para Insertar
    
    $DetalleFactura = new DetalleFactura();
    $CrudDetalleFactura = new CrudDetalleFactura();
    
    
    if($CodigoCotizacionGenerado>-1)
    {
        $ProductosAgregar = $_POST["ProductosAgregados"];
        $regitroexitoso = 0;

        
        for($ConsecutivoProducto = 1; $ConsecutivoProducto <= $ProductosAgregar; $ConsecutivoProducto++)
        {
           
            $DetalleFactura->setCodigoCotizacion($CodigoCotizacionGenerado);
            
            $CodigoProducto = "CodigoProducto".$ConsecutivoProducto;



            
            if(isset($_POST[$CodigoProducto])) //Si las variables enviadas desde el formulario existen
            {
                $DetalleFactura->setCodigoProducto($_POST[$CodigoProducto]);
                
                $CantidadProducto = "CantidadProducto".$ConsecutivoProducto;
                $DetalleFactura->setProductoCantidad($_POST[$CantidadProducto]);


                
                $NomProducto = "NomProduc".$ConsecutivoProducto;
                $DetalleFactura->setNombre_Producto($_POST[$NomProducto]);
                
                $PrecioProducto = "PrecioProducto".$ConsecutivoProducto;
                $DetalleFactura->setValorUnitario($_POST[$PrecioProducto]);


                $ValorTotal = "ValorTotal".$ConsecutivoProducto;
                $DetalleFactura->setValorTotal($_POST[$CantidadProducto]*$_POST[$PrecioProducto]);


                $Total=$DetalleFactura->getValorTotal();
                
                $TotalProductos=$TotalProductos + $Total;
                

                $regitroexitoso=($CrudDetalleFactura::InsertarDetalleFactura($DetalleFactura));// Llamar el método para Insertar
            }
        }

        

        

        if($regitroexitoso==1)
        {
            // $echo $Temp;
            echo "<script>

                        
                        swal.fire({
                            title: 'Éxito',
                            text: 'Cotización registrada exitosamente',
                            type: 'success',
                            confirmButtonText: 'Okey',
                            
                            }).then((result) => {
                              if (result.value) {
                                window.location.href = '../Vista/ListarCotizacion.php';
                              }
                            })
                                  
                            

                        
                </script>";
            // header("Location:../Index.php");
            

           // window.location.href = '../Index.php';
           // echo "<script>
           // window.location.href = '../Vistas/ListadoPropietarios.php';
           // </script>"
        } 
        else
        {
            echo "Problemas en el registro"; 
        }




            
    }
    else
    {
        echo "Problemas en la inserción";
    }


    $Factura = new Factura(); //Crear el objeto Factura
    $CrudFactura = new CrudFactura();

    


    //calculo total productos
    $Factura->setCosto_Total_Produc($TotalProductos);

    //calculo subtotal
    $SubtotalCalculando=$TotalProductos+$ManoObra+$Transporte;
    $Factura->setSubtotal($SubtotalCalculando);

    //calculo total
    $IVAvalordelSubtotal=$SubtotalCalculando*$IVAReal;
    $TotalConIva=$SubtotalCalculando+$IVAvalordelSubtotal;
    $Factura->setTotal($TotalConIva);
    
    $Factura->setCodigoCotizacion($CodigoCotizacionGenerado);
        

    
    $CrudFactura::ModificarSubtotalIngresar($Factura);
    

}elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{   
    $Factura = new Factura(); //Crear el objeto Factura
    $CrudFactura = new CrudFactura();

    $Factura->setCodigoCotizacion($_POST["CodigoCotizacion"]);

    $IDparaCotizacion=$Factura->getCodigoCotizacion();
    $Factura->setCodigoCliente($_POST["CodigoCliente1"]); //Instanciar el atributo
    $Factura->setID_Servicio($_POST["ID_Servicio"]);
    $Factura->setPrimer_Nombre($_POST["Primer_Nombre"]);
    $Factura->setSegundo_Nombre($_POST["Segundo_Nombre"]);
    $Factura->setPrimer_Apellido($_POST["Primer_Apellido"]);
    $Factura->setSegundo_Apellido($_POST["Segundo_Apellido"]);
    $Factura->setDocumento_Iden($_POST["Documento_Iden"]);
    $Factura->setEstadoCoti($_POST["EstadoCoti"]);

    
    //estos no se an calculado los pongo en 0
    $TotalProductos=0;
    $Subtotal= 0;
    $Total=0;
    $Factura->setSubtotal($Subtotal);
    $Factura->setTotal($Total);
    $Factura->setCosto_Total_Produc($TotalProductos);

    $Factura->setCosto_Transporte($_POST["Costo_Transporte"]);
    $Transporte=$Factura->getCosto_Transporte();
    // echo "string $Transporte";

    $Factura->setCosto_Mano_Obra($_POST["Costo_Mano_Obra"]);
    $ManoObra=$Factura->getCosto_Mano_Obra();
    // echo "string $ManoObra";

    $IVA=($_POST["IVA"]);
    // echo "IVA $IVA";
    $IVAReal= $IVA/100;
    // echo "IVA $IVAReal";
    $Factura->setIVA($IVAReal);


    $CodigoCotizacionGenerado=$CrudFactura::ModificarFactura($Factura); 


    $DetalleFactura = new DetalleFactura();
    $CrudDetalleFactura = new CrudDetalleFactura();

    $DetalleFactura->setCodigoCotizacion($_POST["CodigoCotizacion"]);
    $CrudDetalleFactura::EliminarCotizacionDetalle($DetalleFactura);


    // $DetalleFactura = new DetalleFactura();
    // $CrudDetalleFactura = new CrudDetalleFactura();
    
    
    if($CodigoCotizacionGenerado>-1)
    {
        $ProductosAgregar = $_POST["ProductosAgregados"];
        $regitroexitoso = 0;

        
        for($ConsecutivoProducto = 1; $ConsecutivoProducto <= $ProductosAgregar; $ConsecutivoProducto++)
        {
           
            $DetalleFactura->setCodigoCotizacion($IDparaCotizacion);
            
            $CodigoProducto = "CodigoProducto".$ConsecutivoProducto;



            
            if(isset($_POST[$CodigoProducto])) //Si las variables enviadas desde el formulario existen
            {
                $DetalleFactura->setCodigoProducto($_POST[$CodigoProducto]);
                
                $CantidadProducto = "CantidadProducto".$ConsecutivoProducto;
                $DetalleFactura->setProductoCantidad($_POST[$CantidadProducto]);


                
                $NomProducto = "NomProduc".$ConsecutivoProducto;
                $DetalleFactura->setNombre_Producto($_POST[$NomProducto]);
                
                $PrecioProducto = "PrecioProducto".$ConsecutivoProducto;
                $DetalleFactura->setValorUnitario($_POST[$PrecioProducto]);


                $ValorTotal = "ValorTotal".$ConsecutivoProducto;
                $DetalleFactura->setValorTotal($_POST[$CantidadProducto]*$_POST[$PrecioProducto]);


                $Total=$DetalleFactura->getValorTotal();
                
                $TotalProductos=$TotalProductos + $Total;
                

                $regitroexitoso=($CrudDetalleFactura::InsertarDetalleFactura($DetalleFactura));// Llamar el método para Insertar
            }
        }

        

        

        if($regitroexitoso==1)
        {
            // $echo $Temp;
            echo "<script>

                        
                        swal.fire({
                            title: 'Éxito',
                            text: 'Cotización modificada exitosamente',
                            type: 'success',
                            confirmButtonText: 'Okey',
                            
                            }).then((result) => {
                              if (result.value) {
                                window.location.href = '../Vista/ListarCotizacion.php';
                              }
                            })
                                  
                            

                        
                </script>";
            // header("Location:../Index.php");
            

           // window.location.href = '../Index.php';
           // echo "<script>
           // window.location.href = '../Vistas/ListadoPropietarios.php';
           // </script>"
        } 
        else
        {
            echo "Problemas en el registro"; 
        }




            
    }
    else
    {
        echo "Problemas en la inserción";
    }


    $Factura = new Factura(); //Crear el objeto Factura
    $CrudFactura = new CrudFactura();

    


    //calculo total productos
    $Factura->setCosto_Total_Produc($TotalProductos);
    // echo $TotalProductos;
    //calculo subtotal
    $SubtotalCalculando=$TotalProductos+$ManoObra+$Transporte;
    $Factura->setSubtotal($SubtotalCalculando);

    //calculo total
    $IVAvalordelSubtotal=$SubtotalCalculando*$IVAReal;
    $TotalConIva=$SubtotalCalculando+$IVAvalordelSubtotal;
    $Factura->setTotal($TotalConIva);
    
    $Factura->setCodigoCotizacion($IDparaCotizacion);
        

    
    $CrudFactura::ModificarSubtotalIngresar($Factura);



}elseif($_GET["Accion"]=="EliminarCotizacion")
{
    $DetalleFactura = new DetalleFactura();
    $CrudDetalleFactura = new CrudDetalleFactura();
    $CrudDetalleFactura::EliminarCotizacion($_GET["CodigoDetalleCotizacion"]); // Llamar el método para Modificar

        echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Producto modificado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                    // showCancelButton: true,    
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarFacturas.php';
                                
                            
                        }
                            })</script>";


}

?>


</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>