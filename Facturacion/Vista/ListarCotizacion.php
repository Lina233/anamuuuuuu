<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}
require_once('../../Conexion.php'); 
require_once('../Modelo/Factura.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/DetalleFactura.php');
require_once('../Modelo/CrudDetalleFactura.php');
require_once('../Modelo/CrudFactura.php');


$CrudFactura = new CrudFactura(); 
$ListaFactura = $CrudFactura->ListarFactura(); //Llamado al método ListarCompetencia

$CrudDetalleFactura = new CrudDetalleFactura(); //Crear de un objeto CrudCompetencia
$ListaFacturas = $CrudDetalleFactura->ListarFacturas(); //Llamado al método 

require_once('../../Servicio/Modelo/Servicio.php');


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-cotizacion.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<body>
    <div class="modal">
        

        <div class="bodymodal">
            <br>
            <br>
            <br>
            
            
            <form>
                <div class="tabladetalle1">
                    <div class="tabladetalle1-colum1">
                        <br>
                        <p>Nro Cot:</p><input disabled id="CodigoCotizacion" type="" name="">
                        <br>
                        <br>
                        <p>Fecha Cotización:</p><input disabled id="FechaFactura" type="" name="">
                        <br>
                        <br>
                        <p>ID Servicio:</p><input disabled id="ID_Servicio" type="" name="">
                        <br>
                        <br>
                        <p>Estado:</p><input disabled id="EstadoCoti" type="" name="">
                        
                    </div>
                    <div class="tabladetalle1-colum2">
                        <br>
                        <p>Primer Nombre:</p><input disabled id="Primer_Nombre" type="" name="">
                        <br>
                        <br>
                        <p>Primer Apellido:</p><input disabled id="Primer_Apellido" type="" name="">
                        <br>
                        <br>
                        <p>Documento de Identidad:</p><input disabled id="Documento_Iden" type="" name="">
                    </div>
                    <div class="tabladetalle1-colum3">
                        <br>
                        <p>Segundo Nombre:</p><input disabled id="Segundo_Nombre" type="" name="">
                        <br>
                        <br>
                        <p>Segundo Apellido:</p><input disabled id="Segundo_Apellido" type="" name="">
                        <br>
                        <br>
                        <p>ID Cliente:</p><input disabled id="CodigoCliente" type="" name="">
                    </div>
                    <div class="tabladetalle1-colum4">
                        <br>
                        <p>Costo Transporte:</p><input disabled id="Costo_Transporte" type="" name="">
                        <br>
                        <br>
                        <p>Costo Mano de Obra:</p><input disabled id="Costo_Mano_Obra" type="" name="">
                        <br>
                        <br>
                        <p>Costo Total Productos:</p><input disabled id="Costo_Total_Produc" type="" name="">
                    </div>
                    <div class="tabladetalle1-colum5">
                        <br>
                        <p>IVA:</p><input disabled id="IVA" type="" name="">
                        <br>
                        <br>
                        <p>Subtotal:</p><input disabled id="Subtotal" type="" name="">
                        <br>
                        <br>
                        <p>Total/IVA:</p><input disabled id="Total" type="" name="">
                        
                    </div>
                    <div class="btn_ok">
                        <a href="#" title="Salir"  class="closeModal" onclick="closeModal();"><img width="60px" src="https://img.icons8.com/fluent/48/000000/close-window.png"/></a>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        
                    </div>
                </div>
            </form>
            
            
            <br>
            <br>
            <br>
            <br>
         <table class="tabla">
                <thead>
                    <th>Nombre</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th>Valor Total</th>
                </thead>  
            
                <tbody id="tbody">
                    
                </tbody>
            </table>
            

        </div>        
            
     </div>

    <!-- navbar -->
    <?php 
      include('../../vistas/navbar/dashboard.php'); 
    ?>

    <h1 class="titulo1">Gestión de Cotizaciones</h1>
    <div class="form-column-boton1"> 
        
        <button class="btn1" type="button">
            <a href="IngresarFactura.php">Crear Cotización</a>
        </button>
    </div> 
    <div class="form-column-boton2">
        <button class="btn2" type="button">
                <a href="../../TCPDF/examples/reporteCotizacion.php">Reporte PDF</a>
        </button>
    </div>
    <div id="main-container">
    <table>
        <thead>
        <tr>
            <th>ID</th>
            
            <th>Fecha</th>
            <th>ID Cliente</th>
            <th>Servicio</th>
            <th>Subtotal</th>
            
            <th>Total</th>
            <th>Acciones</th>
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaFactura as $Factura1){
                ?>
                <tr>
                    <td><?php echo $Factura1->getCodigoCotizacion(); ?></td>
                    
                    <td><?php echo $Factura1->getFechaFactura(); ?></td>

                    <td><?php
                    // if ($Factura1->getCodigoCliente()==1) 
                    //     {
                    //      echo "Solicitado";
                    //     }
                    // elseif ($Factura1->getCodigoCliente()==2)
                    //     {
                    //      echo "Separación";
                    //     }
                    // elseif ($Factura1->getCodigoCliente()==2)
                    //     {
                    //      echo "Despachado";
                    //     }  
                     echo $Factura1->getCodigoCliente(); 
                     ?></td>
                    
                    

                    
                    
                    <td><?php 
                    require_once('../../Servicio/Modelo/CrudServicio.php');
                    $CrudServicio= new CrudServicio();

                    $ServicioNombreTraer=$Factura1->getID_Servicio();
                             
                    $ServicioNombre = $CrudServicio::ServicioNombreTraer($ServicioNombreTraer);

                    echo $ServicioNombre->getNombre_Servicio(); 
                    ?></td>

                    

                    
                    <td><?php echo $Factura1->getSubtotal(); ?></td>
                    <td><?php echo $Factura1->getTotal(); 
                     ?></td>
                    <td>
                     
                        <a class="link2" href="EditarCotizacion.php?CodigoCotizacion=<?php echo $Factura1->getCodigoCotizacion(); ?>"><img title="Editar" width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a>

                        
                        <a class="Modal" Cotiza="<?php echo $Factura1->getCodigoCotizacion(); ?>" href="#"><img title="Detalle" width="50" src="https://img.icons8.com/fluent/48/000000/search-in-list.png"/></a>

                        <a target="_blank" href="../../TCPDF/examples/ReporteDetalleCotizacion.php?CodigoCotizacion=<?php echo $Factura1->getCodigoCotizacion(); ?>" ><img title="PDF" width="50" src="https://img.icons8.com/color/48/000000/pdf.png"/></a>


                    </td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
    </div>
</body>
<script type="text/javascript">
    

    
    //modal
    $('.Modal').click(function(event){

        event.preventDefault();
        var cotizacion= $(this).attr('Cotiza');
        var dataen= 'cotizacion='+ cotizacion;
        // alert(cotizacion);

        //con esto puedo asignar un valor aun imput que tenga este ID
        $('#prob2').val(cotizacion);


        $('.modal').fadeIn();

        $(function(){
            tabla();
        });

        $.ajax({
            type:'post',
            url:'../Controlador/ControladorListarCotizacion.php',

            data:dataen,
                success:function(r)
                {  
                    // $('#prob3').html(r);
                    var js1= JSON.parse(r);  
                    // alert(js1);
                    for (var i = 0; i < js1.length; i++) {
                        
                        var Primer_Nombre=js1[i].Primer_Nombre;
                        $('#Primer_Nombre').val(Primer_Nombre);

                        var CodigoCotizacion=js1[i].CodigoCotizacion;
                        $('#CodigoCotizacion').val(CodigoCotizacion);

                        var CodigoCliente=js1[i].CodigoCliente;
                        $('#CodigoCliente').val(CodigoCliente);

                        var ID_Servicio=js1[i].ID_Servicio;
                        $('#ID_Servicio').val(ID_Servicio);

                        var Subtotal=js1[i].Subtotal;
                        $('#Subtotal').val(Subtotal);

                        var Total=js1[i].Total;
                        $('#Total').val(Total);

                        var Costo_Transporte=js1[i].Costo_Transporte;
                        $('#Costo_Transporte').val(Costo_Transporte);

                        var Costo_Mano_Obra=js1[i].Costo_Mano_Obra;
                        $('#Costo_Mano_Obra').val(Costo_Mano_Obra);

                        var Costo_Total_Produc=js1[i].Costo_Total_Produc;
                        $('#Costo_Total_Produc').val(Costo_Total_Produc);

                        var IVA=js1[i].IVA;
                        $('#IVA').val(IVA);

                        var Segundo_Nombre=js1[i].Segundo_Nombre;
                        $('#Segundo_Nombre').val(Segundo_Nombre);

                        var Primer_Apellido=js1[i].Primer_Apellido;
                        $('#Primer_Apellido').val(Primer_Apellido);

                        var Segundo_Apellido=js1[i].Segundo_Apellido;
                        $('#Segundo_Apellido').val(Segundo_Apellido);

                        var Documento_Iden=js1[i].Documento_Iden;
                        $('#Documento_Iden').val(Documento_Iden);

                        var EstadoCoti=js1[i].EstadoCoti;
                        $('#EstadoCoti').val(EstadoCoti);

                        var FechaFactura=js1[i].FechaFactura;
                        $('#FechaFactura').val(FechaFactura);

                    } 
                    

                }

            

             });    

        function tabla(){

            $.ajax({
            type:'post',
            url:'../Controlador/ControladorListar.php',

            data:dataen,
                success:function(r)
                {
                      

                    // $('#prob3').html(r);
                    var js= JSON.parse(r);  
                    // alert(js);
                    var tabla;
                    for (var i = 0; i < js.length; i++) {
                        
                        tabla+='<tr><td>'+js[i].Nombre_Producto+'</td><td>'+js[i].CodigoProducto+'</td><td>'+js[i].ProductoCantidad+'</td><td>'+js[i].ValorUnitario+'</td><td>'+js[i].ValorTotal+'</td></tr>';

                    } 
                    $('#tbody').html(tabla);

                }

            

             });

        }

     });


    //funcion para cerrar con el boton salir el modal
    function closeModal()
    {
        $('.modal').fadeOut();

        //este codigo hace que no se ejecute la accion de recargar la pagina al dar clic
        event.preventDefault();
    }


</script>
</html>