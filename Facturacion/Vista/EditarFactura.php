<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}
require_once('../../Conexion.php');
require_once('../Modelo/DetalleFactura.php');
require_once('../Modelo/CrudDetalleFactura.php');

$CrudDetalleFactura = new CrudDetalleFactura(); //Crear un Objeto CrudCompetencia
$DetalleFactura = $CrudDetalleFactura::ObtenerDetalle($_GET["CodigoDetalleCotizacion"]);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Modificar</title>
    
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-editar-cotizacion.css">
    <title>Modificar</title>
</head>
<body>
    
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <form action="../Controlador/ControladorFacturacion.php" method="post">
        <h1 class="titulo1">Editar Cotizacion</h1>
        <div class="diiv-2">
        <p>ID Detalle Cotizacion</p>
        <input  readonly="disabled" type="text" name="CodigoDetalleCotizacion" id="CodigoDetalleCotizacion" value="<?php echo $DetalleFactura->getCodigoDetalleCotizacion(); ?>">
        <br>
        <p>ID Producto:</p> 
        <input type="text" required name="CodigoProducto" id="CodigoProducto" value="<?php echo $DetalleFactura->getCodigoProducto(); ?>">
        <br>
        <p>Valor Unitaro</p>
        <input pattern="[0-9.]+" required type="text" name="ValorUnitario" id="ValorUnitario" value="<?php echo $DetalleFactura->getValorUnitario(); ?>">
        <br>
        <label id="validacion_ValorUnitario" class="alerta"></label>
        <br>
        </div>
        <div class="diiv-3">
        <p>Cantidad Producto</p> 
        <input pattern="[0-9]+" required type="text" name="ProductoCantidad" id="ProductoCantidad" value="<?php echo $DetalleFactura->getProductoCantidad(); ?>">
        <br>
        <label id="validacion_ProductoCantidad" class="alerta"></label>
        

        <p>Estado</p> 
        <select  name="CodigoEstado" id="CodigoEstado" value="<?php echo $DetalleFactura->getCodigoEstado(); ?>">
            <option value="1">Solicitado</option>
            <option value="2">Separación</option>
            <option value="3">Despachado</option>
        </select>
        <br>
        </div>

        
        <!-- <button  type="button" name="Agreg" id="Agreg" >Agregar</button> -->
        <!-- <input type="hidden" name="Agreg" id="Agreg">
        <button type="button">Modificar</button> -->
        <br>
        <div class="form-column-boton1">
            <input type="hidden" name="Modificar" id="Modificar">
            <button class="btn1" type="submit">Modificar</button>
        </div>
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarFacturas.php">Cancelar</a>
            </button>
        </div>
    </form>
</body>

<script>
// $(document).ready(function(){


//     $("#Agreg").click(function(){
//         let validado=0;
//         //Validación del tipo documento
        
        


        

//         //Validación del PrecioProducto
//         if($("#ValorUnitario").val().length == 0 ){
//          $("#validacion_ValorUnitario").text("Precio obligatorio");
//         }
//         else
//         {
//          $("#validacion_ValorUnitario").text("");
//          validado++;
//         }

//         //Validación del CantidadProducto
//         if($("#ProductoCantidad").val().length == 0 ){
//          $("#validacion_ProductoCantidad").text("Cantidad Obligatoria");
//         }
//         else
//         {
//          $("#validacion_ProductoCantidad").text("");
//          validado++;
//         }
        

        
//         // validacion todos los campos
//         if(validado==2) //Si validado == al número de campos que requieren validación entocnces se registra
//          {

//             // AgregarDetalle();
              
           
//             alert("Registro exitosddddo");
//             //Limpiar cajas de texto, select, textarea, checkbox, radiobutton
            

           
            

            
//          }
//     });
// })

</script>
</html>