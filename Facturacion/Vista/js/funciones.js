




// validacion hojas tecnicas
$(document).ready(function(){
	$("#btnregistrarplantas").click(function(){
		//Validación del tipo panta
		let validado=0;
		if($("#tipoplanta").val().length == 0 ){
			$("#validacion_tipoplanta").text("tipo Planta obligatorio");
		}
		else
		{
			$("#validacion_tipoplanta").text("");
			validado++;
		}

		//Validación ubicacion
		if($("#ubicacion").val().length == 0 ){
			$("#validacion_ubicacion").text("ubicacion obligatoria");
		}
		else
		{
			$("#validacion_ubicacion").text("");
			validado++;
		}

		//Validación de DESCRIPCION
		if($("#descripcion").val().length == 0){
			$("#validacion_descripcion").text("Descripcion obligatoria");
		}
		else if($("#descripcion").val().length > 200)
		{
			$("#validacion_descripcion").text("Las descripcion no debe superar los 200 caracteres");
		}
		else
		{
			$("#validacion_descripcion").text("");
			validado++;
		}

		//Validación del nombre comun
		if($("#nombrecomun").val().length == 0 ){
			$("#validacion_nombrecomun").text("campo obligatorio");
		}
		else if($("#nombrecomun").val().length > 30 )
		{
			$("#validacion_nombrecomun").text("El número de caractes no debe superar los 30.");
		}
		else
		{
			$("#validacion_nombrecomun").text("");
			validado++;
		}

		//Validación del nombre cientifico
		if($("#nombrecientifico").val().length == 0 ){
			$("#validacion_nombrecientifico").text("campo obligatorio");
		}
		else if($("#nombrecientifico").val().length > 30 )
		{
			$("#validacion_nombrecientifico").text("El número de caractes no debe superar los 30.");
		}
		else
		{
			$("#validacion_nombrecientifico").text("");
			validado++;
		}


		// validacion todos los campos
		if(validado==5) //Si validado == al número de campos que requieren validación entocnces se registra
		 {
		 	alert("Registro exitoso");
		 	//Limpiar cajas de texto, select, textarea, checkbox, radiobutton
		 	$("input").val("");
		 	$("select").val("");
		 	$("textarea").val("");
		 	$("input[name='condiciones']").prop('checked',false); //Limpiar un checkbox
		 	$("input[name='genero']").prop('checked',false); //Limpiar un checkbox
		 }
	});
})

$(document).ready(function() {
	//Validación de Sólo números
 	$(".solo_numeros").on("keyup",function(){
 		 this.value = this.value.replace(/[^0-9]/g,''); //Validación
 	});

 	//Validación de Sólo letras
 	$(".solo_letras").on("keyup",function(){
 		 this.value = this.value.replace(/[^A-Za-z]/g,''); //Validación solo letras
 	});
 });

