<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}
require_once("../../Conexion.php");
require_once('../../Producto/Modelo/Producto.php');
require_once('../../Producto/Modelo/CrudProducto.php');
$CrudProducto = new CrudProducto();
$TraerProductos = $CrudProducto->TraerProductos();

require_once('../../Usuario/Modelo/Usuario.php');
require_once('../../Usuario/Modelo/CrudUsuario.php');
$CrudUsuario = new CrudUsuario(); 
$TraerUsuario = $CrudUsuario->TraerUsuario();

require_once('../../Servicio/Modelo/Servicio.php');
require_once('../../Servicio/Modelo/CrudServicio.php');
$CrudServicio = new CrudServicio(); 
$TraerServicio = $CrudServicio->TraerServicios();


require_once('../../IVA/Modelo/IVA.php');
require_once('../../IVA/Modelo/CrudIVA.php');
$CrudIVA = new CrudIVA(); 
$TraerIVA = $CrudIVA->TraerIVA();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-ingresar-cotizacion.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

 <!-- formulario -->

    <div class="diiv-1">
    
    <form id="ssx"  class="form-1" action="../Controlador/ControladorFacturacion.php" method="POST">
    <h1 class="titulo-1">Crear Cotización</h1>    
        
    <div class="form-column1">
        <p>Servicio:</p>
        <select name="ID_Servicio" id="ID_Servicio">       
                <option></option>       
              <?php
            foreach($TraerServicio as $Servicio1){
                ?>            
                <option 

                value="<?php
                echo $Servicio1->getID_Servicio();
                
                
                ?>">
                    <?php echo $Servicio1->getNombre_Servicio(); ?>
                </option>             
                <?php
            }
        ?>
        </select> 
        <br> 
        <label id="validacion_Servicio" class="alerta"></label>
        
        <p>Estado:</p>
        <select name="EstadoCoti" id="EstadoCoti">
            <option value=""></option>
            <option value="Solicitado">Solicitado</option>
            <option value="Separado">Separado</option>
            <option value="Despachado">Despachado</option>
        </select>  
        <br>
        <label id="validacion_Estado" class="alerta"></label>
        
        <p>Cliente</p>
        <select name="CodigoCliente" id="CodigoCliente">       
                <option value="0"></option>       
              <?php
            foreach($TraerUsuario as $Usuario1){
                ?>            
                <option 

                value="<?php
                echo $Usuario1->getIdUsuario();
                echo ",";
                echo $Usuario1->getDocumento_Iden(); 
                echo ",";
                echo $Usuario1->getPrimer_Nombre();
                echo ",";
                echo $Usuario1->getSegundo_Nombre();
                echo ",";
                echo $Usuario1->getPrimer_Apellido();
                echo ",";
                echo $Usuario1->getSegundo_Apellido();
                ?>">
                    <?php echo $Usuario1->getNombreUsuario(); ?>
                </option>             
                <?php
            }
        ?>
        </select> 
        <br>
        <label  id="validacion_CodigoCliente" class="alerta"></label>
        <input hidden="" type="" id="CodigoCliente1" name="CodigoCliente1">
        <input hidden="" type="" id="Documento_Iden" name="Documento_Iden">
        <input hidden="" type="" id="Primer_Nombre" name="Primer_Nombre">
        <input hidden="" type="" id="Segundo_Nombre" name="Segundo_Nombre">
        <input hidden="" type="" id="Primer_Apellido" name="Primer_Apellido">
        <input hidden="" type="" id="Segundo_Apellido" name="Segundo_Apellido">
        <br>
        
         
        <p>Producto:</p>
        <select name="valor" id="valor">       
                <option></option>       
              <?php
            foreach($TraerProductos as $Producto1){
                ?>            
                <option 
                
                value="<?php
                echo $Producto1->getID_Producto();
                echo ",";
                echo $Producto1->getPrecio();
                echo ",";
                echo $Producto1->getNombre_Producto();
                
                ?>">
                    <?php echo $Producto1->getNombre_Producto(); ?>
                </option>             
                <?php
            }
        ?>
        </select>
        <br>
        <label id="validacion_CodigoProducto" class=""></label>
        <input hidden="" id="CodPro" value="" type="" name="">
        <input hidden="" id="NomProducto" value="" type="" name="">
        
        <p>Precio:</p>
        <input class="field solo_numeros" type="text" id="PrecioProducto" name="PrecioProducto" 
        value=""
        >
        <br>
        <label id="validacion_PrecioProducto" class=""></label>    
    </div>


    
    
    <div class="form-column2">
        
        <p>Costo Transporte:</p>
        <input class="field solo_numeros" type="text" id="Costo_Transporte" name="Costo_Transporte">
        <br>
        <label id="validacion_Transporte" class="alerta"></label>
        
        <p>Costo Mano Obra:</p>
        <input class="field solo_numeros" type="text" id="Costo_Mano_Obra" name="Costo_Mano_Obra">
        <br>
        <label id="validacion_mano_obra" class="alerta"></label>

        <p>IVA:</p>
        <select name="IVA" id="IVA">       
                       
              <?php
            foreach($TraerIVA as $IVA){
                ?>            
                <option 

                value="<?php
                echo $IVA->getIVA();
                
                ?>">
                    <?php echo $IVA->getIVA(); ?>
                </option>             
                <?php
            }
        ?>
        </select> 
        <br>
        <label id="validacion_IVA" class="alerta"></label>
        <br>
        
        <p>Cantidad Producto:</p>
        <input class="field solo_numeros" type="text" id="CantidadProducto" name="CantidadProducto">
        <br>
        <label id="validacion_CantidadProducto" class="alerta"></label>
        

    </div>

    <div class="form-column-boton1">
            <button class="btn1" type="button" name="AgregarProducto" id="AgregarProducto" >Agregar</button>
            
    </div>
    
    <div class="form-column-boton3">
            <button class="btn3" type="button">
                <a href="ListarCotizacion.php">Cancelar</a>
            </button>
    </div>
    <div class="form-column-boton2">        
            <input type="hidden" name="Registrar" id="Registrar">
            <button  hidden=""  class="btn2" id="RegistrarBoton" type="submit">Registrar Cotización</button>
    </div>
        
    <br>
    <label id="validacion_BotonRegistrar"></label>    

        <table  id="ListaProductos" >
        <thead>
        <th>Nombre</th>
        <th>ID Producto</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th>Total Detalle</th>
        
        <th>Acciones</th>
        </thead>
        <tbody id="ListaProductosbody" name="ListaProductosbody"></tbody>
        </table>
        <input type="text"  hidden="" id="ProductosAgregados" name="ProductosAgregados" value="0">
    </form>
    
    
    </div>
    <script src="js/funciones.js"></script>
</body>
<script>
    
    




    $(document).ready(function(){

        // var count=0;
        // $("#BotonRegistrar").val(count);

       
        if($("#BotonRegistrar").val() == 0 ){
            
             document.getElementById("RegistrarBoton").hidden = false;
        }
        else
        {
            
        }

            



        $('#valor').on('change',function(){
                    
                     // var rr="'fffff'";
                     // alert(rr)

                    //coger el value del option seleccionado
                    var id=$('#valor').val(); 

                    var arr =id.split(",");

                        $("#CodPro").val(arr[0]);
                        $("#PrecioProducto").val(arr[1]);
                        $("#NomProducto").val(arr[2]);

        });


        $('#CodigoCliente').on('change',function(){
                    
                    //coger el value del option seleccionado
                    var CodigoCliente=$('#CodigoCliente').val(); 

                    var arr =CodigoCliente.split(",");
                        $("#CodigoCliente1").val(arr[0]);
                        
                        $("#Documento_Iden").val(arr[1]);
                        $("#Primer_Nombre").val(arr[2]);
                        $("#Segundo_Nombre").val(arr[3]);
                        $("#Primer_Apellido").val(arr[4]);
                        $("#Segundo_Apellido").val(arr[5]);

        });



    $("#AgregarProducto").click(function(){
        let validado=0;
        //Validación del tipo documento

        if($("#IVA").val() == 0 ){
            $("#validacion_IVA").text("IVA obligatorio");
        }
        else
        {
            $("#validacion_IVA").text("");
            validado++;
        }

        if($("#Costo_Mano_Obra").val() == 0 ){
            $("#validacion_mano_obra").text("Costo obligatorio");
        }
        else
        {
            $("#validacion_mano_obra").text("");
            validado++;
        }
        
        if($("#Costo_Transporte").val() == 0 ){
            $("#validacion_Transporte").text("Costo obligatorio");
        }
        else
        {
            $("#validacion_Transporte").text("");
            validado++;
        }

        if($("#CodigoCliente").val() == 0 ){
            $("#validacion_CodigoCliente").text("Cliente obligatorio");
        }
        else
        {
            $("#validacion_CodigoCliente").text("");
            validado++;
        }
        
        if($("#ID_Servicio").val() == 0 ){
            $("#validacion_Servicio").text("Servicio obligatorio");
        }
        else
        {
            $("#validacion_Servicio").text("");
            validado++;
        }

        if($("#EstadoCoti").val() == 0 ){
            $("#validacion_Estado").text("Estado obligatorio");
        }
        else
        {
            $("#validacion_Estado").text("");
            validado++;
        }


        if($("#valor").val() == 0 ){
            $("#validacion_CodigoProducto").text("Producto obligatorio");
        }
        else
        {
            $("#validacion_CodigoProducto").text("");
            validado++;
        }

        //Validación del PrecioProducto
        if($("#PrecioProducto").val() == 0 ){
         $("#validacion_PrecioProducto").text("Precio obligatorio");
        }
        else
        {
         $("#validacion_PrecioProducto").text("");
         validado++;
        }

        //Validación del CantidadProducto
        if($("#CantidadProducto").val() == 0 ){
         $("#validacion_CantidadProducto").text("Cantidad Obligatoria");
        }
        else
        {
         $("#validacion_CantidadProducto").text("");
         validado++;
        }
        

        
        // validacion todos los campos
        if(validado==9) //Si validado == al número de campos que requieren validación entocnces se registra
         {

            AgregarDetalle();
              
            // alert("Registro exitosddddo");
            //Limpiar cajas de texto, select, textarea, checkbox, radiobutton           
         }
    });
})

    

    function AgregarDetalle()
            {
                var g = document.createElement('div');
                g.id = 'Idisito';
                $('#ssx').append(g);
                
                let CodigoProducto = $('#CodPro').val();
                
                let NombreProducto = $('#NomProducto').val();
                let CantidadProducto = $('#CantidadProducto').val();
                let PrecioProducto = $('#PrecioProducto').val();
                let ValorDetalle = CantidadProducto*PrecioProducto;
                
                $('#ProductosAgregados').val(parseInt($('#ProductosAgregados').val()) + 1);
                let ConsecutivoProducto = $('#ProductosAgregados').val();

                
                // document.getElementById("RegistrarBoton").disabled = false;
                // document.getElementById("RegistrarBoton").hidden = false;

                let htmlTags = '<tr id="'+ConsecutivoProducto+'" >' +
                
                '<td>'+ '<input readonly="disabled" type="text" id="NomProduc'+ConsecutivoProducto+'" name="NomProduc'+ConsecutivoProducto+'" value="'+NombreProducto+'">' + '</td>' +
                
                '<td>' + '<input readonly="disabled" type="text" id="CodigoProducto'+ConsecutivoProducto+'" name="CodigoProducto'+ConsecutivoProducto+'" value="'+CodigoProducto+'">'+'</td>' +
                '<td>' + '<input readonly="disabled" class="field solo_numeros" class="inputtable2" type="text" id="CantidadProducto'+ConsecutivoProducto+'" name="CantidadProducto'+ConsecutivoProducto+'" value="'+CantidadProducto+'">'+'</td>' +
                '<td>' + '<input readonly="disabled" class="field solo_numeros" class="inputtable3" type="text" id="PrecioProducto'+ConsecutivoProducto+'" name="PrecioProducto'+ConsecutivoProducto+'" value="'+PrecioProducto+'">'+'</td>' +
                '<td>' + '<input readonly="disabled" class="field solo_numeros" type="text" id="ValorDetalle'+ConsecutivoProducto+'" name="ValorDetalle'+ConsecutivoProducto+'" value="'+ValorDetalle+'">'+'</td>' +
                '<td>' +  '<button class="borrar btn4" type="button" onclick="EliminarDetalle('+ConsecutivoProducto+')" ><img title="Eliminar" src="https://img.icons8.com/color/48/000000/delete-forever.png"/></button>' +'</td>' +
                '</tr>';

               $('#ListaProductos tbody').append(htmlTags);
                
                existe();
               
            }

            
      //funcion para saber si algu objeto con id tales existe      
      function existe()
      {
                //ssx el id del objeto a evaluar
                //Idisito 
                if($('#ssx').find("#Idisito").length){
                    // alert('Si existe');
                    document.getElementById("RegistrarBoton").hidden = false;
                }else{
                    // alert('No existe');
                    document.getElementById("RegistrarBoton").hidden = true;
                }
        }


    function EliminarDetalle(ConsecutivoProducto)
    {
        // alert(ConsecutivoProducto);
        $('#Idisito').remove();

        existe();
    }

    $(function () {
        $(document).on('click', '.borrar', function (event) {
            event.preventDefault();
            $(this).closest('tr').remove();
        });
    });

    $(document).ready(function() {
    //$('#listado_personas').DataTable();
    $(".solo_numeros").on("keyup",function(){
        this.value = this.value.replace(/[^0-9]/g,'');

    });

    $(".solo_letras").on("keyup",function(){
        this.value = this.value.replace(/[^A-Za-z]/g,'');


        });


    } );

</script>
</html>