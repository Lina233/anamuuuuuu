<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}
require_once('../../Conexion.php'); 
require_once('../Modelo/Factura.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/DetalleFactura.php');
require_once('../Modelo/CrudDetalleFactura.php');


$CrudDetalleFactura = new CrudDetalleFactura(); //Crear de un objeto CrudCompetencia
$ListaFacturas = $CrudDetalleFactura->ListarFacturas(); //Llamado al método ListarCompetencia

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-cotizacion.css">
</head>
<body>
    
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <h1 class="titulo1">Gestión de Cotizaciones</h1>
    <div class="form-column-boton1"> 
        
        <button class="btn1" type="button">
            <a href="IngresarFactura.php">Crear Cotizacion</a>
        </button>
    </div> 
    <div class="form-column-boton2">
        <button class="btn2" type="button">
                <a href="../../TCPDF/examples/reporteCotizaciones.php">Reporte PDF</a>
        </button>
    </div>
    <div id="main-container">
    <table>
        <thead>
        <tr>
            <th>ID Cotizacion</th>
            <!-- <th>ID Detalle</th> -->
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Valor Unitario</th>
            <th>Valor Total</th>
            
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaFacturas as $Factura){
                ?>
                <tr>
                    <td><?php echo $Factura->getCodigoCotizacion(); ?></td>
                    <!-- <td><?php echo $Factura->getCodigoDetalleCotizacion(); ?></td> -->
                    <td><?php echo $Factura->getCodigoProducto(); ?></td>
                    <td><?php echo $Factura->getProductoCantidad(); ?></td>
                    <td><?php echo $Factura->getValorUnitario(); ?></td>
                    
                    <td><?php echo $Factura->getValorTotal(); ?></td>
                     <td><?php 
                    //  if ($Factura->getCodigoEstado()==1) 
                    //     {
                    //      echo "Solicitado";
                    //     }
                    // elseif ($Factura->getCodigoEstado()==2)
                    //     {
                    //      echo "Separación";
                    //     }
                    // elseif ($Factura->getCodigoEstado()==2)
                    //     {
                    //      echo "Despachado";
                    //     }  
                      echo $Factura->getCodigoEstado(); 
                     ?></td>
                    <td>
                    <a class="link2" href="EditarFactura.php?CodigoDetalleCotizacion=<?php echo $Factura->getCodigoDetalleCotizacion(); ?>">Editar<img src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a> 



                    <!-- <a href="../Controlador/ControladorFacturacion.php?CodigoDetalleCotizacion=<?php echo $Factura->getCodigoDetalleCotizacion(); ?>&Accion=EliminarCotizacion">Eliminar</a> --></td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
    </div>
</body>
</html>