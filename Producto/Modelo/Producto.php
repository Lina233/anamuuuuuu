<?php
    class Producto{
        //Parámentros de entrada
        private $ID_Producto;
        private $Nombre_Producto;
        private $Precio;
        private $Referencia;
        private $Peso;
        
        private $ID_Categoria;
        private $Hoja_Tecnica;
        private $ID_Estado;
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase

        public function setID_Producto($ID_Producto){
            $this->ID_Producto = $ID_Producto;
        }

        public function getID_Producto(){
            return $this->ID_Producto;
        }

            


        public function setNombre_Producto($Nombre_Producto){
            $this->Nombre_Producto = $Nombre_Producto;
        }

        public function getNombre_Producto(){
            return $this->Nombre_Producto;
        }


        public function setPrecio($Precio){
            $this->Precio = $Precio;
        }

        public function getPrecio(){
            return $this->Precio;
        }

        //
        public function setReferencia($Referencia){
            $this->Referencia = $Referencia;
        }

        public function getReferencia(){
            return $this->Referencia;
        }

        //
        public function setPeso($Peso){
            $this->Peso = $Peso;
        }

        public function getPeso(){
            return $this->Peso;
        }
        
        
        
        
        //
        public function setID_Categoria($ID_Categoria){
            $this->ID_Categoria = $ID_Categoria;
        }

        public function getID_Categoria(){
            return $this->ID_Categoria;
        }

        //
        public function setHoja_Tecnica($Hoja_Tecnica){
            $this->Hoja_Tecnica = $Hoja_Tecnica;
        }

        public function getHoja_Tecnica(){
            return $this->Hoja_Tecnica;
        }

        //
        public function setID_Estado($ID_Estado){
            $this->ID_Estado = $ID_Estado;
        }

        public function getID_Estado(){
            return $this->ID_Estado;
        }
    }
    
    //Testear funcionalidad de clase.
    /*
    $Competencia = new Competencia(); //Crear objeto
    $Competencia->setCodigoCompetencia(27);
    $Competencia->setNombreCompetencia('Python');
    echo "Código Competencia: ".$Competencia->getCodigoCompetencia().
    " NombreCompetencia: ".$Competencia->getNombreCompetencia();
    */
?>