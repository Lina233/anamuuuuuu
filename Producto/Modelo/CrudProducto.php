﻿<?php
//require_once('../../Conexion.php');    
    class CrudProducto{
    
        public function __construct(){
        }

        public function InsertarProducto($Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.

            $Sql =$Db->prepare('INSERT INTO  producto(ID_Producto,Nombre_Producto,Precio,Referencia,Peso,ID_Categoria,Hoja_Tecnica,ID_Estado)
            VALUES(NULL,:Nombre_Producto,:Precio,:Referencia,:Peso,:ID_Categoria,:Hoja_Tecnica,:ID_Estado)');


            $Sql->bindValue('Nombre_Producto',$Producto->getNombre_Producto());

            // $Insert->bindValue('Nombre_Producto',md5($Producto->getNombre_Producto()));
            $Sql->bindValue('Precio',$Producto->getPrecio());
            $Sql->bindValue('Referencia',$Producto->getReferencia());
            $Sql->bindValue('Peso',$Producto->getPeso());
            
            $Sql->bindValue('ID_Categoria',$Producto->getID_Categoria());
            $Sql->bindValue('Hoja_Tecnica',$Producto->getHoja_Tecnica());
            $Sql->bindValue('ID_Estado',$Producto->getID_Estado());
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }

        public function ObtenerProducto($ID_Producto)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM producto WHERE ID_Producto=:ID_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Producto',$ID_Producto);
            $MyProducto = new Producto();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Producto = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyProducto->setID_Producto($Producto['ID_Producto']);
                $MyProducto->setNombre_Producto($Producto['Nombre_Producto']);
                $MyProducto->setPrecio($Producto['Precio']);
                $MyProducto->setReferencia($Producto['Referencia']);
                $MyProducto->setPeso($Producto['Peso']);
                
                $MyProducto->setID_Categoria($Producto['ID_Categoria']);
                $MyProducto->setHoja_Tecnica($Producto['Hoja_Tecnica']);

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyProducto;
        }

        //Listar todos los registros de la tabla
        public function ListarProductos(){
            $Db = Db::Conectar();
            $ListaProductos = [];
            $Sql = $Db->query('SELECT * FROM producto');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Producto){
                $MyProducto = new Producto();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyProducto->setID_Producto($Producto['ID_Producto']);

                $MyProducto->setNombre_Producto($Producto['Nombre_Producto']);

                $MyProducto->setPrecio($Producto['Precio']);

                $MyProducto->setReferencia($Producto['Referencia']);

                $MyProducto->setPeso($Producto['Peso']);

                
                $MyProducto->setID_Categoria($Producto['ID_Categoria']);

                $MyProducto->setHoja_Tecnica($Producto['Hoja_Tecnica']);
                $MyProducto->setID_Estado($Producto['ID_Estado']);
                $ListaProductos[] = $MyProducto;
            }
            return $ListaProductos;
        }

        public function TraerProductos(){
            $Db = Db::Conectar();
            $TraeProductos = [];
            $Sql = $Db->query('SELECT ID_Producto,Nombre_Producto,Precio FROM producto WHERE ID_Estado=1');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Producto){
                $MyProducto = new Producto();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyProducto->setID_Producto($Producto['ID_Producto']);

                $MyProducto->setNombre_Producto($Producto['Nombre_Producto']);

                $MyProducto->setPrecio($Producto['Precio']);

                $TraeProductos[] = $MyProducto;
            }
            return $TraeProductos;
        }

        public function InformacionProductoTraer($ID)
        { 
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT Nombre_Producto FROM Producto 
            WHERE ID_Producto=:ID_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Producto',$ID);
            $MyProducto = new Producto();
            
            
            try{
                $Sql->execute(); 
                $Producto = $Sql->fetch(); 
                $MyProducto->setNombre_Producto($Producto['Nombre_Producto']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyProducto;
        }


        // codigo que puede servir mas adelante no borrar
        //traer producto para la hoja tecnica
        // public function TraerProductosSinHoja(){
        //     $Db = Db::Conectar();
        //     $TraeProductos = [];
            
        //     $Sql = $Db->prepare('SELECT ID_Producto,Nombre_Producto,Precio FROM producto
        //          WHERE Hoja_Tecnica=:Hoja_Tecnica AND 
        //          ID_Tipo_Producto=:ID_Tipo_Producto ');
            
        //     $Tipo_Producto= 'Planta';
        //     $NO= 'No';
        //     // WHERE Hoja_Tecnica
        //     // $NO->['Hoja_Tecnica'];
        //     $Sql->bindValue('Hoja_Tecnica',$NO);
        //     $Sql->bindValue('ID_Tipo_Producto',$Tipo_Producto);

        //     $Sql->execute();
        //     foreach($Sql->fetchAll() as $Producto){
        //         $MyProducto = new Producto();
        //         // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
        //         $NO= 'No';

        //         $MyProducto->setID_Producto($Producto['ID_Producto']);

        //         $MyProducto->setNombre_Producto($Producto['Nombre_Producto']);

        //         $MyProducto->setPrecio($Producto['Precio']);
        //         // $MyProducto->Hoja_Tec($Producto['Hoja_Tecnica']);
                
                
                

        //         $TraeProductos[] = $MyProducto;
        //     }
        //     return $TraeProductos;
        // }

        



        public function ModificarProducto($Producto)
        {
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE producto SET Nombre_Producto=:Nombre_Producto,Precio=:Precio,Referencia=:Referencia,Peso=:Peso,ID_Categoria=:ID_Categoria,Hoja_Tecnica=:Hoja_Tecnica
            WHERE ID_Producto=:ID_Producto'); 
            $Sql->bindValue('ID_Producto',$Producto->getID_Producto());
            $Sql->bindValue('Nombre_Producto',$Producto->getNombre_Producto());
            $Sql->bindValue('Precio',$Producto->getPrecio());
            $Sql->bindValue('Referencia',$Producto->getReferencia());
            $Sql->bindValue('Peso',$Producto->getPeso());
            
            $Sql->bindValue('ID_Categoria',$Producto->getID_Categoria());
            $Sql->bindValue('Hoja_Tecnica',$Producto->getHoja_Tecnica());
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }


        //codigo que puede servir mas adelante no borrar
        // public function ModificarProductoHojaTecnica($Producto){
        //     $Db = Db::Conectar(); //Conectar a la base de datos
        //     //Definir la modificación a realizar.
        //     $Sql = $Db->prepare('UPDATE producto SET Hoja_Tecnica=:Hoja_Tecnica
        //     WHERE Nombre_Producto=:Nombre_Producto'); 

        //     $Si='Si';
        //     $Sql->bindValue('Hoja_Tecnica',$Si);

        //     $Sql->bindValue('Nombre_Producto',$Producto->getNombre_Producto());
            
        //     try{
        //         $Sql->execute(); //Ejecutar el Sql que un Update
        //         // echo "Modificación Exitosa";
        //     }
        //     catch(Exception $e){ //Capturar Errores
        //         echo $e->getMessage(); //Mostar errores en la modificación
        //         die();
        //     }
        // }

        public function ModificarProductoEstado($ID_Producto){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE producto SET ID_Estado=:ID_Estado
            WHERE ID_Producto=:ID_Producto'); 
            $Sql->bindValue('ID_Producto',$ID_Producto);
            
            // echo $ID_Producto;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_Producto=explode(":",$ID_Producto);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_Producto[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('ID_Estado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function EliminarProducto($ID_Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM producto WHERE ID_Producto=:ID_Producto'); 
            $Sql->bindValue('ID_Producto',$ID_Producto);
            $ID_Producto = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>