﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>



<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}


require_once('../../Conexion.php');
require_once('../Modelo/Producto.php'); //Vincular la Clase Competencia
require_once('../Modelo/CrudProducto.php'); //Vincular la Clase Crud

$Producto = new Producto(); //Crear el objeto Competencia
$CrudProducto = new CrudProducto();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Producto->setID_Producto($_POST["ID_Producto"]); //Instanciar el atributo
    
    $Producto->setNombre_Producto($_POST["Nombre_Producto"]); //Instanciar el atributo

    $Producto->setPrecio($_POST["Precio"]); //Instanciar el atributo

    $Producto->setReferencia($_POST["Referencia"]); //Instanciar el atributo

    $Producto->setPeso($_POST["Peso"]); //Instanciar el atributo

     //Instanciar el atributo
    $Producto->setID_Categoria($_POST["ID_Categoria"]);
    $Producto->setHoja_Tecnica($_POST["Hoja_Tecnica"]);
    $Producto->setID_Estado($_POST["ID_Estado"]);
   
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudProducto::InsertarProducto($Producto); // Llamar el método para Insertar


    

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Producto registrado exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarProducto.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    $Producto->setID_Producto($_POST["ID_Producto"]); //Instanciar el atributo
    $Producto->setNombre_Producto($_POST["Nombre_Producto"]); //Instanciar el atributo
    $Producto->setPrecio($_POST["Precio"]);
    $Producto->setReferencia($_POST["Referencia"]);
    $Producto->setPeso($_POST["Peso"]);
    
    $Producto->setID_Categoria($_POST["ID_Categoria"]);
    $Producto->setHoja_Tecnica($_POST["Hoja_Tecnica"]);
    // echo $Producto->getNombre_Producto(); //Verificar instanciación
    $CrudProducto::ModificarProducto($Producto); // Llamar el método para Modificar
    
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Producto modificado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarProducto.php';
                        }
                            })</script>";


}
elseif($_GET["Accion"]=="EditarProductoEstado"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudProducto::ModificarProductoEstado($_GET["ID_Producto"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarProducto.php';
                        }
                            })</script>";
}
elseif($_GET["Accion"]=="EliminarProducto"){
    $CrudProducto::EliminarProducto($_GET["ID_Producto"]); // Llamar el método para Modificar
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Producto Eliminado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarProducto.php';
                        }
                            })</script>";
}

?>

</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>