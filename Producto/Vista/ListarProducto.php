﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/CrudProducto.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Producto.php');

$CrudProducto = new CrudProducto(); //Crear de un objeto CrudCompetencia
$ListaProductos = $CrudProducto->ListarProductos(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listarproducto.css">
</head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>


    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Productos</h1>
    
        <div class="form-column-boton1">
            <button class="btn1" type="button">
            <a href="IngresarProducto.php">Crear  Producto</a>
            </button>
        </div>

        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="../../TCPDF/examples/reporteProducto.php">Reporte PDF</a>
            </button>
        </div>

    <div id="main-container">  
        <table >
        <thead>
        <tr>
            <th>ID</th>
            
            <th>Nombre</th>
            <th>Precio</th>
            <th>Referencia</th>
            <th>Hoja Técnica</th>
            <th>Peso(g)</th>
            
            <th>Categoría</th>
            <th>Acciones</th> 
            
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaProductos as $Producto){
                ?>
                <tr>
                    <td><?php echo $Producto->getID_Producto(); ?></td>
                    
                    <td><?php echo $Producto->getNombre_Producto(); ?></td>
                    <td><?php echo $Producto->getPrecio(); ?></td>
                    <td><?php echo $Producto->getReferencia(); ?></td>
                    <td><?php echo $Producto->getHoja_Tecnica(); ?></td>
                    <td><?php echo $Producto->getPeso(); ?></td>
                    <td hidden=""><?php echo $Producto->getID_Estado(); ?></td>
                    <td><?php echo $Producto->getID_Categoria(); ?></td>
                    <td>
                        <a class="link2" href="EditarProducto.php?ID_Producto=<?php echo $Producto->getID_Producto(); ?>"><img title="Editar" width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a> 
                        
                        <?php
                            if ($Producto->getID_Estado()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorProducto.php?ID_Producto=<?php echo $Producto->getID_Producto();?>:<?php echo $Producto->getID_Estado();?>&Accion=EditarProductoEstado"
                        ><img title="Producto Activado" width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($Producto->getID_Estado()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorProducto.php?ID_Producto=<?php echo $Producto->getID_Producto();?>:<?php echo $Producto->getID_Estado();?>&Accion=EditarProductoEstado"
                        ><img title="Producto Desactivado" width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                    </td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
</body>
</html>