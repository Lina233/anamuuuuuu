<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
// $CrudProducto = new CrudProducto(); //Crear de un objeto CrudCompetencia
// $TraerProductos = $CrudProducto->TraerProductos();
require_once('../../Conexion.php');

require_once('../../Tipo_Producto/Modelo/Tipo_Producto.php');
require_once('../../Tipo_Producto/Modelo/Crud_Tipo_Producto.php');

require_once('../../Categoria/Modelo/Categoria.php');
require_once('../../Categoria/Modelo/Crud_Categoria.php');

require_once('../../HojaTecnica/Modelo/HojaTecnica.php');
require_once('../../HojaTecnica/Modelo/CrudHojaTecnica.php');

$CrudTipoProducto = new CrudTipoProducto(); //Crear de un objeto CrudCompetencia
$TraerTipoProducto = $CrudTipoProducto->TraerTipoProducto();

$CrudCategoria = new CrudCategoria(); //Crear de un objeto CrudCompetencia
$TraerCategoria = $CrudCategoria->TraerCategoria();

$CrudHojaTecnica = new CrudHojaTecnica(); //Crear de un objeto CrudCompetencia
$TraerHojaTecnica = $CrudHojaTecnica->TraerHojaTecnica();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
     <link rel="stylesheet" type="text/css" href="css/estilos-ingresarproducto.css">
    </head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    
    <div class="diiv-1">

        <form action="../Controlador/ControladorProducto.php" method="post">
        <h1 class="titulo1">Crear Producto</h1>
            
        <div class="diiv-2">
                
               
                <p>Precio:</p>
                <input pattern="[0-9]+" required type="text" name="Precio" id="Precio">
                <br>
                <p>Peso(g):</p>
                <input  pattern="[0-9]+" required type="text" name="Peso" id="Peso">
                <br>
                <p>Categoría:</p>
                <select name="ID_Categoria" id="ID_Categoria">
                
                            
                        <?php
                            foreach($TraerCategoria as $Producto){
                                ?>                                
                                <option value="<?php echo $Producto->getID_Categoria(); ?>">


                                    <?php echo $Producto->getNombre_Categoria(); ?>
                                </option>
                                <?php
                            }
                        ?>
                </select>

        </div>          
        <div class="diiv-3">
                <p>Nombre:</p>
                <input pattern="[A-Za-z -ñÑ]+" required type="text" name="Nombre_Producto" id="Nombre_Producto">
                <br>
                
            
        <p>Referencia:</p> 
        <input pattern="[A-Za-z0-9_- ]+" pattern="[0-9]+" required type="text" name="Referencia" id="Referencia">
        <br>   

        <p>Hoja Técnica:</p> 
        <select name="Hoja_Tecnica" id="Hoja_Tecnica">
                
                            
                        <?php
                            foreach($TraerHojaTecnica as $Producto){
                                ?>                                
                                <option value="<?php echo $Producto->getID_Hoja_Tecnica(); ?>">


                                    <?php echo $Producto->getNombre_Comun(); ?>
                                </option>
                                <?php
                            }
                        ?>
                </select>
        <br>    
        <p>Estado:</p>
                <select name="ID_Estado" id="ID_Estado">
                    <option value="1">Activado</option>
                    <option value="2">Desactivado</option>
                    
                </select>
          
        </div>  
              
        <div class="form-column-boton1">
            <input type="hidden" name="Registrar" id="Registrar">
            <button class="btn1" type="submit">Registrar</button>
        </div> 
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarProducto.php">Cancelar</a>
            </button>
        </div>
        <!-- <button class="btn2" type="button">
            <input type="hidden" name="Cancelar" id="Cancelar">
            <button class="btn1" type="submit">Cancelar</button>   
        </div> -->
        <br>
        </div>
    </form>
    
</body>
</html>