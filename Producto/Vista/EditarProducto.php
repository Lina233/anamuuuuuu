<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/Producto.php');
require_once('../Modelo/CrudProducto.php');
require_once('../../Categoria/Modelo/Categoria.php');
require_once('../../Categoria/Modelo/Crud_Categoria.php');
require_once('../../HojaTecnica/Modelo/HojaTecnica.php');
require_once('../../HojaTecnica/Modelo/CrudHojaTecnica.php');

$CrudProducto = new CrudProducto(); //Crear un Objeto CrudCompetencia
$Producto = $CrudProducto::ObtenerProducto($_GET["ID_Producto"]);
$CrudCategoria = new CrudCategoria(); //Crear de un objeto CrudCompetencia
$TraerCategoria = $CrudCategoria->TraerCategoria();
$CrudHojaTecnica = new CrudHojaTecnica(); //Crear de un objeto CrudCompetencia
$TraerHojaTecnica = $CrudHojaTecnica->TraerHojaTecnica();


$NombreCategoria = $CrudCategoria::ObtenerNombreCategoriaProducto($_GET["ID_Producto"]);
$NombreComun = $CrudHojaTecnica::ObtenerNombreComunHojasTecnicas($_GET["ID_Producto"]);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
   <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-editarproducto.css">
    <title>Modificar</title>
</head>
<body>
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <div class="diiv-1">
    
        <form action="../Controlador/ControladorProducto.php" method="post">
        <h1 class="titulo1">Editar Producto</h1>  
        <div class="diiv-2">
            
            
            <input hidden="" required pattern="[0-9]+" readonly="disabled" type="text" name="ID_Producto" id="ID_Producto" value="<?php echo $Producto->getID_Producto(); ?>">
            
            <p>Precio:</p> <input required pattern="[0-9]+" type="text" name="Precio" id="Precio" value="<?php echo $Producto->getPrecio(); ?>">
            <br>
            <p> Peso(g):</p> <input required pattern="[0-9]+" type="text" name="Peso" id="Peso" value="<?php echo $Producto->getPeso(); ?>">
            <br>
            
            
            <p>Categoría:</p>
                <select name="ID_Categoria" id="ID_Categoria" >
                
                        <option value="<?php echo $Producto->getID_Categoria();?>">
                            <?php echo $NombreCategoria->getNombre_Categoria();?>
                        </option>    
                        <?php
                            foreach($TraerCategoria as $Productos){
                                ?>                                
                                <option value="<?php echo $Productos->getID_Categoria(); ?>">


                                    <?php echo $Productos->getNombre_Categoria(); ?>
                                </option>
                                <?php
                            }
                        ?>
                </select>
            
           



        
        </div>
        <div class="diiv-3">
            
            <p>Nombre</p>
            <input pattern="[A-Za-z -ñÑ]+" required type="text" name="Nombre_Producto" id="Nombre_Producto" 
            value="<?php echo $Producto->getNombre_Producto(); ?>"> 
            <br>
            
            <p>Referencia:</p>
                <input  required pattern="[A-Za-z0-9_- ]+"  type="text" name="Referencia" id="Referencia"value="<?php echo $Producto->getReferencia(); ?>">
            
            
            
            <p>Hoja Técnica:</p> 
            <select name="Hoja_Tecnica" id="Hoja_Tecnica">
                
                           <option value="<?php echo $Producto->getHoja_Tecnica(); ?>"><?php echo $NombreComun->getNombre_Comun(); ?></option> 
                        <?php
                            foreach($TraerHojaTecnica as $Producto){
                                ?>                                
                                <option value="<?php echo $Producto->getID_Hoja_Tecnica(); ?>">


                                    <?php echo $Producto->getNombre_Comun(); ?>
                                </option>
                                <?php
                            }
                        ?>
                </select>  
        <br>


        </div>  
              
        <div class="form-column-boton1">
            <input  type="hidden" name="Modificar" id="Modificar">
            <button class="btn1" type="submit">Editar</button>
        </div> 
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarProducto.php">Cancelar</a>
            </button>
        </div>
        <br>
    
        </form>
    </div>
    </body>
</html>