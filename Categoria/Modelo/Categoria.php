<?php
    class Categoria{
        //Parámentros de entrada
        private $ID_Categoria;
        private $Nombre_Categoria;
        private $Estado;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setID_Categoria($ID_Categoria){
            $this->ID_Categoria = $ID_Categoria;
        }

        public function getID_Categoria(){
            return $this->ID_Categoria;
        }

        //
        public function setNombre_Categoria($Nombre_Categoria){
            $this->Nombre_Categoria = $Nombre_Categoria;
        }

        public function getNombre_Categoria(){
            return $this->Nombre_Categoria;
        }

        //
        public function setEstado($Estado){
            $this->Estado = $Estado;
        }

        public function getEstado(){
            return $this->Estado;
        }



        
    
    }
?>