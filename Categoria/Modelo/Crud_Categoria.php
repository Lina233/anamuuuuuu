﻿<?php
//require_once('../../Conexion.php');    
    class CrudCategoria{
    
        public function __construct(){
        }

        public function InsertarCategoria($Categoria){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO  Categoria(ID_Categoria,Nombre_Categoria,Estado)
            VALUES(NULL,:Nombre_Categoria,1)');
 

            // $Sql->execute();
            $Sql->bindValue('Nombre_Categoria',$Categoria->getNombre_Categoria());

            
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }

        public function TraerCategoria(){
            $Db = Db::Conectar();
            $TraeTipoProductos = [];
            $Sql = $Db->query('SELECT ID_Categoria,Nombre_Categoria FROM Categoria WHERE Estado=1');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Categoria){
                $MyCategoria = new Categoria();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyCategoria->setID_Categoria($Categoria['ID_Categoria']);

                $MyCategoria->setNombre_Categoria($Categoria['Nombre_Categoria']);          
                $TraeCategoria[] = $MyCategoria;
            }
            return $TraeCategoria;
        }

        public function ObtenerCategoria($ID_Categoria)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM Categoria WHERE ID_Categoria=:ID_Categoria'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Categoria',$ID_Categoria);
            $MyCategoria = new Categoria();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Categoria = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyCategoria->setID_Categoria($Categoria['ID_Categoria']);
                $MyCategoria->setNombre_Categoria($Categoria['Nombre_Categoria']);
                

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyCategoria;
        }

        public function ObtenerNombreCategoriaProducto($ID_Producto)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT Categoria.Nombre_Categoria 
            FROM Producto
            INNER JOIN  Categoria
            ON Producto.ID_Categoria = Categoria.ID_Categoria
            WHERE ID_Producto=:ID_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Producto',$ID_Producto);
            $MyProducto = new Producto();
            $MyCategoria = new Categoria();//Crear un objeto de tipo competencia
            
            try{
                $Sql->execute(); 
                $Nombre_Categoria = $Sql->fetch(); 
                $MyCategoria->setNombre_Categoria($Nombre_Categoria['Nombre_Categoria']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyCategoria;
        }

        //Listar todos los registros de la tabla
        public function ListarCategoria(){
            $Db = Db::Conectar();
            $ListaCategoria = [];
            $Sql = $Db->query('SELECT * FROM Categoria');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Categoria){
                $MyCategoria = new Categoria();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyCategoria->setID_Categoria($Categoria['ID_Categoria']);

                $MyCategoria->setNombre_Categoria($Categoria['Nombre_Categoria']);
                $MyCategoria->setEstado($Categoria['Estado']);
                $ListaCategoria[] = $MyCategoria;
            }
            return $ListaCategoria;
        }

         

        public function ModificarCategoria($Categoria){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE Categoria SET Nombre_Categoria=:Nombre_Categoria
            WHERE ID_Categoria=:ID_Categoria'); 
            $Sql->bindValue('ID_Categoria',$Categoria->getID_Categoria());
            $Sql->bindValue('Nombre_Categoria',$Categoria->getNombre_Categoria());
            
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarCategoriaEstado($ID_Categoria){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE Categoria SET Estado=:Estado
            WHERE ID_Categoria=:ID_Categoria'); 
            $Sql->bindValue('ID_Categoria',$ID_Categoria);
            
            // echo $ID_Producto;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_Categoria=explode(":",$ID_Categoria);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_Categoria[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('Estado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }    

        public function EliminarTipoProducto($ID_Tipo_Producto){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM tipo_producto WHERE ID_Tipo_Producto=:ID_Tipo_Producto'); 
            $Sql->bindValue('ID_Tipo_Producto',$ID_Tipo_Producto);
            // $ID_Producto = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>