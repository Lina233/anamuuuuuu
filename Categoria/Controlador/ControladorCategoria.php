﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>



<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}


require_once('../../Conexion.php');
require_once('../Modelo/Categoria.php'); //Vincular la Clase Competencia
require_once('../Modelo/Crud_Categoria.php'); //Vincular la Clase Crud

$Categoria = new Categoria(); //Crear el objeto Competencia
$CrudCategoria = new CrudCategoria();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    // $Tipo_Producto->setID_Tipo_Producto($_POST["ID_Tipo_Producto"]); //Instanciar el atributo
    
    $Categoria->setNombre_Categoria($_POST["Nombre_Categoria"]); //Instanciar el atributo

    

   
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudCategoria::InsertarCategoria($Categoria); // Llamar el método para Insertar

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Categoria registrada exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarCategoria.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    $Categoria->setID_Categoria($_POST["ID_Categoria"]); //Instanciar el atributo
    $Categoria->setNombre_Categoria($_POST["Nombre_Categoria"]); //Instanciar el atributo
    
    $CrudCategoria::ModificarCategoria($Categoria); // Llamar el método para Modificar

    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Categoria modificada exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarCategoria.php';
                        }
                            })</script>";


}elseif($_GET["Accion"]=="EditarCategoriaEstado"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudCategoria::ModificarCategoriaEstado($_GET["ID_Categoria"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarCategoria.php';
                        }
                            })</script>";
}
elseif($_GET["Accion"]=="EliminarProducto"){
    $CrudTipoProducto::EliminarTipoProducto($_GET["ID_Tipo_Producto"]); // Llamar el método para Modificar
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Producto Eliminado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarTipoProducto.php';
                        }
                            })</script>";
}

?>

</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>