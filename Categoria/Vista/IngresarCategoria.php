<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
// $CrudProducto = new CrudProducto(); //Crear de un objeto CrudCompetencia
// $TraerProductos = $CrudProducto->TraerProductos();



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-ingresar-categoria.css">
</head>
<body>
    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <!-- formulario -->
    <form action="../Controlador/ControladorCategoria.php" method="post">
        <h1 class="titulo1" >Crear Categoría</h1>

        <!-- ID Tipo Producto: <input required pattern="[0-9]+" type="text" name="ID_Tipo_Producto" id="ID_Tipo_Producto"> -->
        <br>
        <p>Nombre</p>
        <input pattern="[A-Za-z ]+" required type="text" name="Nombre_Categoria" id="Nombre_Categoria">
        <br>   
        <div class="form-column-boton1">
            <input type="hidden" name="Registrar" id="Registrar">
            <button class="btn1" type="submit">Registrar</button>
        </div> 
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarCategoria.php">Cancelar</a>
            </button>
        </div>
    </form>
</body>
</html>