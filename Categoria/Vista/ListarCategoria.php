﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/Crud_Categoria.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Categoria.php');

$CrudCategoria = new CrudCategoria(); //Crear de un objeto CrudCompetencia
$ListaCategoria = $CrudCategoria->ListarCategoria(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listar-categoria.css">
    
</head>
<body>
    
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Categorías</h1>
    <div class="form-column-boton1">    
        <button class="btn1" type="button">
            <a href="IngresarCategoria.php">Crear Categoría</a>
        </button>
    </div> 
    
    <div id="main-container">
        <table>
            <thead>
            <tr>
                <th class="th1">ID Categoria</th>
                <th>Nombre</th>
                <th>Acciones</th>                   
            </tr>
            </thead>

            <tbody>
            <?php
                foreach($ListaCategoria as $Categoria){
                    ?>
                    <tr>
                        <td><?php echo $Categoria->getID_Categoria(); ?></td>
                        <td><?php echo $Categoria->getNombre_Categoria(); ?></td>
                        <td hidden=""><?php echo $Categoria->getEstado(); ?></td>
                        <td>
                        <a class="link2" href="EditarCategoria.php?ID_Categoria=<?php echo $Categoria->getID_Categoria(); ?>"><img width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a> 

                        <?php
                            if ($Categoria->getEstado()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorCategoria.php?ID_Categoria=<?php echo $Categoria->getID_Categoria();?>:<?php echo $Categoria->getEstado();?>&Accion=EditarCategoriaEstado"
                        ><img title="Categoria Activada" width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($Categoria->getEstado()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorCategoria.php?ID_Categoria=<?php echo $Categoria->getID_Categoria();?>:<?php echo $Categoria->getEstado();?>&Accion=EditarCategoriaEstado"
                        ><img title="Categoria Desactivada" width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                        </td>
                    </tr>
                    <?php
                }
            ?>
            </tbody>
        </table>
    </div>
</body>
</html>