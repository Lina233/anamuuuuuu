﻿<?php
//require_once('../../Conexion.php');    
    class CrudIVA{
    
        public function __construct(){
        }

        public function InsertarIVA($IVA){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO  IVA(ID_IVA,IVA,Estado_IVA)
            VALUES(NULL,:IVA,:Estado_IVA)');
 

            // $Sql->execute();
            $Sql->bindValue('IVA',$IVA->getIVA());
            $uno=1;
            $Sql->bindValue('Estado_IVA',$uno);

            
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }
        public function ObtenerServicio($ID_Servicio)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM servicios WHERE ID_Servicio=:ID_Servicio'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Servicio',$ID_Servicio);
            $MyServicio = new Servicio();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Servicio = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyServicio->setID_Servicio($Servicio['ID_Servicio']);
                $MyServicio->setNombre_Servicio($Servicio['Nombre_Servicio']);
                // $MyProducto->setPrecio($Producto['Precio']);
                // $MyProducto->setReferencia($Producto['Referencia']);
                // $MyProducto->setPeso($Producto['Peso']);
                // $MyProducto->setTipo_Producto($Producto['ID_Tipo_Producto']);

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyServicio;
        }

       
        
        //Listar todos los registros de la tabla
        public function ListarIVA(){
            $Db = Db::Conectar();
            $ListaIVA = [];
            $Sql = $Db->query('SELECT * FROM IVA');
            $Sql->execute();
            foreach($Sql->fetchAll() as $IVA){
                $MyIVA = new IVA();
                
                $MyIVA->setID_IVA($IVA['ID_IVA']);

                $MyIVA->setIVA($IVA['IVA']);
                $MyIVA->setEstado_IVA($IVA['Estado_IVA']);
                // $MyProducto->setPrecio($Producto['Precio']);

                // $MyProducto->setReferencia($Producto['Referencia']);

                // $MyProducto->setPeso($Producto['Peso']);

                // $MyProducto->setTipo_Producto($Producto['ID_Tipo_Producto']);

                $ListaIVA[] = $MyIVA;
            }
            return $ListaIVA;
        }

        public function TraerIVA(){
            $Db = Db::Conectar();
            $TraeIVA = [];
            $Sql = $Db->query('SELECT * FROM IVA WHERE Estado_IVA=1' );
            $Sql->execute();
            foreach($Sql->fetchAll() as $IVA){
                $MyIVA = new IVA();
                // echo $Servicio['ID_Servicio']."----".$Producto['Nombre_Servicio'];
                

                $MyIVA->setIVA($IVA['IVA']);

                // $MyServicio->setPrecio($Producto['Precio']);

                $TraeIVA[] = $MyIVA;
            }
            return $TraeIVA;
        }



        public function ModificarServicio($Servicio){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE servicios SET ID_Servicio=:ID_Servicio,Nombre_Servicio=:Nombre_Servicio
            WHERE ID_Servicio=:ID_Servicio'); 
            $Sql->bindValue('ID_Servicio',$Servicio->getID_Servicio());
            $Sql->bindValue('Nombre_Servicio',$Servicio->getNombre_Servicio());
            // $Sql->bindValue('Precio',$Servicio->getPrecio());
            // $Sql->bindValue('Referencia',$Servicio->getReferencia());
            // $Sql->bindValue('Peso',$Servicio->getPeso());
            // $Sql->bindValue('ID_Tipo_Servicio',$Servicio->getTipo_Servicio());
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarIVAEstado($ID_IVA){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE IVA SET Estado_IVA=:Estado_IVA
            WHERE ID_IVA=:ID_IVA'); 
            $Sql->bindValue('ID_IVA',$ID_IVA);
            
            // echo $ID_Servicio;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_IVA=explode(":",$ID_IVA);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_IVA[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('Estado_IVA',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ServicioNombreTraer($ServicioNombreTraer)
        { 
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT Nombre_Servicio FROM servicios 
            WHERE ID_Servicio=:ID_Servicio'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Servicio',$ServicioNombreTraer);
            $MyServicio = new Servicio();
            
            
            try{
                $Sql->execute(); 
                $Nombre_Servicio = $Sql->fetch(); 
                $MyServicio->setNombre_Servicio($Nombre_Servicio['Nombre_Servicio']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyServicio;
        }

        public function EliminarServicio($ID_Servicio){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM servicios WHERE ID_Servicio=:ID_Servicio'); 
            $Sql->bindValue('ID_Servicio',$ID_Servicio);
            $ID_Servicio = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>