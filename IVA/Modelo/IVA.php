<?php
    class IVA{
        //Parámentros de entrada
        private $ID_IVA;
        private $IVA;
        private $Estado_IVA;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setID_IVA($ID_IVA){
            $this->ID_IVA = $ID_IVA;
        }

        public function getID_IVA(){
            return $this->ID_IVA;
        }


        public function setIVA($IVA){
            $this->IVA = $IVA;
        }

        public function getIVA(){
            return $this->IVA;
        }

        //
        public function setEstado_IVA($Estado_IVA){
            $this->Estado_IVA = $Estado_IVA;
        }

        public function getEstado_IVA(){
            return $this->Estado_IVA;
        }


    }
    
    //Testear funcionalidad de clase.
    /*
    $Competencia = new Competencia(); //Crear objeto
    $Competencia->setCodigoCompetencia(27);
    $Competencia->setNombreCompetencia('Python');
    echo "Código Competencia: ".$Competencia->getCodigoCompetencia().
    " NombreCompetencia: ".$Competencia->getNombreCompetencia();
    */
?>