﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/CrudIVA.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/IVA.php');

$CrudIVA = new CrudIVA(); //Crear de un objeto CrudCompetencia
$ListaIVA = $CrudIVA->ListarIVA(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listarServicio.css">
</head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión IVA</h1>
    
        <div class="form-column-boton1">
            <button class="btn1" type="button">
            <a href="IngresarIVA.php">Crear IVA</a>
            </button>
        </div>

    <div id="main-container">  
        <table >
        <thead>
        <tr>
            <th>ID</th>
            <th>IVA</th>
        
            <th>Acciones</th> 
            
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaIVA as $IVA){
                ?>
                <tr>
                    <td><?php echo $IVA->getID_IVA(); ?></td>
                    <td><?php echo $IVA->getIVA(); ?></td>
                    <td hidden=""><?php echo $IVA->getEstado_IVA(); ?></td>
                    <td>
                         
                        
                        <?php
                            if ($IVA->getEstado_IVA()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorIVA.php?ID_IVA=<?php echo $IVA->getID_IVA();?>:<?php echo $IVA->getEstado_IVA();?>&Accion=EditarIVAEstado"
                        ><img width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($IVA->getEstado_IVA()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorIVA.php?ID_IVA=<?php echo $IVA->getID_IVA();?>:<?php echo $IVA->getEstado_IVA();?>&Accion=EditarIVAEstado"
                        ><img width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                    </td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
</body>
</html>