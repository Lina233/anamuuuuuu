<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/Servicio.php');
require_once('../Modelo/CrudServicio.php');

$CrudServicio =new CrudServicio(); //Crear un Objeto CrudCompetencia
$Servicio = $CrudServicio::ObtenerServicio($_GET["ID_Servicio"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-EditarServicio.css">
     <title>Modificar</title>
</head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    <!-- <h1 class="titulo1">EDITAR PRODUCTO</h1> -->
    <!-- <div class="diiv-1"> -->
    
    <form action="../Controlador/ControladorServicio.php" method="post">
        
        <h1 class="titulo1">Editar Servicio</h1>
        
           
        <input hidden="" readonly="disabled" type="text" name="ID_Servicio" id="ID_Servicio" value="<?php echo $Servicio->getID_Servicio(); ?>">
        <br>
        <br>
        
        <p>Nombre Servicio</p>
        <input  pattern="[A-Za-z -´ñ-Ñ]+" required type="text" name="Nombre_Servicio" id="Nombre_Servicio" value="<?php echo $Servicio->getNombre_Servicio(); ?>">
        <br>
        <br>
        
        <div class="form-column-boton1">
            <input type="hidden" name="Modificar" id="Modificar">
            <button class="btn1" type="submit">Modificar</button>
        </div>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarServicio.php">Cancelar</a>
            </button>
        </div>
    </form>
</body>
        
        
            

</html>