<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
// $CrudProducto = new CrudProducto(); //Crear de un objeto CrudCompetencia
// $TraerProductos = $CrudProducto->TraerProductos();
require_once('../../Conexion.php');
require_once('../../Tipo_Producto/Modelo/Tipo_Producto.php');
require_once('../../Tipo_Producto/Modelo/Crud_Tipo_Producto.php');
$CrudTipoProducto = new CrudTipoProducto(); //Crear de un objeto CrudCompetencia
$TraerTipoProducto = $CrudTipoProducto->TraerTipoProducto();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
     <link rel="stylesheet" type="text/css" href="css/estilos-ingresarServicio.css">
    </head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <div class="diiv-1">

        <form action="../Controlador/ControladorServicio.php" method="post">
        <h1 class="titulo1">Crear Servicios</h1>
            
        <div class="diiv-2">
               <p>Estado:</p>
                <select name="ID_Estado" id="ID_Estado">
                    <option value="1">Activado</option>
                    <option value="2">Desactivado</option>
                    
                </select>
                
                
        </div>          
        <div class="diiv-3">
                
                               
                <p>Nombre Servicio:</p>
                <input pattern="[A-Za-z -´ñ-Ñ]+" required type="text" name="Nombre_Servicio" id="Nombre_Servicio">
                <br>            
                        
                
            <br>
            
        
                  
            
        </div>  
              
        
        



        <div class="form-column-boton1">
            <input type="hidden" name="Registrar" id="Registrar">
            <button class="btn1" type="submit">Registrar</button>
        </div> 
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarServicio.php">Cancelar</a>
            </button>
        </div>
        <!-- <button class="btn2" type="button">
            <input type="hidden" name="Cancelar" id="Cancelar">
            <button class="btn1" type="submit">Cancelar</button>   
        </div> -->
        <br>
        </div>
    </form>
    
</body>
</html>