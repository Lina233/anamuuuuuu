﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/CrudServicio.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/Servicio.php');

$CrudServicio = new CrudServicio(); //Crear de un objeto CrudCompetencia
$ListaServicios = $CrudServicio->ListarServicios(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="css/estilos-listarServicio.css">
</head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>

    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Servicios</h1>
    
        <div class="form-column-boton1">
            <button class="btn1" type="button">
            <a href="IngresarServicio.php">Crear Servicio</a>
            </button>
        </div>

        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="../../TCPDF/examples/reporteServicio.php">Reporte PDF</a>
            </button>
        </div>

    <div id="main-container">  
        <table >
        <thead>
        <tr>
            <th>ID Servicio</th>
            <th>Nombre Servicio</th>
             <!-- <th>Precio</th>
            <th>Referencia</th>
            <th>Peso</th> -->
            <!-- <th>Tipo_Producto</th> -->
            <th>Acciones</th> 
            
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaServicios as $Servicio){
                ?>
                <tr>
                    <td><?php echo $Servicio->getID_Servicio(); ?></td>
                    <td><?php echo $Servicio->getNombre_Servicio(); ?></td>
                    <td hidden=""><?php echo $Servicio->getID_Estado(); ?></td>
                    <td>
                        <a class="link2" href="EditarServicio.php?ID_Servicio=<?php echo $Servicio->getID_Servicio(); ?>"><img width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a> 
                        
                        <?php
                            if ($Servicio->getID_Estado()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorServicio.php?ID_Servicio=<?php echo $Servicio->getID_Servicio();?>:<?php echo $Servicio->getID_Estado();?>&Accion=EditarServicioEstado"
                        ><img width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($Servicio->getID_Estado()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorServicio.php?ID_Servicio=<?php echo $Servicio->getID_Servicio();?>:<?php echo $Servicio->getID_Estado();?>&Accion=EditarServicioEstado"
                        ><img width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                    </td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
</body>
</html>