﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>



<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}


require_once('../../Conexion.php');
require_once('../Modelo/Servicio.php'); //Vincular la Clase Competencia
require_once('../Modelo/CrudServicio.php'); //Vincular la Clase Crud

$Servicio = new Servicio(); //Crear el objeto Competencia
$CrudServicio = new CrudServicio();
if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
    
    
    $Servicio->setNombre_Servicio($_POST["Nombre_Servicio"]); //Instanciar el atributo
    $Servicio->setID_Estado($_POST["ID_Estado"]);
  

   
    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudServicio::InsertarServicio($Servicio); // Llamar el método para Insertar

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'Servicio registrado exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarServicio.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    $Servicio->setID_Servicio($_POST["ID_Servicio"]); //Instanciar el atributo
    $Servicio->setNombre_Servicio($_POST["Nombre_Servicio"]); //Instanciar el atributo
    // $Producto->setPrecio($_POST["Precio"]);
    // $Producto->setReferencia($_POST["Referencia"]);
    // $Producto->setPeso($_POST["Peso"]);
    // $Producto->setTipo_Producto($_POST["Tipo_Producto"]);

    
    // echo $Servicio->getNombre_Servicio(); //Verificar instanciación
    $CrudServicio::ModificarServicio($Servicio); // Llamar el método para Modificar

    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Servicio modificado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarServicio.php';
                        }
                            })</script>";


}
elseif($_GET["Accion"]=="EditarServicioEstado"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudServicio::ModificarServicioEstado($_GET["ID_Servicio"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarServicio.php';
                        }
                            })</script>";
}
elseif($_GET["Accion"]=="EliminarServicio"){
    $CrudServicio::EliminarServicio($_GET["ID_Servicio"]); // Llamar el método para Modificar
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Servicio Eliminado exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarServicio.php';
                        }
                            })</script>";
}

?>

</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>