<?php
    class Servicio{
        //Parámentros de entrada
        private $ID_Servicio;
        private $Nombre_Servicio;
        private $ID_Estado;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setID_Servicio($ID_Servicio){
            $this->ID_Servicio = $ID_Servicio;
        }

        public function getID_Servicio(){
            return $this->ID_Servicio;
        }


        public function setNombre_Servicio($Nombre_Servicio){
            $this->Nombre_Servicio = $Nombre_Servicio;
        }

        public function getNombre_Servicio(){
            return $this->Nombre_Servicio;
        }

        //
        public function setID_Estado($ID_Estado){
            $this->ID_Estado = $ID_Estado;
        }

        public function getID_Estado(){
            return $this->ID_Estado;
        }


    }
    
    //Testear funcionalidad de clase.
    /*
    $Competencia = new Competencia(); //Crear objeto
    $Competencia->setCodigoCompetencia(27);
    $Competencia->setNombreCompetencia('Python');
    echo "Código Competencia: ".$Competencia->getCodigoCompetencia().
    " NombreCompetencia: ".$Competencia->getNombreCompetencia();
    */
?>