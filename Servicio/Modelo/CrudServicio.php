﻿<?php
//require_once('../../Conexion.php');    
    class CrudServicio{
    
        public function __construct(){
        }

        public function InsertarServicio($Servicio){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
            $Sql =$Db->prepare('INSERT INTO  servicios(ID_Servicio,Nombre_Servicio,ID_Estado)
            VALUES(NULL,:Nombre_Servicio,:ID_Estado)');
 

            // $Sql->execute();
            $Sql->bindValue('Nombre_Servicio',$Servicio->getNombre_Servicio());
            $Sql->bindValue('ID_Estado',$Servicio->getID_Estado());

            
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        }
        public function ObtenerServicio($ID_Servicio)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM servicios WHERE ID_Servicio=:ID_Servicio'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Servicio',$ID_Servicio);
            $MyServicio = new Servicio();//Crear un objeto de tipo competencia
            try{
                $Sql->execute(); //Ejecutar el Update
                $Servicio = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                $MyServicio->setID_Servicio($Servicio['ID_Servicio']);
                $MyServicio->setNombre_Servicio($Servicio['Nombre_Servicio']);
                // $MyProducto->setPrecio($Producto['Precio']);
                // $MyProducto->setReferencia($Producto['Referencia']);
                // $MyProducto->setPeso($Producto['Peso']);
                // $MyProducto->setTipo_Producto($Producto['ID_Tipo_Producto']);

            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyServicio;
        }

       
        
        //Listar todos los registros de la tabla
        public function ListarServicios(){
            $Db = Db::Conectar();
            $ListaServicios = [];
            $Sql = $Db->query('SELECT * FROM Servicios');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Servicio){
                $MyServicio = new Servicio();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyServicio->setID_Servicio($Servicio['ID_Servicio']);

                $MyServicio->setNombre_Servicio($Servicio['Nombre_Servicio']);
                $MyServicio->setID_Estado($Servicio['ID_Estado']);
                // $MyProducto->setPrecio($Producto['Precio']);

                // $MyProducto->setReferencia($Producto['Referencia']);

                // $MyProducto->setPeso($Producto['Peso']);

                // $MyProducto->setTipo_Producto($Producto['ID_Tipo_Producto']);

                $ListaServicios[] = $MyServicio;
            }
            return $ListaServicios;
        }



        public function TraerServicios(){
            $Db = Db::Conectar();
            $TraeServicios = [];
            $Sql = $Db->query('SELECT ID_Servicio,Nombre_Servicio FROM servicios WHERE ID_Estado=1' );
            $Sql->execute();
            foreach($Sql->fetchAll() as $Servicio){
                $MyServicio = new Servicio();
                // echo $Servicio['ID_Servicio']."----".$Producto['Nombre_Servicio'];
                $MyServicio->setID_Servicio($Servicio['ID_Servicio']);

                $MyServicio->setNombre_Servicio($Servicio['Nombre_Servicio']);

                // $MyServicio->setPrecio($Producto['Precio']);

                $TraeServicios[] = $MyServicio;
            }
            return $TraeServicios;
        }

        public function ObtenerNombreServicio($ID)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT servicios.Nombre_servicio 
            FROM facturas
            INNER JOIN  servicios
            ON facturas.ID_Servicio =servicios.ID_Servicio
            WHERE CodigoCotizacion=:CodigoCotizacion'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('CodigoCotizacion',$ID);
            $MyServicio = new Servicio();
            $MyFactura = new Factura();//Crear un objeto de tipo competencia
            
            try{
                $Sql->execute(); 
                $Nombre = $Sql->fetch(); 
                $MyServicio->setNombre_servicio($Nombre['Nombre_servicio']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyServicio;
        }



        public function ModificarServicio($Servicio){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE servicios SET ID_Servicio=:ID_Servicio,Nombre_Servicio=:Nombre_Servicio
            WHERE ID_Servicio=:ID_Servicio'); 
            $Sql->bindValue('ID_Servicio',$Servicio->getID_Servicio());
            $Sql->bindValue('Nombre_Servicio',$Servicio->getNombre_Servicio());
            // $Sql->bindValue('Precio',$Servicio->getPrecio());
            // $Sql->bindValue('Referencia',$Servicio->getReferencia());
            // $Sql->bindValue('Peso',$Servicio->getPeso());
            // $Sql->bindValue('ID_Tipo_Servicio',$Servicio->getTipo_Servicio());
            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ModificarServicioEstado($ID_Servicio){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE servicios SET ID_Estado=:ID_Estado
            WHERE ID_Servicio=:ID_Servicio'); 
            $Sql->bindValue('ID_Servicio',$ID_Servicio);
            
            // echo $ID_Servicio;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_Servicio=explode(":",$ID_Servicio);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_Servicio[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('ID_Estado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

        public function ServicioNombreTraer($ServicioNombreTraer)
        { 
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT Nombre_Servicio FROM servicios 
            WHERE ID_Servicio=:ID_Servicio'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Servicio',$ServicioNombreTraer);
            $MyServicio = new Servicio();
            
            
            try{
                $Sql->execute(); 
                $Nombre_Servicio = $Sql->fetch(); 
                $MyServicio->setNombre_Servicio($Nombre_Servicio['Nombre_Servicio']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyServicio;
        }

        //funcion reportes.
        public function ListarServiciopdf(){
            $Db = Db::Conectar();
            $ListaServicio = []; // define un vector 
            $Sql = $Db->query('SELECT * FROM servicios');
            $Sql->execute();
            foreach($Sql->fetchAll() as $Servicio){
                
                $MyServicio = new Servicio();
                // echo $Factura['ID_Producto']."----".$Factura['Nombre_Producto'];
                $MyServicio->setID_Servicio($Servicio['ID_Servicio']);
                $MyServicio->setID_Estado($Servicio['ID_Estado']);
                $MyServicio->setNombre_Servicio($Servicio['Nombre_Servicio']);
            
                
                                    
                $ListaServicio[] = $MyServicio;
            }
            return $ListaServicio;
        }

        public function EliminarServicio($ID_Servicio){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM servicios WHERE ID_Servicio=:ID_Servicio'); 
            $Sql->bindValue('ID_Servicio',$ID_Servicio);
            $ID_Servicio = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>