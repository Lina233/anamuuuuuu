<?php
    class Roles{
        //Parámentros de entrada
        private $IdRol;
        private $Nombre_Rol;
        
        

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setIdRol($IdRol){
            $this->IdRol = $IdRol;
        }

        public function getIdRol(){
            return $this->IdRol;
        }

        //
        public function setNombre_Rol($Nombre_Rol){
            $this->Nombre_Rol = $Nombre_Rol;
        }

        public function getNombre_Rol(){
            return $this->Nombre_Rol;
        }


        
    
    }
?>