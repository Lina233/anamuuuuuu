<?php
    class HojaTecnica{
        //Parámentros de entrada
        private $ID_Hoja_Tecnica;
        private $Nombre_Comun;
        private $Familia;
        private $Nombre_Cientifico;
        private $Tamano;
        private $Ubicacion;
        private $Suelo;
        private $Riego;
        private $Fertilizacion;
        private $Poda;
        private $Plagas;
        private $Estado;
        private $ID_Tipo_Planta;
        private $Imagen;

        //Definir el constructor
        public function __construct(){}

        //Definir los métodos set y get para cada atributo de la clase
        public function setID_Hoja_Tecnica($ID_Hoja_Tecnica){
            $this->ID_Hoja_Tecnica = $ID_Hoja_Tecnica;
        }

        public function getID_Hoja_Tecnica(){
            return $this->ID_Hoja_Tecnica;
        }

        //
        public function setID_Tipo_Planta($ID_Tipo_Planta){
            $this->ID_Tipo_Planta = $ID_Tipo_Planta;
        }

        public function getID_Tipo_Planta(){
            return $this->ID_Tipo_Planta;
        }

        public function setEstado($Estado){
            $this->Estado = $Estado;
        }

        public function getEstado(){
            return $this->Estado;
        }


        public function setNombre_Comun($Nombre_Comun){
            $this->Nombre_Comun = $Nombre_Comun;
        }

        public function getNombre_Comun(){
            return $this->Nombre_Comun;
        }


        public function setFamilia($Familia){
            $this->Familia = $Familia;
        }

        public function getFamilia(){
            return $this->Familia;
        }

        //
        public function setNombre_Cientifico($Nombre_Cientifico){
            $this->Nombre_Cientifico = $Nombre_Cientifico;
        }

        public function getNombre_Cientifico(){
            return $this->Nombre_Cientifico;
        }

        //
        public function setTamano($Tamano){
            $this->Tamano = $Tamano;
        }

        public function getTamano(){
            return $this->Tamano;
        }
        
        //
        public function setUbicacion($Ubicacion){
            $this->Ubicacion = $Ubicacion;
        }

        public function getUbicacion(){
            return $this->Ubicacion;
        }

        public function setSuelo($Suelo){
            $this->Suelo = $Suelo;
        }

        public function getSuelo(){
            return $this->Suelo;
        }

        public function setRiego($Riego){
            $this->Riego = $Riego;
        }

        public function getRiego(){
            return $this->Riego;
        }

        public function setFertilizacion($Fertilizacion){
            $this->Fertilizacion = $Fertilizacion;
        }

        public function getFertilizacion(){
            return $this->Fertilizacion;
        }


        public function setPoda($Poda){
            $this->Poda = $Poda;
        }

        public function getPoda(){
            return $this->Poda;
        }

        public function setPlagas($Plagas){
            $this->Plagas = $Plagas;
        }

        public function getPlagas(){
            return $this->Plagas;
        }

        public function setImagen($Imagen){
            $this->Imagen = $Imagen;
        }

        public function getImagen(){
            return $this->Imagen;
        }



    }
    
    //Testear funcionalidad de clase.
    /*
    $Competencia = new Competencia(); //Crear objeto
    $Competencia->setCodigoCompetencia(27);
    $Competencia->setNombreCompetencia('Python');
    echo "Código Competencia: ".$Competencia->getCodigoCompetencia().
    " NombreCompetencia: ".$Competencia->getNombreCompetencia();
    */
?>