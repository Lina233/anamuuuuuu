﻿<?php
//require_once('../../Conexion.php');  
 
    class CrudHojaTecnica{
    
        public function __construct(){
        }

        public function InsertarHojaTecnica($HojaTecnica){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la inserción a realizar.
               
            $Sql =$Db->prepare('INSERT INTO hojas_tecnicas(ID_Hojas_Tecnicas,Nombre_Comun,Familia,Nombre_Cientifico,Tamano,Ubicacion,Suelo,Riego,Fertilizacion,Poda,Plagas,Estado,ID_Tipo_Planta,Imagen)
            VALUES(NULL,:Nombre_Comun,:Familia,:Nombre_Cientifico,:Tamano,:Ubicacion,:Suelo,:Riego,:Fertilizacion,:Poda,:Plagas,:Estado,:ID_Tipo_Planta,:Imagen)');
            //$Sql->bindValue('ID_Hoja_Tecnica',$HojaTecnica->getID_Hoja_Tecnica());
            
            $Sql->bindValue('Nombre_Comun',$HojaTecnica->getNombre_Comun());
            $Sql->bindValue('Nombre_Cientifico',$HojaTecnica->getNombre_Cientifico());
            $Sql->bindValue('Familia',$HojaTecnica->getFamilia());
            $Sql->bindValue('Tamano',$HojaTecnica->getTamano());
            $Sql->bindValue('Ubicacion',$HojaTecnica->getUbicacion());
            
            $Sql->bindValue('Suelo',$HojaTecnica->getSuelo());
            $Sql->bindValue('Riego',$HojaTecnica->getRiego());
            $Sql->bindValue('Fertilizacion',$HojaTecnica->getFertilizacion());
            $Sql->bindValue('Poda',$HojaTecnica->getPoda());
            $Sql->bindValue('Plagas',$HojaTecnica->getPlagas());
            $Sql->bindValue('Estado',$HojaTecnica->getEstado());

            $Sql->bindValue('ID_Tipo_Planta',$HojaTecnica->getID_Tipo_Planta());
            $Sql->bindValue('Imagen',$HojaTecnica->getImagen());
            // $Insert->bindValue('Nombre_Producto',md5($Producto->getNombre_Servicio()));
            try{
                $Sql->execute(); //Ejecutar el Insert
                // echo "Registro Exitoso";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la inserción
                die();
            }
        } 

        ///obtener sirve para traer los datos cuando edito 
        public function ObtenerHojaTecnica($ID_Hoja_Tecnica)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT * FROM hojas_tecnicas WHERE ID_Hojas_Tecnicas=:ID_Hoja_Tecnica'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Hoja_Tecnica',$ID_Hoja_Tecnica);
            $MyHojaTecnica = new HojaTecnica();//Crear un objeto de tipo competencia
            try{

                $Sql->execute(); //Ejecutar el Update
                $HojaTecnica = $Sql->fetch(); //Se almacena en la variable $Producto los datos de la variable $Sql
                
                $MyHojaTecnica->setID_Hoja_Tecnica($HojaTecnica['ID_Hojas_Tecnicas']);
                
                $MyHojaTecnica->setNombre_Comun($HojaTecnica['Nombre_Comun']);
                $MyHojaTecnica->setNombre_Cientifico($HojaTecnica['Nombre_Cientifico']);
                $MyHojaTecnica->setFamilia($HojaTecnica['Familia']);
                $MyHojaTecnica->setTamano($HojaTecnica['Tamano']);
                // $MyHojaTecnica->setUbicacion($HojaTecnica['Ubicacion']);
                $MyHojaTecnica->setSuelo($HojaTecnica['Suelo']);
                $MyHojaTecnica->setRiego($HojaTecnica['Riego']);
                $MyHojaTecnica->setFertilizacion($HojaTecnica['Fertilizacion']);
                $MyHojaTecnica->setPoda($HojaTecnica['Poda']);
                $MyHojaTecnica->setPlagas($HojaTecnica['Plagas']);
                $MyHojaTecnica->setUbicacion($HojaTecnica['Ubicacion']);
                // $MyProducto->setID_Producto($HojaTecnica['ID_Producto']);
                $MyHojaTecnica->setEstado($HojaTecnica['Estado']);
                $MyHojaTecnica->setID_Tipo_Planta($HojaTecnica['ID_Tipo_Planta']);
                $MyHojaTecnica->setImagen($HojaTecnica['Imagen']);
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            return $MyHojaTecnica;
        }

        public function ObtenerNombreComunHojasTecnicas($ID_Producto)
        { //Código para obtener una Competecia
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT hojas_tecnicas.Nombre_Comun 
            FROM Producto
            INNER JOIN  hojas_tecnicas
            ON Producto.Hoja_Tecnica = hojas_tecnicas.ID_Hojas_Tecnicas
            WHERE ID_Producto=:ID_Producto'); //¿Porque en el WHERE solo accede al dato ID_Producto
            $Sql->bindValue('ID_Producto',$ID_Producto);
            $MyProducto = new Producto();
            $MyHojaTecnica = new HojaTecnica();//Crear un objeto de tipo competencia
            
            try{
                $Sql->execute(); 
                $Nombre_Comun = $Sql->fetch(); 
                $MyHojaTecnica->setNombre_Comun($Nombre_Comun['Nombre_Comun']);
                
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificiación
                die();
            }
            
            //aqui returnamos mytipoplanta porque con este esta el nombre tipo planta si retornamos myhojatecnica solo o con mytipoplanta no trae el nombre comun hay algun error no detectado pero asi como esta trabaja para lo qu se necesita

            return $MyHojaTecnica;
        }

       
        
        //Listar todos los registros de la tabla
        public function ListarHojaTecnicas(){
            $Db = Db::Conectar();
            $ListaHojaTecnicas = [];
            $Sql = $Db->query('SELECT * FROM hojas_tecnicas WHERE ID_Hojas_Tecnicas  NOT LIKE 1 ');
            $Sql->execute();
            foreach($Sql->fetchAll() as $HojaTecnica){
                $MyHojaTecnica = new HojaTecnica();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyHojaTecnica->setID_Hoja_Tecnica($HojaTecnica['ID_Hojas_Tecnicas']);
                $MyHojaTecnica->setNombre_Comun($HojaTecnica['Nombre_Comun']);
                $MyHojaTecnica->setNombre_Cientifico($HojaTecnica['Nombre_Cientifico']);
                $MyHojaTecnica->setFamilia($HojaTecnica['Familia']);
                $MyHojaTecnica->setTamano($HojaTecnica['Tamano']);
                $MyHojaTecnica->setUbicacion($HojaTecnica['Ubicacion']);
                $MyHojaTecnica->setSuelo($HojaTecnica['Suelo']);
                $MyHojaTecnica->setRiego($HojaTecnica['Riego']);
                $MyHojaTecnica->setFertilizacion($HojaTecnica['Fertilizacion']);
                $MyHojaTecnica->setPoda($HojaTecnica['Poda']);
                $MyHojaTecnica->setPlagas($HojaTecnica['Plagas']);
                $MyHojaTecnica->setID_Tipo_Planta($HojaTecnica['ID_Tipo_Planta']);
                $MyHojaTecnica->setImagen($HojaTecnica['Imagen']);
                $MyHojaTecnica->setEstado($HojaTecnica['Estado']);
                
                // $MyHojaTecnica->setReferencia($HojaTecnica['Referencia']);

                $ListaHojaTecnicas[] = $MyHojaTecnica;
            }
            return $ListaHojaTecnicas;
        }

        public function ListarHojaTecnicasCliente(){
            $Db = Db::Conectar();
            $ListaHojaTecnicas = [];
            $Sql = $Db->query('SELECT * FROM hojas_tecnicas WHERE Estado=1 AND  ID_Hojas_Tecnicas  NOT LIKE 1');
            $Sql->execute();
            foreach($Sql->fetchAll() as $HojaTecnica){
                $MyHojaTecnica = new HojaTecnica();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyHojaTecnica->setID_Hoja_Tecnica($HojaTecnica['ID_Hojas_Tecnicas']);
                $MyHojaTecnica->setNombre_Comun($HojaTecnica['Nombre_Comun']);
                $MyHojaTecnica->setNombre_Cientifico($HojaTecnica['Nombre_Cientifico']);
                $MyHojaTecnica->setFamilia($HojaTecnica['Familia']);
                $MyHojaTecnica->setTamano($HojaTecnica['Tamano']);
                $MyHojaTecnica->setUbicacion($HojaTecnica['Ubicacion']);
                $MyHojaTecnica->setSuelo($HojaTecnica['Suelo']);
                $MyHojaTecnica->setRiego($HojaTecnica['Riego']);
                $MyHojaTecnica->setFertilizacion($HojaTecnica['Fertilizacion']);
                $MyHojaTecnica->setPoda($HojaTecnica['Poda']);
                $MyHojaTecnica->setPlagas($HojaTecnica['Plagas']);
                $MyHojaTecnica->setID_Tipo_Planta($HojaTecnica['ID_Tipo_Planta']);
                $MyHojaTecnica->setImagen($HojaTecnica['Imagen']);
                $MyHojaTecnica->setEstado($HojaTecnica['Estado']);
                
                // $MyHojaTecnica->setReferencia($HojaTecnica['Referencia']);

                $ListaHojaTecnicas[] = $MyHojaTecnica;
            }
            return $ListaHojaTecnicas;
        }


        public function TraerHojaTecnica(){
            $Db = Db::Conectar();
            $TraeProductos = [];
            $Sql = $Db->query('SELECT ID_Hojas_Tecnicas,Nombre_Comun FROM hojas_tecnicas WHERE Estado=1');
            $Sql->execute();

            foreach($Sql->fetchAll() as $HojaTecnica){
                $MyHojaTecnica = new HojaTecnica();
                // echo $Producto['ID_Producto']."----".$Producto['Nombre_Producto'];
                $MyHojaTecnica->setID_Hoja_Tecnica($HojaTecnica['ID_Hojas_Tecnicas']);

                $MyHojaTecnica->setNombre_Comun($HojaTecnica['Nombre_Comun']);
                $TraerHojaTecnicas[] = $MyHojaTecnica;
            }
            return $TraerHojaTecnicas;
        }


        public function ModificarHojaTecnica($HojaTecnica){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('UPDATE hojas_tecnicas SET Nombre_Comun=:Nombre_Comun,Nombre_Cientifico=:Nombre_Cientifico,Familia=:Familia,Tamano=:Tamano,Ubicacion=:Ubicacion,Suelo=:Suelo,Riego=:Riego,Fertilizacion=:Fertilizacion,Poda=:Poda,Plagas=:Plagas,ID_Tipo_Planta=:ID_Tipo_Planta,Imagen=:Imagen
            WHERE ID_Hojas_Tecnicas=:ID_Hoja_Tecnica'); 
            
            $Sql->bindValue('ID_Hoja_Tecnica',$HojaTecnica->getID_Hoja_Tecnica());
            $Sql->bindValue('Nombre_Comun',$HojaTecnica->getNombre_Comun());
            $Sql->bindValue('Nombre_Cientifico',$HojaTecnica->getNombre_Cientifico());
            $Sql->bindValue('Familia',$HojaTecnica->getFamilia());
            $Sql->bindValue('Tamano',$HojaTecnica->getTamano());
            $Sql->bindValue('Ubicacion',$HojaTecnica->getUbicacion());
            $Sql->bindValue('Suelo',$HojaTecnica->getSuelo());
            $Sql->bindValue('Riego',$HojaTecnica->getRiego());
            $Sql->bindValue('Fertilizacion',$HojaTecnica->getFertilizacion());
            $Sql->bindValue('Poda',$HojaTecnica->getPoda());
            $Sql->bindValue('Plagas',$HojaTecnica->getPlagas());
            
            
            
            $Sql->bindValue('ID_Tipo_Planta',$HojaTecnica->getID_Tipo_Planta());
            $Sql->bindValue('Imagen',$HojaTecnica->getImagen());
            try{
                    $Sql->execute(); //Ejecutar el Sql que un Update
                    // echo "Modificación Exitosa";
                }
                catch(Exception $e){ //Capturar Errores
                    echo $e->getMessage(); //Mostar errores en la modificación
                    die();
                }
            }


        public function ModificarHojaTecnicaEstado($ID_Hojas_Tecnicas){
            $Db = Db::Conectar(); 
            $Sql = $Db->prepare('UPDATE hojas_tecnicas SET Estado=:Estado
            WHERE ID_Hojas_Tecnicas=:ID_Hojas_Tecnicas'); 
            $Sql->bindValue('ID_Hojas_Tecnicas',$ID_Hojas_Tecnicas);
            
            // echo $ID_Producto;
            
            
            //utilizo explode corto el string donde estan los :
            // es el cortador
            $ID_Hojas_Tecnicas=explode(":",$ID_Hojas_Tecnicas);
            //creo una variable donde guardo el digito cortado
            $DigitoNuevo2=$ID_Hojas_Tecnicas[1];

            //condicional que lee si es uno o dos y les da un nuevo valor
            if ($DigitoNuevo2==1) {
                $NuevoEstado=2;
            }elseif ($DigitoNuevo2==2) {
                $NuevoEstado=1;
            }

            //asigno nuevoestado al campo idestado de la sentencia sql
            $Sql->bindValue('Estado',$NuevoEstado);

            try{
                $Sql->execute(); //Ejecutar el Sql que un Update
                // echo "Modificación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }    

        public function EliminarHojaTecnica($ID_Hoja_Tecnica){
            $Db = Db::Conectar(); //Conectar a la base de datos
            //Definir la modificación a realizar.
            $Sql = $Db->prepare('DELETE FROM hoja_tecnica WHERE ID_Hoja_Tecnica=:ID_Hoja_Tecnica'); 
            $Sql->bindValue('ID_Hoja_Tecnica',$ID_Hoja_Tecnica);
            $ID_Hoja_Tecnica = 23;
            try{
                $Sql->execute(); //Ejecutar el Sql que contiene el Delete
                echo "Eliminación Exitosa";
            }
            catch(Exception $e){ //Capturar Errores
                echo $e->getMessage(); //Mostar errores en la modificación
                die();
            }
        }

    }


// $Crud = new CrudProducto();
// $Crud->ListarProductos();


?>