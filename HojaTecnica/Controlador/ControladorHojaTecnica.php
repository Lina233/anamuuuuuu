﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>
</head>
<body>
<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}


require_once('../../Conexion.php');
require_once('../Modelo/HojaTecnica.php'); //Vincular la Clase Competencia
require_once('../Modelo/CrudHojaTecnica.php'); //Vincular la Clase Crud




$HojaTecnica = new HojaTecnica(); //Crear el objeto Competencia
$CrudHojaTecnica = new CrudHojaTecnica();

if(isset($_POST["Registrar"])) //Si la petición es de Registrar
{
    // echo "Registrar";
 
    
    // $HojaTecnica->setID_Hoja_Tecnica($_POST["ID_Hoja_Tecnica"]); //Instanciar el atributo
    $HojaTecnica->setNombre_Comun($_POST["Nombre_Comun"]); //Instanciar el atributo
    $HojaTecnica->setNombre_Cientifico($_POST["Nombre_Cientifico"]); //Instanciar el atributo
    $HojaTecnica->setFamilia($_POST["Familia"]); //Instanciar el atributo
    $HojaTecnica->setTamano($_POST["Tamano"]); //Instanciar el atributo
    $HojaTecnica->setUbicacion($_POST["Ubicacion"]); //Instanciar el atributo
   
    $HojaTecnica->setSuelo($_POST["Suelo"]); //Instanciar el atributo
    $HojaTecnica->setRiego($_POST["Riego"]); //Instanciar el atributo
    $HojaTecnica->setFertilizacion($_POST["Fertilizacion"]); //Instanciar el atributo
    $HojaTecnica->setPoda($_POST["Poda"]); //Instanciar el atributo
    $HojaTecnica->setPlagas($_POST["Plagas"]); //Instanciar el atributo
    
    $HojaTecnica->setEstado($_POST["Estado"]);
    $HojaTecnica->setID_Tipo_Planta($_POST["ID_Tipo_Planta"]);
    $HojaTecnica->setImagen(file_get_contents($_FILES["Imagen"]["tmp_name"]));
    


    // echo $Producto->getID_Producto(); //Verificar instanciación
    $CrudHojaTecnica::InsertarHojaTecnica($HojaTecnica); // Llamar el método para Insertar

    
     //Instanciar el atributo
    
    
    

    echo "<script>
                    
                        
            swal.fire({
            title: 'Éxito',
            text: 'HojaTecnica registrado exitosamente',
            type: 'success',
            confirmButtonText: 'Okey',
                        
            }).then((result) => {
              if (result.value) {
                window.location.href = '../Vista/ListarHojaTecnica.php';
                  }
            })
                                  
                            

                        
        </script>";
        

}
elseif(isset($_POST["Modificar"])) //Si la petición es de Modificar
{
    // echo "Modificar";
    
    $HojaTecnica->setID_Hoja_Tecnica($_POST["ID_Hoja_Tecnica"]);
    $HojaTecnica->setNombre_Comun($_POST["Nombre_Comun"]); //Instanciar el atributo
    $HojaTecnica->setNombre_Cientifico($_POST["Nombre_Cientifico"]); //Instanciar el atributo
    $HojaTecnica->setFamilia($_POST["Familia"]); //Instanciar el atributo
    $HojaTecnica->setTamano($_POST["Tamano"]); //Instanciar el atributo
    $HojaTecnica->setUbicacion($_POST["Ubicacion"]); //Instanciar el atributo
   
    $HojaTecnica->setSuelo($_POST["Suelo"]); //Instanciar el atributo
    $HojaTecnica->setRiego($_POST["Riego"]); //Instanciar el atributo
    $HojaTecnica->setFertilizacion($_POST["Fertilizacion"]); //Instanciar el atributo
    $HojaTecnica->setPoda($_POST["Poda"]); //Instanciar el atributo
    $HojaTecnica->setPlagas($_POST["Plagas"]); //Instanciar el atributo
    
    
    $HojaTecnica->setID_Tipo_Planta($_POST["ID_Tipo_Planta"]);
    $HojaTecnica->setImagen(file_get_contents($_FILES["Imagen"]["tmp_name"]));
    
    
    $CrudHojaTecnica::ModificarHojaTecnica($HojaTecnica); // Llamar el método para Modificar
    
    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'HojaTecnica modificada exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarHojaTecnica.php';
                        }
                            })</script>";


}elseif($_GET["Accion"]=="EditarHojaTecnicaEstado"){


    // $CrudUsuario::ModificarUsuarioEstado($_GET["IdUsuario"]); 

    $CrudHojaTecnica::ModificarHojaTecnicaEstado($_GET["ID_Hoja_Tecnica"]);

    




    echo "<script>                       
                swal.fire({
                    title: 'Éxito',
                    text: 'Estado Modificado Exitosamente',
                    type: 'success',
                    confirmButtonText: 'Okey',
                        
                    }).then((result) => {
                        if (result.value) {
                                window.location.href = '../Vista/ListarHojaTecnica.php';
                        }
                            })</script>";
}
// elseif($_GET["Accion"]=="EliminarProducto"){
//     $CrudProducto::EliminarProducto($_GET["ID_Producto"]); // Llamar el método para Modificar
//     echo "<script>                       
//                 swal.fire({
//                     title: 'Éxito',
//                     text: 'Producto Eliminado exitosamente',
//                     type: 'success',
//                     confirmButtonText: 'Okey',
                        
//                     }).then((result) => {
//                         if (result.value) {
//                                 window.location.href = '../Vista/ListarProducto.php';
//                         }
//                             })</script>";
// }

?>

</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>
