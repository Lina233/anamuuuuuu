﻿<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/CrudHojaTecnica.php'); //Inlcuir el modelo CrudCompetencia
require_once('../Modelo/HojaTecnica.php');

$CrudHojaTecnica = new CrudHojaTecnica(); //Crear de un objeto CrudCompetencia
$ListaHojaTecnicas = $CrudHojaTecnica->ListarHojaTecnicas(); //Llamado al método ListarCompetencia
//var_dump($ListaCompetencias);// para validar los datos que estoy enviando
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- link obligatorio de los iconos no borrar -->
    <!-- <a href="https://icons8.com/icon/AuMLFRmG95tQ/edit">Edit icon by Icons8</a> -->
    
    <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="CSS/estilos-listarHojaTecnica.css">
</head>
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>


    <div class="diiv-1"></div>
    <h1 class="titulo1">Gestión Hojas Técnicas</h1>
    
        <div class="form-column-boton1">
            <button class="btn1" type="button">
            <a href="IngresarHojaTecnica.php">Crear Hoja Técnica</a>
            </button>
        </div>

        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="../../TCPDF/examples/reporteHojaTecnica.php">Reporte PDF</a>
            </button>
        </div>

    <div id="main-container">  
        <table >
        <thead>


            
        <tr>
            <th>ID</th>
            <th>Imagen</th>
            <th> Nombre Común</th>
            <!-- <th>Nombre_Cientifico</th> -->
            <!-- <th>Familia</th> -->
            <th>Tamaño</th>
            <th>Ubicación</th>
            <th>Suelo</th>
            <th>Riego</th>
            <th>FT</th>
            <th>Poda</th>
            <th>Plagas</th>
            <th>Tipo</th>
            <th>Acciones</th> 
            
        </tr>
        </thead>

        <tbody>
        <?php
            foreach($ListaHojaTecnicas as $HojaTecnica){
                ?>
                <tr>
                   
                    <td><?php echo $HojaTecnica->getID_Hoja_Tecnica(); ?></td>
                    <td>
                        <img height="60px" 
                        src="data:image/*;base64,
                        <?php 
                        echo base64_encode($HojaTecnica->getImagen()); 
                        ?>" >

                    </td>
                    <td><?php echo $HojaTecnica->getNombre_Comun(); ?></td>
                    <!-- <td><?php echo $HojaTecnica->getNombre_Cientifico(); ?></td> -->
                    <!-- <td><?php echo $HojaTecnica->getFamilia(); ?></td> -->
                    <td><?php echo $HojaTecnica->getTamano(); ?></td>
                    <td><?php echo $HojaTecnica->getUbicacion(); ?></td>
                    <td hidden=""><?php echo $HojaTecnica->getEstado(); ?></td>
                    <td><?php echo $HojaTecnica->getSuelo(); ?></td>
                    <td><?php echo $HojaTecnica->getRiego(); ?></td>
                    <td><?php echo $HojaTecnica->getFertilizacion(); ?></td>
                    <td><?php echo $HojaTecnica->getPoda(); ?></td>
                    <td><?php echo $HojaTecnica->getPlagas(); ?></td>
                    <td><?php echo $HojaTecnica->getID_Tipo_Planta(); ?></td>
                    <td>
                    <a class="link2" href="EditarHojaTecnica.php?ID_Hoja_Tecnica=<?php echo $HojaTecnica->getID_Hoja_Tecnica(); ?>"><img title="Editar" width="40" src="https://img.icons8.com/fluent-systems-filled/24/000000/edit.png"/></a> 
                    
                    <?php
                            if ($HojaTecnica->getEstado()==1) {
                        ?>
                       
                        <a  
                        href="../Controlador/ControladorHojaTecnica.php?ID_Hoja_Tecnica=<?php echo $HojaTecnica->getID_Hoja_Tecnica();?>:<?php echo $HojaTecnica->getEstado();?>&Accion=EditarHojaTecnicaEstado"
                        ><img title="Hoja Activada" width="40" src="https://img.icons8.com/fluent/48/000000/ok.png"/>
                        </a>
                        
                            
                        <?php             
                            }elseif ($HojaTecnica->getEstado()==2) {
                            ?>
                            <a  
                        href="../Controlador/ControladorHojaTecnica.php?ID_Hoja_Tecnica=<?php echo $HojaTecnica->getID_Hoja_Tecnica();?>:<?php echo $HojaTecnica->getEstado();?>&Accion=EditarHojaTecnicaEstado"
                        ><img title="Hoja Desactivada" width="40" src="https://img.icons8.com/fluent/48/000000/delete-sign.png"/>
                        </a>
                            <?php  

                            }

                        ?>
                    </td>
                </tr>
                <?php
            }
        ?>
        </tbody>
    </table>
</body>
</html>  
                 