<?php
session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

// require_once('../../Modelo/Producto.php');
// require_once('../../Modelo/CrudProducto.php');
    
require_once('../../Conexion.php');
//require_once('../../Tipo_Producto/Modelo/Tipo_Producto.php');

require_once('../../Tipo_Planta/Modelo/Tipo_Planta.php');
require_once('../../Tipo_Planta/Modelo/Crud_Tipo_Planta.php');

$CrudTipo_Planta = new CrudTipo_Planta(); //Crear de un objeto CrudCompetencia
$TraerTipoPlanta = $CrudTipo_Planta->TraerTipoPlanta();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
     <link rel="stylesheet" type="text/css" href="CSS/estilos-ingresarHojaTecnica.css">
    
</head>
    
<body>

    <!-- navbar -->
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <div class="diiv-1">

        <form action="../Controlador/ControladorHojaTecnica.php" method="post" enctype="multipart/form-data">
        <h1 class="titulo1">Crear Hoja Técnica</h1>
            
        <div class="diiv-2">
                <p>Suelo:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Suelo" id="Suelo">
                <br>
                <p>Riego Semanal:</p>
                <input pattern="[0-9]+" type="Riego" name="Riego" id="Riego">
                <br>
                <p>Fertilización Anual:</p>
                <input pattern="[0-9]+" type="Fertilizacion" name="Fertilizacion" id="Fertilizacion">
                <br>
                <p>Poda Mensual Recomendada:</p>
                <input pattern="[0-9]+" type="Poda" name="Poda" id="Poda">
                <br>
                <p>Plagas:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Plagas" id="Plagas">
                <br>
                <p>Estado:</p>
                <select name="Estado" id="Estado">
                    <option value="1">Activado</option>
                    <option value="2">Desactivado</option>
                    
                </select>
          
          
                    
                            
             
                
        </div>          
        <div class="diiv-3">
                <!-- <p>ID Hoja_Tecnica:</p> 
                <input pattern="[0-9]+" type="text" name="ID_Hoja_Tecnica" id="ID_Hoja_Tecnica">
                <br> -->
                <p>Nombre Común:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Nombre_Comun" id="Nombre_Comun">
                <br>
                <p>Nombre Científico:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Nombre_Cientifico" id="Nombre_Cientifico">
                <br>
                <p>Familia:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Familia" id="Familia">
                <br>
                <p>Tamaño(cm):</p> 
                <input pattern="[0-9]+" type="text" name="Tamano" id="Tamano">
                <br>
                <p>Ubicación:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Ubicacion" id="Ubicacion">
                <br>
                <p>Tipo Planta:</p>
                <select name="ID_Tipo_Planta" id="ID_Tipo_Planta">
                
                            
                        <?php
                            foreach($TraerTipoPlanta as $HojaTecnica){
                                ?>                                
                                <option value="<?php echo $HojaTecnica->getID_Tipo_Planta(); ?>">


                                    <?php echo $HojaTecnica->getNombre_Tipo_Planta(); ?>
                                </option>
                                <?php
                            }
                        ?>
                </select>
                <br>
                <p>Imagen:</p>
                <input  required type="file" name="Imagen" id="Imagen" accept="image/*">
                
         </div>  
                
        
        <div class="form-column-boton1">
            <input type="hidden" name="Registrar" id="Registrar">
            <button class="btn1" type="submit">Registrar</button>
        </div> 
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarHojaTecnica.php">Cancelar</a>
            </button>
        </div>
        <!-- <button class="btn2" type="button">
            <input type="hidden" name="Cancelar" id="Cancelar">
            <button class="btn1" type="submit">Cancelar</button>   
        </div> -->
        <br>
        </div>
    </form>
    
</body>




</html>
