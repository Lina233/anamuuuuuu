<?php

session_start();
if (!(isset($_SESSION["NombreUsuario"]))) {//si la sesion no existe redireccionar al login
    header("Location:../../Index.html");
}

if ( $_SESSION["IdRol"]==3  ) 
{
    header("Location:../../Index.php");
}

?>
<?php
require_once('../../Conexion.php');
require_once('../Modelo/HojaTecnica.php');
require_once('../Modelo/CrudHojaTecnica.php');
require_once('../../Tipo_Planta/Modelo/Tipo_Planta.php');
require_once('../../Tipo_Planta/Modelo/Crud_Tipo_Planta.php');
$CrudHojaTecnica = new CrudHojaTecnica(); //Crear un Objeto CrudCompetencia
$CrudTipo_Planta = new CrudTipo_Planta();

$HojaTecnica = $CrudHojaTecnica::ObtenerHojaTecnica($_GET["ID_Hoja_Tecnica"]);

$NombreComun = $CrudTipo_Planta::ObtenerHojaTecnicaNombreTipoPlanta($_GET["ID_Hoja_Tecnica"]);

// $HojaTecnicas = $CrudHojaTecnica::ObtenerHojaTecnicaNombreTipoPlanta($_GET["ID_Hoja_Tecnica"]);

$TraerTipoPlanta = $CrudTipo_Planta->TraerTipoPlanta();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
   <link rel="stylesheet" type="text/css" href="../../vistas/navbar/css/estilos-dashboard.css">
    <link rel="stylesheet" type="text/css" href="CSS/estilos-editarHojaTecnica.css">
    <title>Modificar</title>
</head>
<body>
    <?php 
    include('../../vistas/navbar/dashboard.php'); 
    ?>
    
    <div class="diiv-1">

        <form action="../Controlador/ControladorHojaTecnica.php"  method="post" enctype="multipart/form-data">
        <h1 class="titulo1">Editar Hoja Técnica</h1>
            
        <div class="diiv-2">
                
            
        <p>Suelo:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Suelo" id="Suelo" value="<?php echo $HojaTecnica->getSuelo(); ?>">
                <br>
                <p>Riego Semanal:</p>
                <input pattern="[0-9]+" type="text" name="Riego" id="Riego" value="<?php echo $HojaTecnica->getRiego(); ?>">
                <br>
                <p>Fertilización Anual:</p>
                <input pattern="[0-9]+" type="text" name="Fertilizacion" id="Fertilizacion" value="<?php echo $HojaTecnica->getFertilizacion(); ?>">
                <br>
                <p>Poda mensual recomendada:</p>
                <input pattern="[0-9]+" type="text" name="Poda" id="Poda" value="<?php echo $HojaTecnica->getPoda(); ?>">
                <br>
                <p>Plagas:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Plagas" id="Plagas" value="<?php echo $HojaTecnica->getPlagas(); ?>">
                <br>
                <p>Tipo Planta:</p>
                <select name="ID_Tipo_Planta" id="ID_Tipo_Planta">
                <option >
                    <?php echo $NombreComun->getNombre_Tipo_Planta(); ?>
                </option>
                            
                        <?php
                            foreach($TraerTipoPlanta as $HojaTecnica1){
                                ?>                                
                                <option value="<?php echo $HojaTecnica1->getID_Tipo_Planta(); ?>">


                                    <?php echo $HojaTecnica1->getNombre_Tipo_Planta(); ?>
                                </option>
                                <?php
                            }
                        ?>
                </select>
                
                
        </div>          
        <div class="diiv-3">
                
                <input hidden="" required pattern="[0-9]+" readonly="disabled" type="text" name="ID_Hoja_Tecnica" id="ID_Hoja_Tecnica" value="<?php echo $HojaTecnica->getID_Hoja_Tecnica(); ?>">
                
                <p>Nombre Común:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Nombre_Comun" id="Nombre_Comun" value="<?php echo $HojaTecnica->getNombre_Comun(); ?>">
                <br>
                <p>Nombre Científico:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Nombre_Cientifico" id="Nombre_Cientifico" value="<?php echo $HojaTecnica->getNombre_Cientifico(); ?>">
                <br>
                <p>Familia:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Familia" id="Familia" value="<?php echo $HojaTecnica->getFamilia(); ?>">
                <br>
                
                <p>Tamaño:</p> 
                <input required pattern="[0-9]+" type="text" name="Tamano" id="Tamano" value="<?php echo $HojaTecnica->getTamano(); ?>">
                <br>
                <p>Ubicación:</p>
                <input pattern="[A-Za-z -ñÑ-´]+" required type="text" name="Ubicacion" id="Ubicacion" value="<?php echo $HojaTecnica->getUbicacion(); ?>">
                <br>
                <p>Imagen</p>
                <input required title="Este campo debe estar cargado con una imagen"  type="file" name="Imagen" id="Imagen" accept="image/*" 

                                
                >
                <br>
                <img  height="100px" 
                        src="data:image/*;base64,
                        <?php 
                        echo base64_encode($HojaTecnica->getImagen()); 
                ?>" >
                

        </div>  
                
        <br>
        <div class="form-column-boton1">
            <input  type="hidden" name="Modificar" id="Modificar">
            <button class="btn1" type="submit">Editar</button>
        </div> 
        <br>
        
        <div class="form-column-boton2">
            <button class="btn2" type="button">
                <a href="ListarHojaTecnica.php">Cancelar</a>
            </button>
        </div>
        <br>
    
        </form>
    </div>
    </body>
</html>